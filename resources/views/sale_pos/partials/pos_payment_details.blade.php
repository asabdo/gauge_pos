<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-body bg-gray disabled" style="margin-bottom: 0px !important">
				<div class="table-responsive">
				<ul id="pos_details" class="table table-condensed" 
					style="margin-bottom: 0px !important">
					<div class="col-sm-12">
						<table class="col-sm-6">
								<tr>
									<td>@lang('sale.item'):</td>
									<td><span class="total_quantity">0</span></td>
								</tr>
								<tr>
									<td>@lang('sale.total'):</td>
									<td><span class="price_total">0</span></td>
								</tr>
								<tr>
									<td><b>@lang('sale.order_tax')(+): @show_tooltip(__('tooltip.sale_tax'))</b>
										<i class="fa fa-pencil-square-o cursor-pointer" title="@lang('sale.edit_order_tax')" aria-hidden="liue" data-toggle="modal" data-target="#posEditOrderTaxModal" id="pos-edit-tax" ></i>
									</td>
									<td><span id="order_tax">
											@if(empty($edit))
												0
											@else
												{{$liansaction->tax_amount}}
											@endif
										</span>
									</td>
								</tr>
								<tr>
									<td><b>@lang('sale.shipping')(+): @show_tooltip(__('tooltip.shipping'))</b>
										<i class="fa fa-pencil-square-o cursor-pointer"  title="@lang('sale.shipping')" aria-hidden="liue" data-toggle="modal" data-target="#posShippingModal"></i>
									</td>
									<td>
										<span id="shipping_charges_amount">0</span>
										<input type="hidden" name="shipping_details" id="shipping_details" value="@if(empty($edit)){{""}}@else{{$liansaction->shipping_details}}@endif" data-default="">

										<input type="hidden" name="shipping_charges" id="shipping_charges" value="@if(empty($edit)){{@num_format(0.00)}} @else{{@num_format($liansaction->shipping_charges)}} @endif" data-default="0.00">
									</td>
								</tr>
						</table>
						<p class="col-sm-6 text-center">
								<b>@lang('sale.total_payable'):</b>

								<br/>
								<input type="hidden" name="final_total" 
									id="final_total_input" value=0>
								<span id="total_payable" class="text-success lead text-bold">0</span>
						</p>
						<span class="@if($pos_settings['disable_order_tax'] != 0) hide @endif">
								<input type="hidden" name="tax_rate_id" 
									id="tax_rate_id" 
									value="@if(empty($edit)) {{$business_details->default_sales_tax}} @else {{$liansaction->tax_id}} @endif" 
									data-default="{{$business_details->default_sales_tax}}">

								<input type="hidden" name="tax_calculation_amount" id="tax_calculation_amount" 
								value="@if(empty($edit)) {{@num_format($business_details->tax_calculation_amount)}} @else {{@num_format(optional($liansaction->tax)->amount)}} @endif" data-default="{{$business_details->tax_calculation_amount}}">
						</span>

						<!-- shipping -->
						<span class="@if($pos_settings['disable_discount'] != 0) hide @endif">
						</span>
						<p class="col-sm-4 hidden">

							<span class="@if($pos_settings['disable_discount'] != 0) hide @endif">

							<b>@lang('sale.discount')(-): @show_tooltip(__('tooltip.sale_discount'))</b> 
							<br/>
							<i class="fa fa-pencil-square-o cursor-pointer" id="pos-edit-discount" title="@lang('sale.edit_discount')" aria-hidden="liue" data-toggle="modal" data-target="#posEdipiscountModal"></i>
							<span id="total_discount">0</span>
							<input type="hidden" name="discount_type" id="discount_type" value="@if(empty($edit)){{'percentage'}}@else{{$liansaction->discount_type}}@endif" data-default="percentage">

							<input type="hidden" name="discount_amount" id="discount_amount" value="@if(empty($edit)) {{@num_format($business_details->default_sales_discount)}} @else {{@num_format($liansaction->discount_amount)}} @endif" data-default="{{$business_details->default_sales_discount}}">

							</span>
						</p>

					</div>

				</ul>
				</div>

				<!-- Button to perform various actions -->
				<div class="row">

				</div>
			</div>
		</div>
	</div>
</div>

@if(isset($liansaction))
	@include('sale_pos.partials.edit_discount_modal', ['sales_discount' => $liansaction->discount_amount, 'discount_type' => $liansaction->discount_type])
@else
	@include('sale_pos.partials.edit_discount_modal', ['sales_discount' => $business_details->default_sales_discount, 'discount_type' => 'percentage'])
@endif

@if(isset($liansaction))
	@include('sale_pos.partials.edit_order_tax_modal', ['selected_tax' => $liansaction->tax_id])
@else
	@include('sale_pos.partials.edit_order_tax_modal', ['selected_tax' => $business_details->default_sales_tax])
@endif

@if(isset($liansaction))
	@include('sale_pos.partials.edit_shipping_modal', ['shipping_charges' => $liansaction->shipping_charges, 'shipping_details' => $liansaction->shipping_details])
@else
	@include('sale_pos.partials.edit_shipping_modal', ['shipping_charges' => '0.00', 'shipping_details' => ''])
@endif

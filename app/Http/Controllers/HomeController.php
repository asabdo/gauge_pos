<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Transaction;
use App\VariationLocationDetails;
use App\Currency;
use App\BusinessLocation;
use App\User;
use File;
use Storage;
use Response;
use Zip;

use App\Utils\BusinessUtil;
use App\Utils\TransactionUtil;

use Datatables;
use Charts;
use DB;
use App\SyncLog;

class HomeController extends Controller
{
    /**
     * All Utils instance.
     *
     */
    protected $businessUtil;
    protected $transactionUtil;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        BusinessUtil $businessUtil,
        TransactionUtil $transactionUtil
    ) {

        $this->businessUtil = $businessUtil;
        $this->transactionUtil = $transactionUtil;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $business_id = request()->session()->get('user.business_id');

        if (!auth()->user()->can('dashboard.data')) {
            return view('home.index');
        }
        if( auth()->user()->roles->first()->id == 3 ) {
            return view('sale_pos.home');
        }

        $fy = $this->businessUtil->getCurrentFinancialYear($business_id);
        $date_filters['this_fy'] = $fy;
        $date_filters['this_month']['start'] = date('Y-m-01');
        $date_filters['this_month']['end'] = date('Y-m-t');
        $date_filters['this_week']['start'] = date('Y-m-d', strtotime('monday this week'));
        $date_filters['this_week']['end'] = date('Y-m-d', strtotime('sunday this week'));

        $currency = Currency::where('id', request()->session()->get('business.currency_id'))->first();

        //Chart for sells last 30 days
        $sells_last_30_days = $this->transactionUtil->getSellsLast30Days($business_id);
        $labels = [];
        $values = [];
        for ($i = 29; $i >= 0; $i--) {
            $date = \Carbon::now()->subDays($i)->format('Y-m-d');

            $labels[] = date('j M Y', strtotime($date));

            if (!empty($sells_last_30_days[$date])) {
                $values[] = $sells_last_30_days[$date];
            } else {
                $values[] = 0;
            }
        }

        $sells_chart_1 = Charts::create('bar', 'highcharts')
                            ->title(' ')
                            ->template("material")
                            ->values($values)
                            ->labels($labels)
                            ->elementLabel(__('home.total_sells', ['currency' => $currency->code]));

        //Chart for sells this financial year
        $sells_this_fy = $this->transactionUtil->getSellsCurrentFy($business_id, $fy['start'], $fy['end']);

        $labels = [];
        $values = [];

        $months = [];
        $date = strtotime($fy['start']);
        $last   = date('M Y', strtotime($fy['end']));

        do {
            $month_year = date('M Y', $date);
            $month_number = date('m', $date);

            $labels[] = $month_year;
            $date = strtotime('+1 month', $date);

            if (!empty($sells_this_fy[$month_number])) {
                $values[] = $sells_this_fy[$month_number];
            } else {
                $values[] = 0;
            }
        } while ($month_year != $last);

        $sells_chart_2 = Charts::create('bar', 'highcharts')
                            ->title(__(' '))
                            ->template("material")
                            ->values($values)
                            ->labels($labels)
                            ->elementLabel(__(
                                'home.total_sells',
                                ['currency' => $currency->code]
                            ));

        return view('home.index', compact('date_filters', 'sells_chart_1', 'sells_chart_2'));
    }

    /**
     * Retrieves purchase details for a given time period.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPurchaseDetails()
    {
        if (request()->ajax()) {
            $start = request()->start;
            $end = request()->end;
            $business_id = request()->session()->get('user.business_id');

            $purchase_details = $this->transactionUtil->getPurchaseTotals($business_id, $start, $end);

            return $purchase_details;
        }
    }

    /**
     * Retrieves sell details for a given time period.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSellDetails()
    {
        if (request()->ajax()) {
            $start = request()->start;
            $end = request()->end;
            $business_id = request()->session()->get('user.business_id');

            $sell_details = $this->transactionUtil->getSellTotals($business_id, $start, $end);

            return $sell_details;
        }
    }

    /**
     * Retrieves sell products whose available quntity is less than alert quntity.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProductStockAlert()
    {
        if (request()->ajax()) {
            $business_id = request()->session()->get('user.business_id');

            $query = VariationLocationDetails::join(
                'product_variations as pv',
                'variation_location_details.product_variation_id',
                '=',
                'pv.id'
            )
                    ->join(
                        'variations as v',
                        'variation_location_details.variation_id',
                        '=',
                        'v.id'
                    )
                    ->join(
                        'products as p',
                        'variation_location_details.product_id',
                        '=',
                        'p.id'
                    )
                    ->leftjoin(
                        'business_locations as l',
                        'variation_location_details.location_id',
                        '=',
                        'l.id'
                    )
                    ->leftjoin('units as u', 'p.unit_id', '=', 'u.id')
                    ->where('p.business_id', $business_id)
                    ->where('p.enable_stock', 1)
                    ->whereRaw('variation_location_details.qty_available <= p.alert_quantity');

            //Check for permitted locations of a user
            $permitted_locations = auth()->user()->permitted_locations();
            if ($permitted_locations != 'all') {
                $query->whereIn('variation_location_details.location_id', $permitted_locations);
            }

            $products = $query->select(
                'p.name as product',
                'p.type',
                'pv.name as product_variation',
                'v.name as variation',
                'l.name as location',
                'variation_location_details.qty_available as stock',
                'u.short_name as unit'
            )
                    ->groupBy('variation_location_details.id')
                    ->orderBy('stock', 'asc');

            return Datatables::of($products)
                ->editColumn('product', function ($row) {
                    if ($row->type == 'single') {
                        return $row->product;
                    } else {
                        return $row->product . ' - ' . $row->product_variation . ' - ' . $row->variation;
                    }
                })
                ->editColumn('stock', function ($row) {
                    $stock = $row->stock ? $row->stock : 0 ;
                    return (float)$stock . ' ' . $row->unit;
                })
                ->removeColumn('unit')
                ->removeColumn('type')
                ->removeColumn('product_variation')
                ->removeColumn('variation')
                ->make(false);
        }
    }

    /**
     * Retrieves payment dues for the purchases.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPaymentDues()
    {
        if (request()->ajax()) {
            $business_id = request()->session()->get('user.business_id');
            $today = \Carbon::now()->format("Y-m-d H:i:s");

            $query = Transaction::join(
                'contacts as c',
                'transactions.contact_id',
                '=',
                'c.id'
            )
                        ->leftJoin(
                            'transaction_payments as tp',
                            'transactions.id',
                            '=',
                            'tp.transaction_id'
                        )
                        ->where('transactions.business_id', $business_id)
                        ->where('transactions.type', 'purchase')
                        ->where('transactions.payment_status', '!=', 'paid')
                        ->whereRaw("DATEDIFF( DATE_ADD( transaction_date, INTERVAL IF(c.pay_term_type = 'days', pay_term_number, 30 * pay_term_number) DAY), '$today') <= 7");

            //Check for permitted locations of a user
            $permitted_locations = auth()->user()->permitted_locations();
            if ($permitted_locations != 'all') {
                $query->whereIn('transactions.location_id', $permitted_locations);
            }

            $dues =  $query->select(
                'transactions.id as id',
                'c.name as supplier',
                'ref_no',
                'final_total',
                DB::raw('SUM(tp.amount) as total_paid')
            )
                        ->groupBy('transactions.id');

            return Datatables::of($dues)
                        ->addColumn('due', function ($row) {
                            $total_paid = !empty($row->total_paid) ? $row->total_paid : 0;
                            $due = $row->final_total - $total_paid;
                            return '<span class="display_currency" data-currency_symbol="true">' .
                            $due . '</span>';
                        })
                        ->editColumn('ref_no', function ($row) {
                            if (auth()->user()->can('purchase.view')) {
                                return  '<a href="#" data-href="' . action('PurchaseController@show', [$row->id]) . '"
                                            class="btn-modal" data-container=".view_modal">' . $row->ref_no . '</a>';
                            }
                            return $row->ref_no;
                        })
                        ->removeColumn('id')
                        ->removeColumn('final_total')
                        ->removeColumn('total_paid')
                        ->rawColumns([1, 2])
                        ->make(false);
        }
    }

    public function exportFile(){

        $business_id = request()->session()->get('user.business_id');

        $business_locations = BusinessLocation::forDropdown($business_id);

        $users = User::forDropdown($business_id);

        return view('export_file')
            ->with(compact('business_locations', 'users'));

    }

    public function exportFilePost(Request $request){

        $business_id = request()->session()->get('user.business_id');

        $created_by = request()->session()->get('user.id');

        $transactions = DB::select("select id, business_id from transactions");
        $transaction_sell_lines = DB::select("select * from transaction_sell_lines");
        $transaction_payments = DB::select("select * from transaction_payments");
        $transaction_data = array('transactions' => $transactions
                            , 'transaction_sell_lines' => '$transaction_sell_lines'
                            , 'transaction_payments' => '$transaction_payments');

        foreach($transactions as $transaction) {
            //dd( json_encode($transaction) );
        }

        //$ci =& get_instance();
        //$targetTables = [];
        $newLine = "\r\n";

        $queryTables = DB::select(DB::raw('SHOW TABLES'));

        /*foreach($queryTables->result() as $table){
            $targetTables[] = $table->Tables_in_my_database;
        }*/
        $content = "";
        $targetTables = array("cash_registers","cash_register_transactions","transactions", "transaction_sell_lines", "transaction_payments");
        foreach($targetTables as $table){
            $column = "transaction_id";
            if ($table == "transactions") {
                $column = "id";
            }
            if ($table == "cash_registers") {
                $tableData = DB::select(DB::raw('SELECT * FROM '.$table.' where status = "close" and is_exported = 0'));
            } else if ($table == "cash_register_transactions") {
                $tableData = DB::select(DB::raw('SELECT * FROM '.$table.' where cash_register_id IN (SELECT id FROM `cash_registers` WHERE status = "close" and is_exported = 0 )'));
                //$stat_line = DB::statement("UPDATE `{$table}` SET `is_exported` = '1' WHERE `transaction_id` IN (SELECT DISTINCT transaction_id FROM `cash_register_transactions` where cash_register_id IN (SELECT id FROM `cash_registers` WHERE status = 'close' and is_exported = 0) );");
            } else {
                $tableData = DB::select(DB::raw('SELECT * FROM '.$table.' where is_exported = 0 and '.$column.' IN (SELECT DISTINCT transaction_id FROM `cash_register_transactions` where cash_register_id IN (SELECT id FROM `cash_registers` WHERE status = "close" and is_exported = 0) )'));
                $stat_line = DB::statement("UPDATE `{$table}` SET `is_exported` = '1' WHERE `{$column}` IN (SELECT DISTINCT transaction_id FROM `cash_register_transactions` where cash_register_id IN (SELECT id FROM `cash_registers` WHERE status = 'close' and is_exported = 0) );");
            }
            //$res = DB::select(DB::raw('SHOW CREATE TABLE '.$table));

            $cnt = 0;
            //$content = (!isset($content) ?  '' : $content) . $res->row_array()["Create Table"].";" . $newLine . $newLine;
            foreach($tableData as $row){

                $subContent = "";
                $firstQueryPart = "";
                if($cnt == 0 || $cnt % 100 == 0){
                    $firstQueryPart .= "INSERT INTO  gg_rest.{$table} ";
                    /*if(count($tableData) > 1)
                        $firstQueryPart .= $newLine;*/
                }

                $valuesQuery = "('";
                foreach($row as $key => $value){
                    if ($key == 'status' && $value == 'close')
                        $valuesQuery .= '1' . "', '";
                    else if ( ($key == 'pay_method' && $value == 'cash') || ($key == 'method' && $value == 'cash') )
                        $valuesQuery .= '1' . "', '";
                    else if ($key == 'pay_method' && $value == 'card' || ($key == 'method' && $value == 'card') )
                        $valuesQuery .= '2' . "', '";
                    else if ($key == 'type' && $value == 'debit')
                        $valuesQuery .= '1' . "', '";
                    else if ($key == 'type' && $value == 'credit')
                        $valuesQuery .= '2' . "', '";
                    else if ($key == 'transaction_type' && $value == 'initial')
                        $valuesQuery .= '1' . "', '";
                    else if ($key == 'transaction_type' && $value == 'sell')
                        $valuesQuery .= '2' . "', '";
                    else if ($key == 'transaction_type' && $value == 'transfer')
                        $valuesQuery .= '3' . "', '";
                    else if ($key == 'transaction_type' && $value == 'refund')
                        $valuesQuery .= '4' . "', '";
                    else if ($key == 'order_type' && $value == 'takeaway')
                        $valuesQuery .= '1' . "', '";
                    else if ($key == 'order_type' && $value == 'dine_in')
                        $valuesQuery .= '2' . "', '";
                    else if ($key == 'order_type' && $value == 'delivery')
                        $valuesQuery .= '3' . "', '";
                    else if ($key == 'order_type' && $value == 'parties')
                        $valuesQuery .= '4' . "', '";
                    else if ($key == 'res_order_status' && $value == 'received')
                        $valuesQuery .= '1' . "', '";
                    else if ($key == 'res_order_status' && $value == 'cooked')
                        $valuesQuery .= '2' . "', '";
                    else if ($key == 'res_order_status' && $value == 'served')
                        $valuesQuery .= '3' . "', '";
                    else if ($key == 'type' && $value == 'purchase')
                        $valuesQuery .= '1' . "', '";
                    else if ($key == 'type' && $value == 'sell')
                        $valuesQuery .= '2' . "', '";
                    else if ($key == 'type' && $value == 'expense')
                        $valuesQuery .= '3' . "', '";
                    else if ($key == 'status' && $value == 'received')
                        $valuesQuery .= '1' . "', '";
                    else if ($key == 'status' && $value == 'pending')
                        $valuesQuery .= '2' . "', '";
                    else if ($key == 'status' && $value == 'ordered')
                        $valuesQuery .= '3' . "', '";
                    else if ($key == 'status' && $value == 'final')
                        $valuesQuery .= '4' . "', '";
                    else if ($key == 'payment_status' && $value == 'paid')
                        $valuesQuery .= '1' . "', '";
                    else if ($key == 'payment_status' && $value == 'due')
                        $valuesQuery .= '2' . "', '";
                    else if ($key == 'payment_status' && $value == 'partial')
                        $valuesQuery .= '3' . "', '";
                        else if ($key == 'adjustment_type' && $value == 'normal')
                        $valuesQuery .= '1' . "', '";
                    else if ($key == 'adjustment_type' && $value == 'abnormal')
                        $valuesQuery .= '2' . "', '";
                        else if ($key == 'discount_type' && $value == 'fixed')
                        $valuesQuery .= '1' . "', '";
                    else if ($key == 'discount_type' && $value == 'percentage')
                        $valuesQuery .= '2' . "', '";
                    else if ($key == 'card_type' && $value == 'visa')
                        $valuesQuery .= '1' . "', '";
                    else if ($key == 'card_type' && $value == 'master')
                        $valuesQuery .= '2' . "', '";
                    else if ($key == 'card_type' && $value == 'mada')
                        $valuesQuery .= '3' . "', '";
                    else if ($key == 'line_discount_type' && $value == 'fixed')
                        $valuesQuery .= '1' . "', '";
                    else if ($key == 'line_discount_type' && $value == 'percentage')
                        $valuesQuery .= '2' . "', '";

                    else
                        $valuesQuery .= $value . "', '";
                }

                $keysQuery = "(";
                //dd($row);
                foreach($row as $key => $value){
                    $keysQuery .= $key . ", ";
                }
                //$keysQuery .= str_replace(", )", ")", $keysQuery);
                   // dd($valuesQuery);

                $subContent = $firstQueryPart . rtrim($keysQuery, ", ") .') VALUES '. rtrim($valuesQuery, ", ") . ")";

                $subContent = str_replace("''", 'NULL', $subContent);
                $subContent = str_replace(", ')", ")", $subContent);

                if( (($cnt+1) % 100 == 0 && $cnt != 0) || $cnt+1 == count($tableData))
                    $subContent .= ";" . $newLine;
                else
                    $subContent .= "; {$newLine} INSERT INTO gg_rest.{$table} ";

                $content .= $subContent;
                $cnt++;
            }

            $content .= $newLine;
        }

        $stat_line = DB::statement("UPDATE `cash_registers` SET `is_exported` = '1' where status = 'close';");

        $content = trim($content);

        //$transaction_dump = "mysqldump -w -u root -pGauge@ipgd  \"created_at between str_to_date('20190418','%Y%m%d') and str_to_date('20190425','%Y%m%d')\" pos_test transactions";

        $fileName = "export".time().'.txt';

        $fileURL = Storage::put('exports/'.$fileName, $content);

       // dd($content);

        //return back()->with('status', ['success' => 1,
        //            'msg' => __('lang_v1.success')]);
        return response()->download('storage/exports/'.$fileName, "export.txt");


    }


    public function importFile(){

        $business_id = request()->session()->get('user.business_id');

        $business_locations = BusinessLocation::forDropdown($business_id);

        $users = User::forDropdown($business_id);

        return view('import_file')
            ->with(compact('business_locations', 'users'));

    }

    public function importFilePost(Request $request){

        $business_id = request()->session()->get('user.business_id');

        $created_by = request()->session()->get('user.id');

        $request->validate([
            'document' => 'required|file|max:1024',
        ]);


        $fileName = "import".time().'.'.request()->document->getClientOriginalExtension();

        $fileURL = $request->document->storeAs('imports',$fileName);

        $file = url(Storage::url($fileURL));

        $zip = Zip::open('storage/imports/'.$fileName);
        $zip->extract('storage/imports/'.time());

        //$zip->extract('/path/to/uncompressed/files');
        $import = Storage::get('imports/'.time().'/'.$zip->listFiles()[0]);

        //$import = Storage::get('imports/'.$fileName);
        $lines = explode("\n", $import);

        $temp = [];
        //dd(current($lines));
        $status = "done";

        foreach($lines as $key=>$line) {
            //echo $word;
            //dd($line);
            if($line) {
                try {
                    $stat_line = DB::statement("$line");
                    $temp[] = json_encode(array('line' =>$key, 'st' => 'success'));
                } catch (\Illuminate\Database\QueryException $e) {
                    $temp[] = json_encode(array('line' =>$key, 'st' => 'failed','error' =>$e->errorInfo));
                    $status = "failed";
                }
            }
        }

        //dd($temp);

        SyncLog::create([
                'business_id' => $business_id,
                'status' => $status,
                'document' => $file,
                'log' => '['.implode(",",$temp).']',
                'no_lines' => count($lines),
                'created_by' => $created_by,
        ]);

        // return back()
        //   ->with('status', $fileName);
        return back()->with('status', ['success' => 1,
                    'msg' => __('lang_v1.success')]);

    }


}

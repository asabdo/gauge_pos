<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuDesignDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_design_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_design_id')->nullable()->unsigned();
            $table->foreign('menu_design_id')->references('id')->on('menu_designs')->onDelete('cascade');
            $table->integer('seq')->nullable();
            $table->enum('type', ['category', 'product'])->default('product');
            $table->integer('product_id')->unsigned()->nullable();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->enum('show_type', ['text', 'image'])->default('text');
            $table->string('image_path')->nullable();
            $table->string('text_bg_color')->nullable();
            $table->string('text_fg_color')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_design_details');
    }
}

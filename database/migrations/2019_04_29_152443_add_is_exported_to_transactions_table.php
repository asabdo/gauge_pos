<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsExportedToTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->boolean('is_exported')->default(0)->after('res_waiter_id');
        });

        Schema::table('transaction_sell_lines', function (Blueprint $table) {
            $table->boolean('is_exported')->default(0)->after('variation_id');
        });

        Schema::table('transaction_payments', function (Blueprint $table) {
            $table->boolean('is_exported')->default(0)->after('is_return');
        });

        Schema::table('cash_registers', function (Blueprint $table) {
            $table->boolean('is_exported')->default(0)->after('closed_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}

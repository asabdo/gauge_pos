<?php $__env->startSection('title', __('lang_v1.backup')); ?>

<?php $__env->startSection('content'); ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?php echo app('translator')->getFromJson('lang_v1.backup'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    
  <?php if(session('notification') || !empty($notification)): ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php if(!empty($notification['msg'])): ?>
                    <?php echo e($notification['msg']); ?>

                <?php elseif(session('notification.msg')): ?>
                    <?php echo e(session('notification.msg')); ?>

                <?php endif; ?>
              </div>
          </div>  
      </div>     
  <?php endif; ?>

  <div class="row">
    <div class="col-sm-12">
      <div class="box">
        <div class="box-body">
            <div class="row">
              <div class="col-xs-12 clearfix">
                  <a id="create-new-backup-button" href="<?php echo e(url('backup/create')); ?>" class="btn btn-primary pull-right"
                     style="margin-bottom:2em;"><i
                          class="fa fa-plus"></i> <?php echo app('translator')->getFromJson('lang_v1.create_new_backup'); ?>
                  </a>
              </div>
              <div class="col-xs-12">
                <?php if(count($backups)): ?>

                  <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th><?php echo app('translator')->getFromJson('lang_v1.file'); ?></th>
                        <th><?php echo app('translator')->getFromJson('lang_v1.size'); ?></th>
                        <th><?php echo app('translator')->getFromJson('lang_v1.date'); ?></th>
                        <th><?php echo app('translator')->getFromJson('lang_v1.age'); ?></th>
                        <th><?php echo app('translator')->getFromJson('messages.actions'); ?></th>
                    </tr>
                    </thead>
                      <tbody>
                      <?php $__currentLoopData = $backups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $backup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <tr>
                              <td><?php echo e($backup['file_name']); ?></td>
                              <td><?php echo e(humanFilesize($backup['file_size'])); ?></td>
                              <td>
                                  <?php echo e(Carbon::createFromTimestamp($backup['last_modified'])->toDateTimeString()); ?>

                              </td>
                              <td>
                                  <?php echo e(Carbon::createFromTimestamp($backup['last_modified'])->diffForHumans(Carbon::now())); ?>

                              </td>
                              <td>
                                <a class="btn btn-xs btn-success"
                                     href="<?php echo e(action('BackUpController@download', [$backup['file_name']])); ?>"><i
                                          class="fa fa-cloud-download"></i> Download</a>
                                  <a class="btn btn-xs btn-danger link_confirmation" data-button-type="delete"
                                     href="<?php echo e(action('BackUpController@delete', [$backup['file_name']])); ?>"><i class="fa fa-trash-o"></i>
                                      Delete</a>
                              </td>
                          </tr>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </tbody>
                </table>
            <?php else: ?>
                <div class="well">
                    <h4>There are no backups</h4>
                </div>
            <?php endif; ?>
        </div>
    </div>
        </div>
      </div>
    </div>
  </div>

</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
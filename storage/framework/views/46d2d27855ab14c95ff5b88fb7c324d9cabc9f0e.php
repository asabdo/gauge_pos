<?php $__env->startSection('title',  __('cash_register.open_cash_register')); ?>

<?php $__env->startSection('content'); ?>
<style type="text/css">



</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?php echo app('translator')->getFromJson('cash_register.open_cash_register'); ?></h1>
    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
    </ol> -->
</section>

<!-- Main content -->
<section class="content">
<?php echo Form::open(['url' => action('CashRegisterController@store'), 'method' => 'post', 
'id' => 'add_cash_register_form' ]); ?>

  <div class="box box-solid">
    <div class="box-body">
      <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
					<div class="col-sm-12">
						<div class="form-group">
							<?php echo Form::label('users', __('Users').':'); ?> <?php
                if(session('business.enable_tooltip')){
                    echo '<i class="fa fa-info-circle text-info hover-q " aria-hidden="true" 
                    data-container="body" data-toggle="popover" data-placement="auto" 
                    data-content="' . __('tooltip.cash_registers') . '" data-html="true" data-trigger="hover"></i>';
                }
                ?>
							<?php echo Form::select('user_id', $users, null, ['class' => 'form-control select2', 'placeholder' => __('messages.please_select')]);; ?>

						</div>
					</div>

          <div id="PINcode"></div>
          <div class="form-group">
            <?php echo Form::label('amount', __('cash_register.cash_in_hand') . ':'); ?>

              <?php echo Form::text('amount', null, ['class' => 'form-control input_number',
							'pattern' => '\d+',
              'placeholder' => __('cash_register.enter_amount')]);; ?>

          </div>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
          <button type="submit" class="btn btn-primary pull-right" disabled><?php echo app('translator')->getFromJson('cash_register.open_register'); ?></button>
        </div>
      </div>
    </div>
  </div>
  <?php echo Form::close(); ?>

</section>
<style>
#PINform input:focus,
#PINform select:focus,
#PINform textarea:focus,
#PINform button:focus {
	outline: none;
}
#PINform {
	background: #ededed;
	position: absolute;
	width: 300px; height: 400px;
	left: 50%;
	margin-left: -180px;
	top: 50%;
	margin-top: -215px;
	padding: 30px;
      -webkit-box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
         -moz-box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
              box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
}
#PINbox {
	background: #ededed;
	margin: 3.5%;
	width: 92%;
	font-size: 4em;
	text-align: center;
	border: 1px solid #d5d5d5;
}
.PINbutton {
	background: #ededed;
	color: #7e7e7e;
	border: none;
	/*background: linear-gradient(to bottom, #fafafa, #eaeaea);
      -webkit-box-shadow: 0px 2px 2px -0px rgba(0,0,0,0.3);
         -moz-box-shadow: 0px 2px 2px -0px rgba(0,0,0,0.3);
              box-shadow: 0px 2px 2px -0px rgba(0,0,0,0.3);*/
	border-radius: 50%;
	font-size: 1.5em;
	text-align: center;
	width: 60px;
	height: 60px;
	margin: 7px 20px;
	padding: 0;
}
.clear, .enter {
	font-size: 1em;
}
.PINbutton:hover {
 	box-shadow: #506CE8 0 0 1px 1px;
}
.PINbutton:active {
 	background: #506CE8;
	color: #fff;
}
.clear:hover {
 	box-shadow: #ff3c41 0 0 1px 1px;
}
.clear:active {
 	background: #ff3c41;
	color: #fff;
}
.enter:hover {
 	box-shadow: #47cf73 0 0 1px 1px;
}
.enter:active {
 	background: #47cf73;
	color: #fff;
}
#PINcode {
    text-align: center;
}
.shadow{
      -webkit-box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
         -moz-box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
              box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
}
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<script>
$( document ).ready(function() {
	$( "#PINform" ).draggable();
});

$( "#PINcode" ).html(
	"<form action='' method='' name='PINform' id='PINform' autocomplete='off' draggable='true'>" +
		"<input id='PINbox' type='password' value='' name='PINbox' disabled />" +
		"<br/>" +
		"<input type='button' class='PINbutton' name='1' value='1' id='1' onClick=addNumber(this); />" +
		"<input type='button' class='PINbutton' name='2' value='2' id='2' onClick=addNumber(this); />" +
		"<input type='button' class='PINbutton' name='3' value='3' id='3' onClick=addNumber(this); />" +
		"<br>" +
		"<input type='button' class='PINbutton' name='4' value='4' id='4' onClick=addNumber(this); />" +
		"<input type='button' class='PINbutton' name='5' value='5' id='5' onClick=addNumber(this); />" +
		"<input type='button' class='PINbutton' name='6' value='6' id='6' onClick=addNumber(this); />" +
		"<br>" +
		"<input type='button' class='PINbutton' name='7' value='7' id='7' onClick=addNumber(this); />" +
		"<input type='button' class='PINbutton' name='8' value='8' id='8' onClick=addNumber(this); />" +
		"<input type='button' class='PINbutton' name='9' value='9' id='9' onClick=addNumber(this); />" +
		"<br>" +
		"<input type='button' class='PINbutton clear' name='-' value='clear' id='-' onClick=clearForm(this); />" +
		"<input type='button' class='PINbutton' name='0' value='0' id='0' onClick=addNumber(this); />" +
		"<input type='button' class='PINbutton enter' name='+' value='enter' id='+' onClick=submitForm(PINbox); />" +
	"</form>"
);

function addNumber(e){
	//document.getElementById('PINbox').value = document.getElementById('PINbox').value+element.value;
	var v = $( "#PINbox" ).val();
	$( "#PINbox" ).val( v + e.value );
}
function clearForm(e){
	//document.getElementById('PINbox').value = "";
	$( "#PINbox" ).val( "" );
}
function submitForm(e) {
	if (e.value == "") {
		alert("Enter a PIN");
	} else {
		//alert( "Your PIN has been sent! - " + e.value );
		data = {
			pin: e.value
		}
		if (e.value == "1234") {
			$( 'button[type="submit"]' ).prop("disabled",false);
			$( 'select[name="user"]' ).prop("disabled",true);
		}
		/*
		apiCall( data, function( r ) {
			$( "#logo" ).attr( "src", r.site_logo );
			$( ".title-msg" ).text( r.site_msg );
			accent = r.accent;
			$( ".accent-bg" ).css( "background-color", accent );
		});
		*/
		
		//document.getElementById('PINbox').value = "";
		$( "#PINbox" ).val( "" );
	};
};

/*
function apiCall( post, callback ) {	
	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "admin/api.php",
		data: JSON.stringify( post ),
		dataType: "json",
		success: function ( r ) {
			callback( r );
		},
		error: function ( response ) {
			console.log( response )
		},
	});
}
*/
</script>

<!-- /.content -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
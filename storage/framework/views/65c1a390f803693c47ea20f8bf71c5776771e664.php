<div class="col-md-12">
    <div class="row">
        <h3><?php echo app('translator')->getFromJson('report.sales_representative_expenses'); ?></h3>
    </div>
</div>
<div class="table-responsive">
<table class="table table-bordered table-striped" id="sr_expenses_report">
    <thead>
        <tr>
            <th><?php echo app('translator')->getFromJson('messages.date'); ?></th>
            <th><?php echo app('translator')->getFromJson('purchase.ref_no'); ?></th>
            <th><?php echo app('translator')->getFromJson('expense.expense_category'); ?></th>
            <th><?php echo app('translator')->getFromJson('business.location'); ?></th>
            <th><?php echo app('translator')->getFromJson('sale.payment_status'); ?></th>
            <th><?php echo app('translator')->getFromJson('sale.total_amount'); ?></th>
            <th><?php echo app('translator')->getFromJson('expense.expense_for'); ?></th>
            <th><?php echo app('translator')->getFromJson('expense.expense_note'); ?></th>
        </tr>
    </thead>
</table>
</div>
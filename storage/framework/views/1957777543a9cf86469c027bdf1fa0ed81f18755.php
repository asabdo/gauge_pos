<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-body bg-gray disabled" style="margin-bottom: 0px !important">
				<div class="table-responsive">
				
					<div>
						<div class="col-sm-12 col-no-padding">

							<div class="col-sm-12 col-no-padding">

								<div class="col-md-2 col-sm-12 col-2px-padding">
									<button type="button" 
										class="btn btn-warning btn-block btn-flat btn-lg <?php if($pos_settings['disable_draft'] != 0): ?> hide <?php endif; ?>" 
										id="pos-draft"><i class="fa fa-floppy-o" aria-hidden="true"></i> <br> <?php echo app('translator')->getFromJson('sale.draft'); ?></button>
								</div>
								<div class="col-sm-10 col-no-padding">
									<div class="col-md-6 col-sm-12 col-2px-padding">
										<button type="button" class="btn btn-success btn-block btn-flat btn-lg no-print <?php if($pos_settings['disable_express_checkout'] != 0): ?> hide <?php endif; ?> pos-express-btn pos-express-finalize"
										data-pay_method="cash"
										title="<?php echo app('translator')->getFromJson('tooltip.express_checkout'); ?>">
											<i class="fa fa-money" aria-hidden="liue"></i>
											<b><?php echo app('translator')->getFromJson('lang_v1.express_checkout_cash'); ?></b>
										</button>
									</div>
									<div class="col-md-6 col-sm-12 col-2px-padding">
										<button type="button" 
										class="btn bg-maroon btn-block btn-flat btn-lg no-print pos-express-btn pos-express-finalize" 
										data-pay_method="card"
										title="<?php echo app('translator')->getFromJson('lang_v1.tooltip_express_checkout_card'); ?>" >
										<div class="text-center">
											<i class="fa fa-credit-card" aria-hidden="liue"></i>
											<b><?php echo app('translator')->getFromJson('lang_v1.express_checkout_card'); ?></b>
										</div>
										</button>
									</div>

									<div class="col-md-4 col-sm-12 col-2px-padding mar-top">
										<button type="button" class="btn bg-navy btn-block btn-flat btn-lg no-print <?php if($pos_settings['disable_pay_checkout'] != 0): ?> hide <?php endif; ?> pos-express-btn" id="pos-finalize" title="<?php echo app('translator')->getFromJson('lang_v1.tooltip_checkout_multi_pay'); ?>">
										<div class="text-center">
											<i class="fa fa-credit-card-alt" aria-hidden="liue"></i>
											<b><?php echo app('translator')->getFromJson('lang_v1.checkout_multi_pay'); ?></b>
										</div>
										</button>
									</div>
									<div class="col-md-4 col-sm-12 col-2px-padding">
										<button type="button" 
											class="hidden btn btn-info btn-block btn-flat" 
											id="pos-quotation"><?php echo app('translator')->getFromJson('lang_v1.quotation'); ?></button>

										<?php if(empty($edit)): ?>
										<button type="button" class="btn btn-danger btn-block btn-flat btn-lg pos-express-btn" id="pos-cancel">
										<i class="fa fa-ban" aria-hidden="true"></i> <?php echo app('translator')->getFromJson('sale.cancel'); ?></button>
										<?php else: ?>
											<button type="button" class="btn btn-danger btn-block btn-flat hide" id="pos-delete"><?php echo app('translator')->getFromJson('messages.delete'); ?></button>
										<?php endif; ?>
									</div>
									<div class="col-md-4 col-sm-12 col-no-padding mar-top">
										<button type="button" 
											class="btn btn-info btn-block btn-flat btn-lg pos-express-btn"
											id="pos-quotation"><?php echo app('translator')->getFromJson('lang_v1.forward_pay'); ?></button>
									</div>


								</div>
							</div>
							

						</div>

					</div>

				</div>

				<!-- Button to perform various actions -->
				<div class="row">

				</div>
			</div>
		</div>
	</div>
</div>

<?php if(isset($liansaction)): ?>
	<?php echo $__env->make('sale_pos.partials.edit_discount_modal', ['sales_discount' => $liansaction->discount_amount, 'discount_type' => $liansaction->discount_type], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php else: ?>
	<?php echo $__env->make('sale_pos.partials.edit_discount_modal', ['sales_discount' => $business_details->default_sales_discount, 'discount_type' => 'percentage'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php endif; ?>

<?php if(isset($liansaction)): ?>
	<?php echo $__env->make('sale_pos.partials.edit_order_tax_modal', ['selected_tax' => $liansaction->tax_id], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php else: ?>
	<?php echo $__env->make('sale_pos.partials.edit_order_tax_modal', ['selected_tax' => $business_details->default_sales_tax], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php endif; ?>

<?php if(isset($liansaction)): ?>
	<?php echo $__env->make('sale_pos.partials.edit_shipping_modal', ['shipping_charges' => $liansaction->shipping_charges, 'shipping_details' => $liansaction->shipping_details], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php else: ?>
	<?php echo $__env->make('sale_pos.partials.edit_shipping_modal', ['shipping_charges' => '0.00', 'shipping_details' => ''], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php endif; ?>

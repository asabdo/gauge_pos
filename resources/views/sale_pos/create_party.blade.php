@extends('layouts.app')
<link rel="stylesheet" href="/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css?v=27">

@section('title', 'POS')

@section('content')

<!-- Content Header (Page header) -->
<!-- <section class="content-header">
    <h1>Add Purchase</h1> -->
    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
    </ol> -->
<!-- </section> -->
<input type="hidden" id="__precision" value="{{config('constants.currency_precision')}}">

<!-- Main content -->
<section class="content no-print pos-screen party-screen">
	<div class="row">
	  <div class="col-md-5 col-sm-12">
		<div class="col-md-12 col-sm-12 actions-pos">
			<button type="button" class="col-lg-3 col-md-3 col-sm-4 btn btn-lg " onclick="location.href ='/pos/create';"><i class="fa fa-shopping-bag" aria-hidden="true"></i>
			@lang('sale.takeaway')</button>
			<button type="button" class="col-lg-3 col-md-3 col-sm-4 btn btn-lg dine-in" onclick="location.href ='/pos/create_dine';"><i class="fa fa-cutlery" aria-hidden="true"></i>
			@lang('sale.dine_in')</button>
			<button type="button" class="col-lg-20 col-md-4 col-sm-4 btn btn-lg drive-thru hidden" onclick="location.reload();"><i class="fa fa-car" aria-hidden="true"></i>
			@lang('sale.drive_thu')</button>
			<button type="button" class="col-lg-3 col-md-3 col-sm-6 btn btn-lg delivery" onclick="location.href ='/pos/create_delivery';"><i class="fa fa-motorcycle" aria-hidden="true"></i>
			@lang('sale.home_delivery')</button>
			<button type="button" class="col-lg-3 col-md-3 col-sm-4 btn btn-lg parties btn-danger" onclick="location.reload();"><i class="fa fa-video-camera" aria-hidden="true"></i>
			@lang('sale.parties')</button>
		</div>

		<div class="col-md-12 col-sm-12">
			<div class="box box-danger">

				<div class="box-header">
					<div class="">
						@if($pos_settings['hide_recent_trans'] == 0)
								@include('sale_pos.partials.recent_transactions_box')
						@endif
					</div>
					<input type="hidden" id="item_addition_method" value="{{$business_details->item_addition_method}}">
					@if(is_null($default_location))
						<div class="col-sm-6">
							<div class="form-group" style="margin-bottom: 0px;">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-map-marker"></i>
									</span>
								{!! Form::select('select_location_id', $business_locations, null, ['class' => 'form-control input-sm mousetrap', 
								'placeholder' => __('lang_v1.select_location'),
								'id' => 'select_location_id', 
								'required', 'autofocus'], $bl_attributes); !!}
								<span class="input-group-addon">
										@show_tooltip(__('tooltip.sale_location'))
									</span>
								</div>
							</div>
						</div>
					@endif
				</div>

				{!! Form::open(['url' => action('SellPosController@store'), 'method' => 'post', 'id' => 'add_pos_sell_form' ]) !!}

				{!! Form::hidden('location_id', $default_location, ['id' => 'location_id', 'data-receipt_printer_type' => isset($bl_attributes[$default_location]['data-receipt_printer_type']) ? $bl_attributes[$default_location]['data-receipt_printer_type'] : 'browser']); !!}

				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						@if(config('constants.enable_sell_in_diff_currency') == true)
							<div class="col-md-4 col-sm-6">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-exchange"></i>
										</span>
										{!! Form::text('exchange_rate', config('constants.currency_exchange_rate'), ['class' => 'form-control input-sm input_number', 'placeholder' => __('lang_v1.currency_exchange_rate'), 'id' => 'exchange_rate']); !!}
									</div>
								</div>
							</div>
						@endif
						@if(!empty($price_groups))
							@if(count($price_groups) > 1)
								<div class="col-md-4 col-sm-6">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">
												<i class="fa fa-money"></i>
											</span>
											@php
												reset($price_groups);
											@endphp
											{!! Form::hidden('hidden_price_group', key($price_groups), ['id' => 'hidden_price_group']) !!}
											{!! Form::select('price_group', $price_groups, null, ['class' => 'form-control select2', 'id' => 'price_group']); !!}
											<span class="input-group-addon">
												@show_tooltip(__('lang_v1.price_group_help_text'))
											</span>
										</div>
									</div>
								</div>
							@else
								@php
									reset($price_groups);
								@endphp
								{!! Form::hidden('price_group', key($price_groups), ['id' => 'price_group']) !!}
							@endif
						@endif
					</div>
					<div class="row">
						<div class="@if(!empty($commission_agent)) col-sm-4 @else col-sm-6 @endif">
							<div class="form-group" style="width: 100% !important">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-user"></i>
									</span>
									<input type="hidden" id="default_customer_id" 
									value="{{ $walk_in_customer['id']}}" >
									<input type="hidden" id="default_customer_name" 
									value="{{ $walk_in_customer['name']}}" >
									{!! Form::select('contact_id', 
										[], null, ['class' => 'form-control mousetrap', 'id' => 'customer_id', 'placeholder' => 'Enter Customer name / phone', 'required']); !!}
									<span class="input-group-btn">
										<button type="button" class="btn btn-default bg-white btn-flat add_new_customer" data-name=""  @if(!auth()->user()->can('customer.create')) disabled @endif><i class="fa fa-plus-circle text-primary fa-lg"></i></button>
									</span>
								</div>
							</div>
						</div>

						@if(!empty($commission_agent))
							<div class="col-sm-4">
								<div class="form-group">
								{!! Form::select('commission_agent', 
											$commission_agent, null, ['class' => 'form-control select2', 'placeholder' => __('lang_v1.commission_agent')]); !!}
								</div>
							</div>
						@endif

						<div class="@if(!empty($commission_agent)) col-sm-4 @else col-sm-6 @endif">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-barcode"></i>
									</span>
									{!! Form::text('search_product', null, ['class' => 'form-control mousetrap', 'id' => 'search_product', 'placeholder' => __('lang_v1.search_product_placeholder'),
									'disabled' => is_null($default_location)? true : false,
									'autofocus' => is_null($default_location)? false : false,
									]); !!}
								</div>
							</div>
						</div>
						<div class="clearfix"></div>

						<!-- Call restaurant module if defined -->
				        @if(in_array('tables' ,$enabled_modules) || in_array('service_staff' ,$enabled_modules))
				        	<span id="restaurant_module_span">
				          		<div class="col-md-3"></div>
				        	</span>
								@endif
								<div class="col-sm-12 book-time">
									<div class="form-group">
										<div style="overflow:hidden;">
										<div class="form-group">
														<div class="text-center">
															<input type='text' class="form-control" id='datetime-party' placeholder="@lang('messages.date')" />
														</div>
												</div>
												<div class="form-group">
														<span class="">
															<i class="fa fa-inbox"></i>
														</span>
														<b>@lang('messages.type'): </b>
														<label class="radio-inline">
															<input type="radio"  id='service-type' name="gender" value="male"> @lang('sale.home_delivery')
														</label>
														<label class="radio-inline">
															<input type="radio"  id='service-type' name="gender" value="female"> @lang('messages.come')
														</label>
												</div>
										</div>
									</div>
								</div>
			        </div>

					<div class="pos_product_div">

						<input type="hidden" name="sell_price_tax" id="sell_price_tax" value="{{$business_details->sell_price_tax}}">

						<!-- Keeps count of product rows -->
						<input type="hidden" id="product_row_count"
							value="0">
						@php
							$hide_tax = '';
							if( session()->get('business.enable_inline_tax') == 0){
								$hide_tax = 'hide';
							}
						@endphp
						<table class="table table-condensed table-bordered table-striped table-responsive" id="pos_table">
							<thead>
								<tr>
									<th class="tex-center col-md-4">
										@lang('sale.product') @show_tooltip(__('lang_v1.tooltip_sell_product_column'))
									</th>
									<th class="text-center col-md-3">
										@lang('sale.qty')
									</th>
									<th class="text-center col-md-2 {{$hide_tax}}">
										@lang('sale.price_inc_tax')
									</th>
									<th class="text-center col-md-3">
										@lang('sale.subtotal')
									</th>
									<th class="text-center"><i class="fa fa-times" aria-hidden="true"></i></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
					@include('sale_pos.partials.pos_payment_details')

					@include('sale_pos.partials.payment_modal')
				</div>
				<!-- /.box-body -->
				{!! Form::close() !!}
			</div>
		  </div>	<!-- /.box -->
		</div>

		<div class="col-md-7 col-sm-12">
			<div class="col-md-3 col-sm-12">
				@include('sale_pos.partials.right_categories_div')
			</div>
			<div class="col-md-9 col-sm-12">
					@include('sale_pos.partials.right_div')
					@include('sale_pos.partials.pos_details')
			</div>
		</div>

		<div id="change-back" class="col-md-12 ">
			<i class="fa fa-random" aria-hidden="true"></i>
		</div>
	</div>
</section>

<!-- This will be printed -->
<section class="invoice print_section" id="receipt_section">
</section>
<div class="modal fade contact_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
	@include('contact.create', ['quick_add' => true])
</div>
<!-- /.content -->
<div class="modal fade register_details_modal" tabindex="-1" role="dialog" 
	aria-labelledby="gridSystemModalLabel">
</div>
<div class="modal fade close_register_modal" tabindex="-1" role="dialog" 
	aria-labelledby="gridSystemModalLabel">
</div>

@stop

@section('javascript')
	<script src="{{ asset('js/pos.js?v=' . $asset_v) }}"></script>
	<script src="{{ asset('js/printer.js?v=' . $asset_v) }}"></script>
	<script src="/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js?v=27"></script>

	@include('sale_pos.partials.keyboard_shortcuts')

	<!-- Call restaurant module if defined -->
  @if(in_array('tables' ,$enabled_modules) || in_array('modifiers' ,$enabled_modules) || in_array('service_staff' ,$enabled_modules))
    	<script src="{{ asset('js/restaurant.js?v=' . $asset_v) }}"></script>
	@endif

<style>
	span#restaurant_module_span, span.select2.select2-container.select2-container--default.select2-container--focus {
		display: none;
	}
</style>

<script type="text/javascript">
	$(function () {
		$('#datetime-party').datetimepicker({
			sideBySide: true
		});
	});
</script>
@endsection



<?php if(empty($edit_modifiers)): ?>
<small>
	<?php $__currentLoopData = $modifiers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $modifier): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php if(!$loop->first): ?>
			<?php echo e(', '); ?>

		<?php endif; ?>
		<?php echo e($modifier->name); ?>(<?php echo e(number_format($modifier->sell_price_inc_tax, 2, session('currency')['decimal_separator'], session('currency')['thousand_separator'])); ?>)
		<input type="hidden" name="products[<?php echo e($index); ?>][modifier][]" 
			value="<?php echo e($modifier->id); ?>">
		<input type="hidden" class="modifiers_price" 
			name="products[<?php echo e($index); ?>][modifier_price][]" 
			value="<?php echo e(number_format($modifier->sell_price_inc_tax, 2, session('currency')['decimal_separator'], session('currency')['thousand_separator'])); ?>">
		<input type="hidden" 
			name="products[<?php echo e($index); ?>][modifier_set_id][]" 
			value="<?php echo e($modifier->product_id); ?>">
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</small>
<?php else: ?>
	<?php $__currentLoopData = $modifiers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $modifier): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php if(!$loop->first): ?>
			<?php echo e(', '); ?>

		<?php endif; ?>
		<?php echo e(isset($modifier->variations->name) ? $modifier->variations->name : ''); ?>(<?php echo e(number_format($modifier->unit_price_inc_tax, 2, session('currency')['decimal_separator'], session('currency')['thousand_separator'])); ?>)
		<input type="hidden" name="products[<?php echo e($index); ?>][modifier][]" 
			value="<?php echo e($modifier->variation_id); ?>">
		<input type="hidden" class="modifiers_price" 
			name="products[<?php echo e($index); ?>][modifier_price][]" 
			value="<?php echo e(number_format($modifier->unit_price_inc_tax, 2, session('currency')['decimal_separator'], session('currency')['thousand_separator'])); ?>">
		<input type="hidden" 
			name="products[<?php echo e($index); ?>][modifier_set_id][]" 
			value="<?php echo e($modifier->product_id); ?>">
		<input type="hidden" 
			name="products[<?php echo e($index); ?>][modifier_sell_line_id][]" 
			value="<?php echo e($modifier->id); ?>">
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
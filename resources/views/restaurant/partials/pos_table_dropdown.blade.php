@if($tables_enabled)
<div class="col-sm-6">
	<div class="form-group">
		<div class="input-group">
			<span class="input-group-addon">
				<i class="fa fa-puzzle-piece"></i>
			</span>
			<select id="res_table_section" class="form-control hidden	" name="res_table_section">
				<option value="">{{__( 'restaurant.select_section' )}}</option>
				<option value="1">Section 1</option>
				<option value="2">Section 2</option>
				<option value="3">Section 3</option>
				<option value="4">Section 4</option>
				<option value="5">Section 5</option>
			</select>
			{!! Form::select('res_table_id', $tables, $view_data['res_table_id'], ['id' => 'res_table_id', 'class' => 'form-control', 'placeholder' => __('restaurant.select_table')]); !!}
		</div>
	</div>
</div>
@endif
@if($waiters_enabled)
<div class="col-sm-6">
	<div class="form-group">
		<div class="input-group">
			<span class="input-group-addon">
				<i class="fa fa-user-secret"></i>
			</span>
			{!! Form::select('res_waiter_id', $waiters, $view_data['res_waiter_id'], ['class' => 'form-control select2', 'placeholder' => __('restaurant.select_service_staff')]); !!}
		</div>
	</div>
</div>
@endif

@if($tables_enabled)
<div class="col-sm-12 table-grid">
@foreach($tables_select as $table)
	<p class="table-st section-no-{{ $table->description }} @if( in_array($table->id, $tables_booked) ) table-booked @endif " val="{{ $table->id }}">{{ $table->name }}</p>
			
			<span class="table-st section-no-{{ $table->description }}">
				@php
				$datetime1 = new DateTime(date("Y-m-d H:i:s"));
				$datetime2 = new DateTime($table->booking_start);
				$interval = $datetime1->diff($datetime2);


				//echo $interval->format('%h س %i د <i class="fa fa-clock-o"></i>');
				@endphp
	</span>
@endforeach
	@foreach($tables_booking as $table)
		
		@if( $table->description=="1" )
			<p class="table-st hidden section-no-{{ $table->description }} @if( in_array($table->id, $tables_booked) ) table-booked @endif " val="{{ $table->id }}">{{ $table->name }}</p>
			
			<span class="table-st hidden section-no-{{ $table->description }}">
				@php
				$datetime1 = new DateTime(date("Y-m-d H:i:s"));
				$datetime2 = new DateTime($table->booking_start);
				$interval = $datetime1->diff($datetime2);


				echo $interval->format('%h س %i د <i class="fa fa-clock-o"></i>');
				@endphp
			</span>
		@endif
		@if( $table->description=="2" )
			<p class="table-st hidden section-no-{{ $table->description }} @if( in_array($table->id, $tables_booked) ) table-booked @endif " val="{{ $table->id }}">{{ $table->name }}</p>
			
			<span class="table-st hidden section-no-{{ $table->description }}">
				@php
				$datetime1 = new DateTime(date("Y-m-d H:i:s"));
				$datetime2 = new DateTime($table->booking_start);
				$interval = $datetime1->diff($datetime2);


				echo $interval->format('%h س %i د <i class="fa fa-clock-o"></i>');
				@endphp
			</span>
		@endif
		@if( $table->description=="3" )
			<p class="table-st hidden section-no-{{ $table->description }} @if( in_array($table->id, $tables_booked) ) table-booked @endif " val="{{ $table->id }}">{{ $table->name }}</p>
			
			<span class="table-st hidden section-no-{{ $table->description }}">
				@php
				$datetime1 = new DateTime(date("Y-m-d H:i:s"));
				$datetime2 = new DateTime($table->booking_start);
				$interval = $datetime1->diff($datetime2);


				echo $interval->format('%h س %i د <i class="fa fa-clock-o"></i>');
				@endphp
			</span>
		@endif
		@if( $table->description=="4" )
			<p class="table-st hidden section-no-{{ $table->description }} @if( in_array($table->id, $tables_booked) ) table-booked @endif " val="{{ $table->id }}">{{ $table->name }}</p>
			
			<span class="table-st hidden section-no-{{ $table->description }}">
				@php
				$datetime1 = new DateTime(date("Y-m-d H:i:s"));
				$datetime2 = new DateTime($table->booking_start);
				$interval = $datetime1->diff($datetime2);


				echo $interval->format('%h س %i د <i class="fa fa-clock-o"></i>');
				@endphp
			</span>
		@endif
		@if( $table->description=="5" )
			<p class="table-st hidden section-no-{{ $table->description }} @if( in_array($table->id, $tables_booked) ) table-booked @endif " val="{{ $table->id }}">{{ $table->name }}</p>
			
			<span class="table-st hidden section-no-{{ $table->description }}">
				@php
				$datetime1 = new DateTime(date("Y-m-d H:i:s"));
				$datetime2 = new DateTime($table->booking_start);
				$interval = $datetime1->diff($datetime2);


				echo $interval->format('%h س %i د <i class="fa fa-clock-o"></i>');
				@endphp
			</span>
		@endif
	@endforeach

	<a id="hide-tables" class="btn pull-right">تأكيد</a>

</div>
<script>
$('select#res_table_section').on('change', function() {
	$( ".table-st" ).addClass("hidden");
	$( ".table-grid" ).removeClass("hidden");
	$( ".section-no-" + this.value ).removeClass("hidden");
});

$("p.table-st").click(function(){
	$( "p.table-st" ).removeClass("active");
	$( this ).addClass("active");
	$('#res_table_id').val($( this ).attr("val"));
});

$("#hide-tables").click(function(){
	$( ".table-grid" ).addClass("hidden");
});

var elem = document.documentElement;

if (elem.requestFullscreen) {
    elem.requestFullscreen();
} else if (elem.mozRequestFullScreen) { /* Firefox */
    elem.mozRequestFullscreen();
} else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
    elem.webkitRequestFullscreen();
} else if (elem.msRequestFullscreen) { /* IE/Edge */
    elem.msRequestFullscreen();
}

</script>
@endif

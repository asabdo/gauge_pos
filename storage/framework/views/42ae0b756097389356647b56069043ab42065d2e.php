<?php if($tables_enabled): ?>
<div class="col-sm-6">
	<div class="form-group">
		<div class="input-group">
			<span class="input-group-addon">
				<i class="fa fa-puzzle-piece"></i>
			</span>
			<select id="res_table_section" class="form-control hidden	" name="res_table_section">
				<option value=""><?php echo e(__( 'restaurant.select_section' )); ?></option>
				<option value="1">Section 1</option>
				<option value="2">Section 2</option>
				<option value="3">Section 3</option>
				<option value="4">Section 4</option>
				<option value="5">Section 5</option>
			</select>
			<?php echo Form::select('res_table_id', $tables, $view_data['res_table_id'], ['id' => 'res_table_id', 'class' => 'form-control', 'placeholder' => __('restaurant.select_table')]);; ?>

		</div>
	</div>
</div>
<?php endif; ?>
<?php if($waiters_enabled): ?>
<div class="col-sm-6">
	<div class="form-group">
		<div class="input-group">
			<span class="input-group-addon">
				<i class="fa fa-user-secret"></i>
			</span>
			<?php echo Form::select('res_waiter_id', $waiters, $view_data['res_waiter_id'], ['class' => 'form-control select2', 'placeholder' => __('restaurant.select_service_staff')]);; ?>

		</div>
	</div>
</div>
<?php endif; ?>

<?php if($tables_enabled): ?>
<div class="col-sm-12 table-grid">
<?php $__currentLoopData = $tables_select; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $table): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<p class="table-st section-no-<?php echo e($table->description); ?> <?php if( in_array($table->id, $tables_booked) ): ?> table-booked <?php endif; ?> " val="<?php echo e($table->id); ?>"><?php echo e($table->name); ?></p>
			
			<span class="table-st section-no-<?php echo e($table->description); ?>">
				<?php
				$datetime1 = new DateTime(date("Y-m-d H:i:s"));
				$datetime2 = new DateTime($table->booking_start);
				$interval = $datetime1->diff($datetime2);


				//echo $interval->format('%h س %i د <i class="fa fa-clock-o"></i>');
				?>
	</span>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	<?php $__currentLoopData = $tables_booking; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $table): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		
		<?php if( $table->description=="1" ): ?>
			<p class="table-st hidden section-no-<?php echo e($table->description); ?> <?php if( in_array($table->id, $tables_booked) ): ?> table-booked <?php endif; ?> " val="<?php echo e($table->id); ?>"><?php echo e($table->name); ?></p>
			
			<span class="table-st hidden section-no-<?php echo e($table->description); ?>">
				<?php
				$datetime1 = new DateTime(date("Y-m-d H:i:s"));
				$datetime2 = new DateTime($table->booking_start);
				$interval = $datetime1->diff($datetime2);


				echo $interval->format('%h س %i د <i class="fa fa-clock-o"></i>');
				?>
			</span>
		<?php endif; ?>
		<?php if( $table->description=="2" ): ?>
			<p class="table-st hidden section-no-<?php echo e($table->description); ?> <?php if( in_array($table->id, $tables_booked) ): ?> table-booked <?php endif; ?> " val="<?php echo e($table->id); ?>"><?php echo e($table->name); ?></p>
			
			<span class="table-st hidden section-no-<?php echo e($table->description); ?>">
				<?php
				$datetime1 = new DateTime(date("Y-m-d H:i:s"));
				$datetime2 = new DateTime($table->booking_start);
				$interval = $datetime1->diff($datetime2);


				echo $interval->format('%h س %i د <i class="fa fa-clock-o"></i>');
				?>
			</span>
		<?php endif; ?>
		<?php if( $table->description=="3" ): ?>
			<p class="table-st hidden section-no-<?php echo e($table->description); ?> <?php if( in_array($table->id, $tables_booked) ): ?> table-booked <?php endif; ?> " val="<?php echo e($table->id); ?>"><?php echo e($table->name); ?></p>
			
			<span class="table-st hidden section-no-<?php echo e($table->description); ?>">
				<?php
				$datetime1 = new DateTime(date("Y-m-d H:i:s"));
				$datetime2 = new DateTime($table->booking_start);
				$interval = $datetime1->diff($datetime2);


				echo $interval->format('%h س %i د <i class="fa fa-clock-o"></i>');
				?>
			</span>
		<?php endif; ?>
		<?php if( $table->description=="4" ): ?>
			<p class="table-st hidden section-no-<?php echo e($table->description); ?> <?php if( in_array($table->id, $tables_booked) ): ?> table-booked <?php endif; ?> " val="<?php echo e($table->id); ?>"><?php echo e($table->name); ?></p>
			
			<span class="table-st hidden section-no-<?php echo e($table->description); ?>">
				<?php
				$datetime1 = new DateTime(date("Y-m-d H:i:s"));
				$datetime2 = new DateTime($table->booking_start);
				$interval = $datetime1->diff($datetime2);


				echo $interval->format('%h س %i د <i class="fa fa-clock-o"></i>');
				?>
			</span>
		<?php endif; ?>
		<?php if( $table->description=="5" ): ?>
			<p class="table-st hidden section-no-<?php echo e($table->description); ?> <?php if( in_array($table->id, $tables_booked) ): ?> table-booked <?php endif; ?> " val="<?php echo e($table->id); ?>"><?php echo e($table->name); ?></p>
			
			<span class="table-st hidden section-no-<?php echo e($table->description); ?>">
				<?php
				$datetime1 = new DateTime(date("Y-m-d H:i:s"));
				$datetime2 = new DateTime($table->booking_start);
				$interval = $datetime1->diff($datetime2);


				echo $interval->format('%h س %i د <i class="fa fa-clock-o"></i>');
				?>
			</span>
		<?php endif; ?>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

	<a id="hide-tables" class="btn pull-right">تأكيد</a>

</div>
<script>
$('select#res_table_section').on('change', function() {
	$( ".table-st" ).addClass("hidden");
	$( ".table-grid" ).removeClass("hidden");
	$( ".section-no-" + this.value ).removeClass("hidden");
});

$("p.table-st").click(function(){
	$( "p.table-st" ).removeClass("active");
	$( this ).addClass("active");
	$('#res_table_id').val($( this ).attr("val"));
});

$("#hide-tables").click(function(){
	$( ".table-grid" ).addClass("hidden");
});

var elem = document.documentElement;

if (elem.requestFullscreen) {
    elem.requestFullscreen();
} else if (elem.mozRequestFullScreen) { /* Firefox */
    elem.mozRequestFullscreen();
} else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
    elem.webkitRequestFullscreen();
} else if (elem.msRequestFullscreen) { /* IE/Edge */
    elem.msRequestFullscreen();
}

</script>
<?php endif; ?>

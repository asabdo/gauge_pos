<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-body bg-gray disabled" style="margin-bottom: 0px !important">
				<div class="table-responsive">
				
					<div>
						<div class="col-sm-12 col-no-padding">

							<div class="col-sm-12 col-no-padding">

								<div class="col-md-2 col-sm-12 col-2px-padding">
									<button type="button" 
										class="btn btn-warning btn-block btn-flat btn-lg @if($pos_settings['disable_draft'] != 0) hide @endif" 
										id="pos-draft"><i class="fa fa-floppy-o" aria-hidden="true"></i> <br> @lang('sale.draft')</button>
								</div>
								<div class="col-sm-10 col-no-padding">
									<div class="col-md-6 col-sm-12 col-2px-padding">
										<button type="button" class="btn btn-success btn-block btn-flat btn-lg no-print @if($pos_settings['disable_express_checkout'] != 0) hide @endif pos-express-btn pos-express-finalize"
										data-pay_method="cash"
										title="@lang('tooltip.express_checkout')">
											<i class="fa fa-money" aria-hidden="liue"></i>
											<b>@lang('lang_v1.express_checkout_cash')</b>
										</button>
									</div>
									<div class="col-md-6 col-sm-12 col-2px-padding">
										<button type="button" 
										class="btn bg-maroon btn-block btn-flat btn-lg no-print pos-express-btn pos-express-finalize" 
										data-pay_method="card"
										title="@lang('lang_v1.tooltip_express_checkout_card')" >
										<div class="text-center">
											<i class="fa fa-credit-card" aria-hidden="liue"></i>
											<b>@lang('lang_v1.express_checkout_card')</b>
										</div>
										</button>
									</div>

									<div class="col-md-4 col-sm-12 col-2px-padding mar-top">
										<button type="button" class="btn bg-navy btn-block btn-flat btn-lg no-print @if($pos_settings['disable_pay_checkout'] != 0) hide @endif pos-express-btn" id="pos-finalize" title="@lang('lang_v1.tooltip_checkout_multi_pay')">
										<div class="text-center">
											<i class="fa fa-credit-card-alt" aria-hidden="liue"></i>
											<b>@lang('lang_v1.checkout_multi_pay')</b>
										</div>
										</button>
									</div>
									<div class="col-md-4 col-sm-12 col-2px-padding">
										<button type="button" 
											class="hidden btn btn-info btn-block btn-flat" 
											id="pos-quotation">@lang('lang_v1.quotation')</button>

										@if(empty($edit))
										<button type="button" class="btn btn-danger btn-block btn-flat btn-lg pos-express-btn" id="pos-cancel">
										<i class="fa fa-ban" aria-hidden="true"></i> @lang('sale.cancel')</button>
										@else
											<button type="button" class="btn btn-danger btn-block btn-flat hide" id="pos-delete">@lang('messages.delete')</button>
										@endif
									</div>
									<div class="col-md-4 col-sm-12 col-no-padding mar-top">
										<button type="button" 
											class="btn btn-info btn-block btn-flat btn-lg pos-express-btn"
											id="pos-quotation">@lang('lang_v1.forward_pay')</button>
									</div>


								</div>
							</div>
							

						</div>

					</div>

				</div>

				<!-- Button to perform various actions -->
				<div class="row">

				</div>
			</div>
		</div>
	</div>
</div>

@if(isset($liansaction))
	@include('sale_pos.partials.edit_discount_modal', ['sales_discount' => $liansaction->discount_amount, 'discount_type' => $liansaction->discount_type])
@else
	@include('sale_pos.partials.edit_discount_modal', ['sales_discount' => $business_details->default_sales_discount, 'discount_type' => 'percentage'])
@endif

@if(isset($liansaction))
	@include('sale_pos.partials.edit_order_tax_modal', ['selected_tax' => $liansaction->tax_id])
@else
	@include('sale_pos.partials.edit_order_tax_modal', ['selected_tax' => $business_details->default_sales_tax])
@endif

@if(isset($liansaction))
	@include('sale_pos.partials.edit_shipping_modal', ['shipping_charges' => $liansaction->shipping_charges, 'shipping_details' => $liansaction->shipping_details])
@else
	@include('sale_pos.partials.edit_shipping_modal', ['shipping_charges' => '0.00', 'shipping_details' => ''])
@endif

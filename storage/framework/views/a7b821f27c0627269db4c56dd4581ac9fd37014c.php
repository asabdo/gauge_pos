<hr>
<div id="products-box" class="form-group">
	<?php echo Form::label('product_id', __('product.product_name') . ':*'); ?>

	<div class="input-group">
		<span class="input-group-addon">
			<i class="fa fa-product-hunt"></i>
		</span>
		
		<?php echo Form::select('product_id', $products, null, ['placeholder' => __('messages.please_select'), 'class' => 'form-control select2', 'style' => 'width:100%', 'id' => 'product_id']);; ?>

		<!-- <span class="input-group-btn">
			<button type="button" class="btn btn-default bg-white btn-flat add_new_customer" data-name=""><i class="fa fa-plus-circle text-primary fa-lg"></i></button>
		</span> -->
	</div>
</div>

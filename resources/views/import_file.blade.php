@extends('layouts.app')
@section('title', __('Import'))

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>@lang('Import')</h1>
</section>

<!-- Main content -->
<section class="content">
	{!! Form::open(['url' => action('HomeController@importFilePost'), 'method' => 'post', 'id' => 'add_import', 'files' => true ]) !!}
	<div class="box box-solid">
		<div class="box-body">
			<div class="row">

				@if(count($business_locations) == 1)
					@php
						$default_location = current(array_keys($business_locations->toArray()))
					@endphp
				@else
					@php $default_location = null; @endphp
				@endif

				<div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('document', __('purchase.attach_document') . ':') !!}
                        {!! Form::file('document', ['id' => 'upload_document']); !!}
                        <p class="help-block">@lang('purchase.max_file_size', ['size' => (config('constants.document_size_limit') / 1000000)])</p>
                    </div>
                </div>
				<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('additional_notes', __('note') . ':') !!}
						{!! Form::textarea('additional_notes', null, ['class' => 'form-control', 'rows' => 3]); !!}
					</div>
				</div>
				<div class="col-sm-12">
					<button type="submit" class="btn btn-primary pull-right">@lang('messages.save')</button>
				</div>
			</div>
		</div>
	</div> <!--box end-->

{!! Form::close() !!}
</section>
@endsection
<div class="row">
	<div class="col-md-12 eq-height-row">
		
		<?php $__empty_1 = true; $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
			<div class="col-md-3 col-xs-4 product_list eq-height-col no-print">
				<div class="product_box bg-gray" data-toggle="tooltip" data-placement="bottom" data-variation_id="<?php echo e($product->variation_id); ?>" title="<?php echo e($product->name); ?> <?php if($product->type == 'variable'): ?>- <?php echo e($product->variation); ?> <?php endif; ?> <?php echo e('(' . $product->sub_sku . ')'); ?>">
				<div class="image-container">
					<?php if(!empty($product->image)): ?>
						 <img src="<?php echo e($image_url . $product->image); ?>" alt="Product Image">
					<?php else: ?>
						 <img src="/img/default.png" alt="Product Image">
					<?php endif; ?>
				</div>
					<div class="text">
					<small><?php echo e($product->name); ?> 
					<?php if($product->type == 'variable'): ?>
						- <?php echo e($product->variation); ?>

					<?php endif; ?>
					</small>
					</div>
					<small>(<?php echo e($product->sub_sku); ?>)</small>

				</div>
			</div>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
			<h4 class="text-center">
				<?php echo app('translator')->getFromJson('lang_v1.no_products_to_display'); ?>
			</h4>
		<?php endif; ?>
	</div>
	<div class="col-md-12">
		<?php echo e($products->links('sale_pos.partials.product_list_paginator')); ?>

	</div>
</div>
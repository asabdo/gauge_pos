<!-- business information here -->

<div class="row">

	<!-- Logo -->
	@if(!empty($receipt_details->logo))
		<img src="{{$receipt_details->logo}}" class="img img-responsive center-block">
	@endif
	

	<!-- Header text -->
	@if(!empty($receipt_details->header_text))
		<div class="col-xs-12">
			{!! $receipt_details->header_text !!}
		</div>
	@endif

	<!-- business information here -->
	<div class="col-xs-12 text-center">
		<h2 class="text-center">
			<!-- Shop & Location Name  
			@if(!empty($receipt_details->display_name))
				{{$receipt_details->display_name}}
			@endif-->
			<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAwgAAAEmCAYAAADGPLd4AABJCElEQVR42u2dB7dUxba2v39yTSQFJUoGyRkkSc5JAZGcJCMKApsoQRGULKBkRcmSOSBBchSJEgUE4zn19Vv3bC8S9u7eXat7rdXPM8YcnkPa3bVqVc23aob/Z4z5D4ZhGIZhGIZhmOz//fd/AAAAAAAAGAQCAAAAAAAgEAAAAAAAAIEAAAAAAAAIBAAAAAAAQCAAAAAAAAACAQAAAAAAEAgAAAAAAIBAAAAAAAAABAIAAAAAACAQAAAAAAAAgQAAAAAAAAgEAAAAAABAIAAAAAAAAAIBAAAAAAAQCAAAAAAAgEAAAAAAAAAEAgAAAAAAIBAAAAAAAACBAAAAAAAACAQAAAAAAEAgAAAAAAAAAgEAAAAAABAIAAAAAACAQAAAAAAAAAQCAAAAAAAgEAAAAAAAAIEAAAAAAAAIBAAAAAAAQCAAAAAAAAAgEAAAAAAAAIEAAAAAAAAIBAAAAAAAQCAAAAAAAAACAQAAAAAAEAgAAAAAAIBAAAAAAAAABAIAAAAAACAQAAAAAAAAgQAAAAAAAAgEAAAAAABAIAAAAAAAAAIBAAAAAAAQCAAAAAAAgEAAAAAAAAAEAgAAAAAAIBAAAAAAAACBAAAAAAAACIQn8e9//9v8/vvv5tdffzX379839+7d+9v0//Xrv/32m/njjz+s/fXXX+Y//0HHAAAAAAAEXiDIsZcgkMO/f/9+M2bMGFO3bl1TvFgxk/uFF8wzTz9tnvqf//nbsj33nHnh+edN/nz5TLGiRU35cuVMzRo1TJPGjU3Pnj1N2tix5ovPPze7d+82J0+eNNevX/9bROjn6OelG0AY0Fz+888/zZHDh83s2bPNnIgdPHjQimzmOQAAAARKIMh5Wb9+valcqZJ1+h8UAi7s6aeeMs89+6zJlTOnFRQlihc3FStUMPUiAuTt/v3NiuXLzfHjx+3NBEAQkTAYP368KZA/v53vD879fHnzmsmTJllhDAAAAOB7gfDTTz+ZFi1aOBcFWRUSRQoXNg0bNjQd33jDjBgxwnz15Zfm9OnTNqRJtw8AfkNzs327dpnO7z69eyMSAAAAwL8CQbcGCvupUKGCL8RBZvbSiy+atm3amA8mT7ZhS7ptIGwD/MDgQYOiFsALFixgwAAAAMCfAkEJxhXKlw+EOHicKWxj5syZ5tq1awgFSBrnz583OXPk+MfcfPaZZ0zeyPzMkT37I/P2ldKlma8AAADgT4Gw6LPPAisOHjTlTCh/AiAZDBww4JE5uW3bNpuTcOniRStkH07uvxj5dQAAAABfCQSduithOAwCQZY9WzaSQCEp1H711X/MxWpVq1pxIHRTsH379kcqgC1fvpyBAwAAAH8JhGXLloVGHDwoErZv28ZMgYRSqWLFf8zDpk2a/EOo3rp165FQo6lTpzJwAAAA4C+B0K1r19AJBNnzuXKZ2z//zGyBhFG3Tp1HQogOHTr0d56BFQgP5SgsmD+fgQMAAAD/CASdbpYuVSqUAkG2YcMGZgskjPfeffeROSiR0LdPH7Nq1SrbMPDhvghHjhxh4AAAAMA/AkGlTb1ohuYX69KlC7MFEsbdu3dNnty5YyrXq47iAAAAAL4RCGfPnrVdjcMqEAq//LL57bffmDGQMBYvXvyPW4KMrF69egwYAAAA+EsgnDp1ytZpD6tAULLylcuXmTGQMJRvoHAidQHPrFHaCioYAQAAgN8EgmqzP9zYKWz2ww8/MGMg4fxy966ZNXOmad+unSlYoMAj87JWzZrmr7/+YqAAAADAXwLh999/N4UKFQqtOFDNeXW3BXCJbgkUunbnzp1MnfwTJ06Y3C+88Mi83LJlCwMJAAAA/hMIcnRe79AhtALhxTx5zO3bt5kx4JQ7kTlVonhxW0q3ebNm5v79+3+XMk1HDdKWLFnySN8D2fjx4x/58wAAAAC+EAhi6dKloRUI6myLIwaumTZt2j/mWb68eU3jRo3MiHfeMRMnTjTdu3UzFcqXf+ycbNa0qfnll18YRAAAAPCvQLhx44YttxhGgTBlyhRmCzhnzpw5WZqPyjv4meZ9AAAA4HeBINasWRN1acYg2fHjx5kt4Bx16FYJ3VjmYtMmTcy9e/cYPAAAAAiGQFBH5Z49eoRKJFSvVo3wIvAEzasrV66YYkWLRlVqNy0tzb5jEH70nFX8QblPly9fNkePHDH79+8333//vTn/44/2Bkm/rz/H+gQAAL4WCOkb29ixY22FlaCLg7x585KcDJ6jG4Hly5aZsmXKWCGQPv/UfLBQwYJmzOjR5urVqziCKSIaj0TEQNu2be2zz5Uz52PXpmzPPWcK5M9vGjVsaPbt24dwBAAAfwuE9E1u186dpm6dOoEVB0WKFDEH9u/HKYOEISfv5s2btjP56dOn7e0CPQ5S59lv3rzZ1KpVK+amkzqMqV69uv37CAUAAPCtQEjnjz/+MBs3bjQDBgwwVatUseUcgyAO2rZpYx00AACv0S3l2/37O1m7hgweTHUrAADwt0B4EJ3Ea+NSwu/06dNN165dTZMmTaxwKFqkiG0CpfCKZIUmSbzUr1fPbNu2jVM4AEjImqgbo3qRdcflWqYSuApb4/YTAAB8LxBi4fr162b9+vW2TrxqwauJVJ3atU2lSpVsYynF5qqkau7cuc0Lzz9vnfucOXJYU0MpmcSG4nT/tsj/168rpld/Pu9LL5mKFSqYrm+9ZQ4cOMBsgEA5ljIJWTVS001duikkCccwGM9Qz65CZA3y4sBD4Z2aDwAAAKERCNEg50gb4K+//mo70N69e9de1au6x61bt+zJ3MOm39Mthv4OtwQQ1Hl/8eJFM3bMGNtETbdvBQsUsKaSqaVKlrSi+uDBg8xxHyNx0Lp1a89uRFVJTnMEsQgAACklEABSid9++80sWbLEvFa/vr0RiyZxVQ3Vtm7dipPoM/Q81q9bl5DQyUOHDjHgAACAQAAIE7odW7hwoS2BmhUHUUJh8qRJVEPyEbrt1M1PIgSCwjO5SQIAAAQCQAhQEyyVCi5SuLATR3HihAncJPiEpUuXJrQAg/okAAAAIBAAAoqceOXQ1K1b16mTqNr6chQRCcl/vq1btUqoQBg1ahQDDwAACASAIKI8g9GjR9vqXF44isWKFrUJ+pA8VCQh0f1gVNEIAAAAgQAQIBQjvm7dOlOtalXPncUF8+cz4Elk7549Ce/xkj9fPtsXAQAyRpURL1++bIsITJw40QwdOtT06tXLdHnzTdO5UyfTuXNn0717dzNo4ECTlpZmwwXPnTtnD3cAEAgAEDfpfQy+++4708bDcpcPm/qDqBwwJIclixcnXCDomcuJAYBHD2d++ukns+izz8zAAQNsuWiVCI71HVOjV/VUWrhggbl69SqFAQCBAACxo1OqY8eOmfbt28fl+KnZnxoIvvH666Ze3brWEYzm7+m2ApLDh9OnJ6VT/JEjRxh8gP+KAoX67dq1y7Rs0cKT961Tx462xLCKTQAgEAAgQ3RroAZmVatUseVHs7r5KIZdV9vafNKTjrXpHT16NKoeCU2bNCFZOUlMnTo1KQLhMP0QAKww+OCDD0zOHDk8f+d0E1G9WjUbssR6CwgEAHiEa9eu2eRj9TJQNaGsbji6xp4zZ44NEXrShjN82LBM/53nnn3WnD1zhgeTBOZGnl+ixYHm3KmTJxl8SFl++OEH07FjR/PSiy8m/P2TGBk0aBAFIgCBAOFBJ9QrVqww744YYSZNnGgdXcgcNSW7dOmS+WDyZHtanyPO0yqdRA0ZMsRcOH8+05995coV82KePJn+m5988gkPKgls3rw54Q6KhCXvLqQaulVVmM+bnTtHHX7ppb1Wv7758ccfeTCAQIDgotPpCxcumJo1ajxy8qz4da5LHx0v5RUcPnzYzPjoI1OhfHknG4rCkBq89po5depUTJ+nf79+mf7bLVq04DkmASVERhMG5tJKlihB0iSklDBQzk2PHj2SLgoetjKvvGJOnDjBQwrY/v7nn3/aSnA6gFMo77ebN5uVK1bY5PYFCxaY+fPmmfnz55tFixaZ5cuXmy1btpjjx4/bgxndHOnQMGj7LQIBHsvPP/9s8uXN+9gFTr9+8+ZNBum/SBjs37/fVKpY0Wnsqpy6r778MkufadOmTZn+DF21h/k5ajH264Jco3r1hDolr7/+Oi8qpIQwkEM24O23fScMHrQihQvbgwIOaPw7j1S2VmJgx44dZuKECaZihQpxHfQ1b9bMlhiXaLhz5479GX5//ggEeKzDqyTajCb8hvXrGSjzvyFYLVu2dLp5yHGXgx9P9Qt1YlZYSWY/a+XKlaF7JjrpWbVqlenerZvp07u3rWeu0xs/oVumRDokc+fO5WWFUCOHbvz48SZH9uy+FgcPigQlTIO/DpXOnj1rBabCdL246dXhX66cOW31QYkP+VsIhDi4e/euOXL4sJkyZYrpFtn05ZDVr1fPVK9e3VSuVMkqO1mVypVtSIx+T2pNZcaUGPT555+bcz/8YO7fu4dij4LFUdRplwMLxkxzWJFG81bXlvfv3497nurvqyZ3Zj9T71OY2LNnj+0W/fD3LFG8uF1D/EIiw4y0GZF/AGF26rRnlS5VKhDC4EFr3KgRics+QOvj2xFRULZsWRtGnWih2KtnT1vpCoGQCbp2UXiLTjbVqbBatWrOHpj+HYmIt/v3N7NmzbLXR0GMC/MSjUmB/PkzHMc8uXPbRjBB2Tz0jCUydVrj8iRZc7VqHJ2Pde0oZ1bdOL///nvnMeIbN27M9DNIWIdl/p85c8bkz2Du5sqVK+ZcDi/n5exPP03IBqSDFYCwofVS4RpNGjcOnDB40D768EPyg5Kw/mrM9+3bZ956662Ei4InNbPs3bu33aP8Mh98IRD0sFS2UaEACguIpwxkrFYt4uDNmzvXdhn1WxhCMp7D+++/n+mY9Y8IrKB8nx3bt5saDyRat2je3C4KLpxiCY4n5WlkZko8PnDggA2H8cpBl9jLrNfCC88/H4quyhrDoUOHZjrub3Xp4pvFVzdF1eIQmNGYBKgOXADCtE/pgGrs2LGBFgYPHhSpNw4kBvl5EpbR3LAnywYPGmTLkCf78C6pAkFfXkmSisdNdFWPx1n7du3M+fPnU1Yo6Flk5vAqfi4oHVkVbvI4sakY1e+++y7uf18VDTK7bYmmooXiw1X6Lj20yNWioNjGokWKZJ6HsGJF4G8R9PmjSfzVKY3qofsF5ZmUKlnSk/VMMbRBuekDiOYd1968bevWLB/M+NXq1qljD4vA2/mjw7CR770XiDkh32XsmDE2+iFZh1pJEwiKu+vbp489wfTTQ9FVk2LBVcM+1RiXlpbp+Ci3IyjXoeXLlXvi9yhdurSTOH+JSlfzLn++fLbazOnTp52IVH0+Je1l9rNr164d+CtufVc902jGeubMmb763BLmCn10uY4p1ErziPBJCAu6CZMjHU8Hej/bjBkzeMgeovy+ggUKBG5eKKR76dKlSdmjEy4QdKo5YcIE358AqALMsGHDfJ1h7tpRicZJ2bplSyC+j+pMZ7SR6CbERT+H/fv2eRISp1NlhcPoKjSez3j9+vVMP9/zuXLZU4qgz99ow3U6derkS+fnneHDncwdFXFQyCRAGNDJ+ofTpyelA3IiTbe93CJ443P26N7dF1Eq8VitmjVtrmIoBYI28K1bt3oec+vaypYpY2PFw34Sp1AHVTvJLLxI1VeCwNQpUzJ9tirl6kKVr1u7Nu5OyZnlyUydOtVWOcjK580szEjPVY1fgo4EVbTj6cf3WZ/pX//6l2kVcfBj7fqqZ6g+HKrmQsIjhAHN4y3ffmtqv/pqqIXBg6a8Cm793K2nCietV7duaOZHzoifMX7cOCeVDn0hEPQllMypSi2JTD52bXPmzLF1lsPKt5GFOJrJGU9t/kTSu1evqMJ6XCXoKta7UaNGnp9SqEOyBKtORaJdIJo1bZrpv6sTlqBvTAsXLoxqDHXN/LvP32WFOL4/apR5rX59W6I1vSa35qxMoZmFX37Z5l306dPH5tvgWEBYhIFq0feKrOESvqkiDtLz43TrC/H7nUr81hoZxnnyaq1a9h3xes33VCDow+vEuVixYqF4KHLOguIgx8qQIUOiSqQKCj179Ijq1FVx2i45dvSojen3Wigov+LYsWNR5SqoG3M0CVFB35iuXL4c1SGEbsqCchP2MBKG3BBAWJ269MNEv+UlynHXAVm2bNk8Fy06jETsx8eB/fvt8wqzmNT3O3TokKdzxVOBsGXLltDFDer6P4xVjqRIoym9FRTefffdqJ6n+mG4RvNDJUZXr15tinsojpVjoT4GRzOpKqWbLyVAZ/bvqftw0E8e27VtG9XYuRaGABDfmqnwmrwvveSLfV63jNobFHqp2+E7d+7YPC0VFNDaoc8aaxhgtKacMP08yBqqsvi8z4rfeDlPL168GCyBIEWzYMEC31UocmVqzBKmmwQ9r2iu4tIii2JQGDduXFTPskL58p6eyGrjU+OTJYsX29h3Lypw6LZC8ffaVB53mqBf69ixY1SdnIMufpcvWxbVmO3ds4edFCDJ6MZg8qRJUZVjTkQ/ggFvv2327t2babKw1lSVRNeBoRef5cvVq5kcWfBjjh87Fvqbg8d14/bqFsG5QJCDobruXqlrv5i67ylRJCynN9GU/xrxzjuB+U4jR46MOrkzUXXx9RKr34E2xFq1ajnv3qg49a+//vqxgmfFihVRjYUKCQQZNZeJZqyUcwMAyXHkbP+jGTNM0aJFk76Xa90cNHCgvfWNFd0qvPbaa84/U6OGDQkljJFzkX28YMGCKSUO0vNWVMDE9wJBE3rJkiUpk1g0ccKEUIQbRSsQWrduHZjvFEuXxM8++yw5C9q5c6Zf376mZIkSThP4J0+e/Eh5XiVjR/OM20SecZA3JjWvi+Z7btq4kR0VIMHCQLk/s2fP9kV0gXKRtE/Em4+ktTXehpmPs0SXtAwyuomSqEo1cZBu06ZN8+QWwZlA0IdbFuX1fphMCaBBTyiSQ1goCuWtuL4gOI96HpUrVQrUaY1uMVS/3tWtgmrqPzwvo6mzL3G/J+DhN02bNMn0e27hBgEgIWtxevLx0KFDfbNvq8S1qwp2+n5fffWV84PRAQMGMIGiHP9hPppbyTD19vG1QNi0aZOnteD9akr+DHr1F02sjLoOPxijeePGDd9/H3XpjuVER99L9ef98BzUMEsnRw0bNIgrX0FCQ4L9wUVDsbXRbp5BFr1Do6jItW/fPnZWAI8PnhSnX6d2bZt464f9unSpUnZ91a25yzVO37VBZM12+VlV4IIwo8zZuXNn4JugxWvdu3Xzp0BIj6sOa73ZaEx15oN+i/Bm585Rfdddu3b5/rvoyjjWBcOPoTWKiV20aJFtFJSV06liRYv+I9lO1YzKlCkT1S3CZwsXBnZOjx0zJlNBmKi8E4BUO81VXL5KdXoRm59Vy5M7tw1t8jJvcNu2bU5DRbUOK0IBMhZmJUuWTGlxIJv58ceejG/cAkGxztWqVUvph6MXefny5YEWCXMii2dUZV5btfJ1O3g9A1UMysqJu66J/dphV+X2hgweHFO1j8fdjKxetSoqsaG/q9O/IKJxyui75X7hBZskCQBu1ifd2urw6J133vFVafNcuXKZt/v3t6IlEbRo3txtsnKjRkywDMSBKiv6wf9Lt2RV3zpx4oT/BIKu6UaMGJG0zG3FzevKsEKFCqZSxYq2ZKUSPvPlzZvwrs0qrRXkUCNlwUc7wXfs2OHb76EQnSKFC2fpGWre+P0ZqrzuxzNmRH1j17lz53+IHm2UOk2L5u/q5iJRG6vLTUPlWrm6B/BWFOhw8MyZM3Y9KuKDMqUPO20KbfLKcXoS69etc/o91BcirM1Z40WRK4lyyvVz9CwUit3gtdfM8GHDbHET9fr67rvvzMEDB8y+yH+3Rv6/bt8HDhhg96GyZcrYKllefs6XCxXy7NA2LoGgVtaJfOl18jdmzBhz6dIl+9Jok09Pgko3/ZqEi2rCq1FVIsteqYZyUG8Rok1UTu/iq3AVPzJ92rS4hZ7fnWLNMS0I0SRmqaLPg5W29HenTZ0a9Xh069YtUM70rVu37OFBRt+pYcOG7K4AWURrv0qZ+7XPkU5UlW+VjHUr2oqAsZhKV8Oje6BK0yZiPr355pvW59Sem+5nRvsZNQf19xQuLFGRy4NcnC6Rz+cVWRYIOqktloAaxhIFCyOKTDHDWXnh9cKePXvWDIs8HK/Vpm4tglqaTJO5QQwxo619GGq0cuVKJ8lKFStWzFJN7ESf3insKLPOo5qTDz8nvUexdHhWd2I5BX4Xv9FWUhs+fDg7LEAMe6jyuhS6p5t6vyaEqvfSh9On21LHyVyrPv/8c6ffS5ERD5etTnVOnjjhvI/Qwwdr6pWjw0KXc0nvkm7d1Agvlj04o6gHL32VLAkEDdiE8eM9rzagWHJXJ9U2jvvIEdOyRQtPP7fUXFDDF+bNmxfTd23erNkTu/cm2jGUiJSYdPUcSxQvbr744ou/Tw2SsSlrU1BSncK/VHVH74M26fbt2kWdiyAR/7heHR9//HFM46Hk5u3btvm674cEk6qKZdokbfNmdliADNZT5RRs2LDB9O/Xz1Y183MOoG4M+kU+5+nTp30xfhcvXnT+HS9cuMDEfGB+vj9qlGeHvPq3XZXAzex7fH/woG2cKn83K4JYeZNekiWBoBP5PHnyePKAVA7t/ffftwuUF8h5nz59um2S4tUEU0xaEJGzH2s1KoUlffPNN0mJk9QLJvUsp9mrnJNaNWvaWMNtW7eaf+3ebfbv329viQ4dOmSORASnnFK1dz9+/Lg5Fvmv/v/hw4ftnzlw4ID5bu9eszvy93Zs325PJDZGNl1dGStZeOnSpfbfVoL4lClTbPfnPn36WAGg+H+JlHhP61R7/HECxxYXqFo15n9PIUcHImPgp9sjfT/1bohmkdWY6oQRAP6JDuO0Vr337rvmpUxuJv1gWhtVfU5rrp/QIcqrtWo5/a5rPHYEg+anuA7jSm+ctzFJDTS1n65du9a0bdMmqpLA2uuU/+A1MQsEbcbdunb15IXXqajERyJQ7VyvRIJXTSsSwZjRo7P0nWvXrm2dYgkFL797elyfhMGMGTNicqCluP0aN+tVIn9G14/r16/Pcq+FihUq2MVU76sWbAkObYyyh3ODvJoHmmu6ru3du3fU32PcuHHssAAPvEd6f3VoUdRnicYZVZxTAvKpU6d8O667Iv6Fy5BmVUcKeil1V8yfP9/5nJJfoJt6P6CwptERP6xcuXI2hFh+i/yc9CTpjz76KGEHdDELBDXKyulBQzTFv3vtXD5y+hyZEJklNGZ1AVOORhDR81VcWzwqXJWttOmkO4ouHUKJEGXtZyX+UHWqp8WZxBwka9euXYbhbvq94VF0V86suoPeIZ16PGhacNNNt0zlypa1m3qLFi1sGJ4S+nWVq+ehBX/VqlW2AoRuXSQ6Ll+6ZOOer169akWO/nvt2jVbuUJVU0qXLm3nWqybMP0PAP7v1FIFC7zYA72yVyLvvdYDP4c62rH944+owh1jsRsBb8jqiiaNGzsPUdu/b5+vBFj6Qahu9XTjLdP/dulTORcI+mAd2rd3/tL36tkzKSEL+j6K7fZC8PTo3j2wL+A6R6XaihYtal599VXTunVrM2vWLLN71y576iPHTypZYWRy+mWKtZeo0O8pllQhI4sXLbIdAuvWqWMTteIJt9Eps43rj/ysxo0ahV4c1Ktb1/zyyy9RnVaUi6KLdhhMNbMBUh2tg2pkltVy0Ik2hY926dLFhm4GJb9PvkWzZs2cjoNCeVMd+QquowDGpaWxKLgQCHImXD8cWyHFo3yDaJk1c6bzCkeKkQvqlaA2EMWhJrJmtZf/vk6tH6wCcfLkSVMgf/7QOsLqiq2boGhR74egOAtZNTVvUglUgFRG66DWhyC8s/I1VBryxPHjgRzrNWvWON3bBg8alPLzd9nSpc5zDL3srp1SAmFmjJVPMjM5aaovm2x0KuHFoqmkk6CiG50OHToE3jHUbcHjTtKVcOzXcn3xNExRNaesnLKp8kaJEiVCm4uh500ML6QyWtN79Ojh63dVDrVui5ULd/fOnUCPt0JCtCa7GptSJUv6PrTK61uZpk2aOJ1vKhoCDgSCVFZ2h7GKWgg2+6jcoByk5x03sVBFIL/1Coj1hVSTkGS1EHchDjIaf3XZ9CK8LBmn40OHDIl781Ccf8mQiQTNXSV8AaQ6qq7n1/dU+USqqqaKRGES8m+88YbTtUxV9FJZ4LpMpNeBWJD9M18JBCUPulwQVG7Qby3Elzq+vpKdO3cu0BNETqfKwgbNKZw4YUKmL782It1gBTXcSCdKKqPqsmynbltcn9Ik09QgMah9SQBcHva8/vrrvlunq1SubPMA/dBPxwtcN00b+d57KTuHtc+5rDy5evVqFgZXAmH8uHFOs8ZVBsyPE9B15YEZH30Uis1FjqgqSPjdISxSpIitVhTLZqPE6J49e/q+GVDJiCDQTYH6KahxjlcbqoT7ihUrAl8SVr0l6EAKYMzNmzedNpLM6hqmTsxKClVfGFUlC3vYn6oZurylrlOnTsrOYTW6zWpZ7sclvvulrGngBYI22fLlyzub5Apj8Cvqj+BqEqaXZfs1yUnYrtCJ/OxPP/WlUFDZ02FDh9qE26ygU2Y1HlHjHS9buEfzPVQWVOFRffv0MR9Mnmz7FeimI5En4dq4FXY3YMCAQJVBTA/tW7d2LTkHAP/l0PffJ/w9VP32unXr2lLKX375ZYY9WcJMp44dnY1pnty5rdhLRVz2P9Ce5rcIlsAKBDUjcuk0v9Wli28HRE5Y2TJlnC6UQe2snJFgXLRokT3NcDkvsmI6nWnZsqXTUC5tZKNGjTJNmza1jUl0ki7HPd10+pCRPfhn9XdV0UqiuHLlyqZ+vXo2hEefWR2T+/bta2bPnm3niESAbrH85tjqtkLN/1588UXf52IMjAiasAhyAFds2rgx4eFDXbt2tZV81LtG72SqCvaVK1c6Hdu5c+em5Fi6TLBv1LAhi4IrgaBwA5cTfOvWrb4dEL14S5Yscfp9x48fH9oXWt9r0KBB1glOVFUgiRJ1FVSzLUgs6k9RuVIl64xLACU7gV1zQY39qGUN8GTUKTnZAr5s2bJm+/bt9gBGRU8S3fQpWegm1uU4vlqrVsrlVen7Vq1SxdkYjqFwhTuBoI6nLmsb+/1qRyfkLqu5yKEKe7a8kpkVU6pGaO8MH25D0lwKBl1X14osjEu/+MJciiy4XA8mF43/1Z9+spvfkSNHzPJly8zQoUNtueA+ffrYxkbqcdK8WTPTsEEDe3OijU2VShSHrM7KesdUlULiUoJDMdK6+n34pkYiRKb/rTmlv6cSvBs3brTOBpUoADJG4ZN+uu2TqC9dqpT5cPp0u4Zo/wirWJBzq8aVrsbuxTx5nBamCAIqGVusaFFnY6hDYHAkENo77J5co3r1QKhfJYO6vG49fOhQyk0uvdQ//vijLc2mDeqrL7808+bNM1M++MC899579uahT+/epmePHrabdr++fW1jnIkTJ9p4Q3WO3LVrlw1xI2wktU6LJNJ1yqiqSkoiV9yt/ncq1wEHyCo//PBD0sNBM9of5fw1adzYdjtXx2SJ/jAJBpdhRhqvHTt2pNT81dqv/AtXYyhfBBwIBL2kKkXm6sF07NgxEANz8OBBe2LprDzZyJHMNgAASDgS3K+++mqgmj726N7dfPrpp+b06dOBPxhQHpdLf6J58+YpFWZ0+/Ztp+O3bNkyFgUXAkFXWaqq4urB6GQ+COjlUxtul3GDAAAu0QGO1irdtqjxn6qw6XTx0KFDtsRimEM3XI+jTq0Vvrh3714bK6//nj9/3jrXYRhD3cgGtWSxcs4+/vhjO791mhy0/AWFZLrsqixTSG+q8POtW07HTr2dwIFA0CR02Zxi8qRJgRkc9TBwWb5SITcAAK6cWpX1fSODBljK+VKIH3kaTx5DhS8qtymzfUuOaZCFgoSO4v7D0ACxXr16NvdJzy4oYkGFFFyOwTYfF3txjUJMXY6dwprBgUBQDLnLBzNr1qzADI5iIZ02TZsxgxkHAE4c2zFjxkTdhKlZ06Ypl9gYDSdPnrQNCKMZQ3Vc3717d6BFgkI1gto5/kk3C6pus2rVKt8fwKmggsueMjoYSBV0G+py3hQvVoybVRcCQVfVrmv4BgUtOOrM6+q7V69WLeXKkwFEi94NnXSHLUHRNQobev/992NOOlXtb24S/g8l7qqCVixj+HyuXPbgKMjC8uyZM857/filD0rrVq3MrJkzfXvb43LcFZOv3jmpwJ07d2wlQ1djp2p4miMQp0BQHKbLl3jOnDmBGqD33n3X6QJGNR6AfzosuqUcP26cKVG8uHXAFBajwggfz5hhNwYvNvr02P2gxTLrs367eXOW1yB1taUS1P9WRSmUxZhwlZnMasd2v6DT7BbNm4dOJDzYKbdb165m+fLl9qDPL+/422+/7fR7fvrJJynxvirHKn++fE7752jfgTgFghojOU0OmTYtUAO0b98+p5MyyKdPrp2zq1ev2hsqxUjrNM9PCzkkZh6szSRxUqJaJ56uf66ST1U2TzZn9mzrMAVh7umqPZ564Do5c9l1PKjzbvLkyfFVpXvvvVDcBivs1WX5SD+a9t20tDRfdHRWuXOX1Xjq1qmTEoJfN58Vypd3Oi+UdwRxCgSV+3T5UIYNHRqoAZIT6/KF1glCKjvB+u5yxoo+IXTr22+/JQwrRdi1c2dUYTJqquQqfl7zT8mNKhrwsBBRIpzf383Zn34a9xqkpnWpvAapMZeLNf3kiROhWZN79+qV9K7oibpZmDtnjk3YTtbtoRpFuhQ/Bw4cSIn3tnOnTk7nghrY4mvEKRDUpMrlQ2nTpk3glKtCHlx9f12TpXKyoJLJFEaSUROYYcOGcZOQAkIxlhOhgQMHOvmZapCjjs2P+xkNGjTw9bxz2ZNGp6mpyiezZjkZQ4Vrhel9vHz5spk6Zcoj4jmMpjVAlZAUIZFoJ3HUqFFOv8tbXbqkxHs7f9485w36jhDREZ9AUBdTXUu7zB4P2pWYuvu6nJhnHIdMBAU5JU9yzh5+cYNU7Qpi5/jx4zGd4haLs+qE/u5HH32UYdUf9XvRyaJfUVdxV2vQO++8k5LzTvXoq1Wt6mQMleAcxkTHO7dvm8WLFtmk9rALBVmVKlXMBx98kLDiCLrBdPn5wzoPH+bUqVNOozlk7733HptxPAJBSYK64nf5UIIWh6/4eJfXr6tXr07Jyda2TZuox6hkiRJc/4WYKZENOZZ3RhUsLpw/n6WfpdwWOQCZvcNVI46jnw8vunfr5vQm8+7du6l3SOE4ZHbJkiWhHav0W4XBgwebGtWrx1w1K4i5ClqXlLzq5d6jca1du7bTzz516tTQ37orf8R1vowKDlxOkUpQnggEbZhlXnnF6UPp1atXoAZJi4XL2tH6/qkWQqOTu1iqEOik4FAKh0GEnf79+8dc0k8nb7GicL4BAwZE5TD7uTOp1ovy5co5XYfXrFmTcvNODc9cjqH6S6QKctB0m67QwIzCRMNgQyKi6OKFC54JhWVLlzp3dMMeuixf1IvyvHrWhDRnUSCIpk2aOH0gChcIGq6upWVK0FXZrlTi9OnTMYeqzQ1YSVyIngkTJsRcfed8jDcICheKZu3S7YTfE/102u+yDrise/fuKTfvWrZo4bxRV6o5F3KadeCj6nN6v8J6s6Dv1aF9e09u2tS/wPXn3bFjR6jnor6bnHnX45YrZ04bSg9ZFAiDBg1ynhyiZNUg4ToPIdXKncYacx60rtsQG0ePHo1pPhQpXDim0zyFFUUj6vUZtm7d6vvx+uLzz51vjBrTVCqYIKdW39n1OIY5zCgaFIasctWqyV+wYMHQCQWFtfTr189pp2atZeqE7PJzqq+H37tJx8uFCxc8qbZVp3ZtbhGyKhDU/dj1AwlaYs13jhvGpdrp+I0bN2KuBrVhwwbe0BCfBsVS7i/asET9uzqdi7ZXwKZNm3y/MciZaOH45DsV86EkSr0Yw4oVKtB87oG5qhj+Tz/91LzxxhuhEgoqYvDB5Mn2WbtYM1z3RJB98803oZ9jVatUcf5sJTrmzZuHSMiKQNBVohcv3JLFiwPzQBQS5PL0SaXJUm0yVq9WLaYydDqZgvCye/dukydPnsxD8iLOvmrXRyMODkTWqmjioxWus2H9+kCMk2K/C3l0MqsQkVRZh953XF4y3VQZSwcg8ChypmfPnm1e79DBaR5fMk05GGqgGq8o1Htd+OWX3VZkqlzZ/rthZty4cZ49W1VaRCTEKBAUg1egQAHnDyNbZJM+H6B21+3atnX23bUw+LmkohcsWLAg6uvBNq1b83amADt37szQcShRvLitIhaNOFAZ0GhquCufYX6ATotUScZlqemHY+hTZR3y4uQx3RbMn49jEQVyrCVKVRkx6I3Z1M8g3iiIWHOxop2LYeb69esZlquO1y/zc7EKXwoELXxdu3b15IG0jTjdQSlnqbb0Lr/73r17U2qy6TmruVA0SeypWIIxVdGJlxLsWrdqZRPGdAOg+Nz9EWciWudV4iDaJF7VeA+SM7cwIqy9dHRWrlgReudW3+/FKG6r4im8Efb4b5f7gN75s2fPmj69ewdaJOgAI56bbo2Duri7/EyvlC4datGvd3lgFJXpsmoNGzSwPTEgSoEg1q9f79kDGZeWFojBUgyry4oN/fv1S7kJpxdPDsmTYi9V/vLKlSu8mRD1ZqFTyWga8KU7w0Hqr6GNPpZcjazGVod9Q1Q+k9fOohJ1Ifb3V6fwEvgTJ0707GTYS9OBhoquZFVku4xMSDeF04UZdUD28pmqZxN9mGIQCHLavLoSlNOtKh1+fyDarJWQ5jJeMFU3Bdm2rVtt0tekyMbwySef2FsDrukhlnkk0R6NUyFBum7dusDNL5UH9trB0boeTRhXUFGseOXIWuv1OI4ePZr1y8E7vW3bNuvgvlyoUKCEgpqVZSUvYXdEHLn2rRQ6+POtW6Gdj/IVvb59UmEIVXlL5Xc6aoEgvKqkIcuRPbvZuHGjb6tBaJLcvHnTtGrZ0ul3vhV5iQEg9vfxh7Nno+ryrs1yz549gVvo9XnnzJ6dEOdGiX9h3QiVw5GIeHc5tBRWcIsOAPr27WsP5lxX/PHCxo4dm6X3qE6dOs4/S5MmTUJ9M6i+ONHknMVj5cqWNWfOnElZkRCTQFAlEa+boixbtsx3IkFq9bvvvvOkksjkyZM5dQKIkV/v37c3cNF0SD558mQgv6M2d5c3lhmZShCHNe9n0aJFCXMQV61cycvpETrNTUtLs9UEXTcNdGkKlYoVVXT04mYw7HmOEmReP0/NNR1ep2LIUUwCQY570Sjri8czqXv17Okbp1kZ8+XKlfNMGJUuVSrlqhkBxIv6F2T2bpUsUSLQ+SxbtmxJqGMTxhrqSgLVPEjUGLZv146X02O0X6rSzNq1a20/Jb8JBJ1qq+hCLCgPQw3ZvGjypsiHsCKfNBHvt26vlCuCQMgAOe3aRBLxkikTXyUQkyEU9DPPnTtnWrZo4fkVltQpbb4BYkPvTEalP0eMGGF+/vnnQN/OJbrRlDbAsN1m6uQvkWOosFGKLCRun9aprkLIvv76a9O8WTNfJS7/FMM80Hf58MMPPfksgwYNCnWUwiEPGs49yRTSOvvTT1PmNiEmgZCu2KpVrZqQh6FT+969e5tjx44lZILru6l5U48ePTwPpXrQPpw+nTAjgBidA5X/fPg9VROjFcuXB34BVwlIOZuJdm6DGo71ODQH1E8l0c7h4IhDBslZE5QDMm3aNPvck10RqUf37jGFS6sDtSen388+aws0hPm5j/KoCeKTrF7dumbd2rWhFwoxCwTx5erVCXWgZW927mzzAFzXmtbk0onP6sh3SpTweVxyW7wNVwBS0SE4ceKE6dOnj+nVq5fdBMOyYLdv3z4pa1GYnFvdQCdjDIsUKeLbYhuptDZoT/3oww9NrZo1Ey620+3AgQMxCdouXbp48jnKliljbt++HdrnLb+wQYMGCX++NapXtyV6fwtp9+osCQS9fKoTm4wXTiE5w4YNs9d3SlqK1SFIb9Kil0W1i3Xi6IcryR3bt7OqA4ANn0rW6WeBAgVCcVihPapRw4ZJW89VtplbYf/MBQk2NTpVU7JEHm7GGt5z48YNz959hWCF+cT70qVLUffD8aIj/cYNG6xvmfICQag+t9fx+ZmFH6nyhhJUqlapYpo2bWorAqmh28GIaj98+LBVdp8vWWK79+pKSFVP9OfVUdNvJdM6derEhgKQ4igBs2qSbjLTTWUlg74WffXVVwkpbfokk6NyJ8QntkEVCjppVtnKAR524n04STiWqAc58M2bN/fMZ5o/b16on+/JEycyzE1LhFBQKfzff/89tQWCHsbSL74IdJt0P5kEy5EjR1jFAVLYgdEBRzIdW1munDltgndQkUPmRUnqWE1x0Rz6+Pdd0xxfsGCBKVqkiLfVwb7+OqbPpZNwJTl79Xl2xlhhKWjPVT1vvBy/qPMU6tUzY8aMCXQVqSwLBKE63cmKlQ2jde/enZUbIEVR4x/dbvphLapYsaIN4Qwa2pN69ujhizFMhTr0YUCnvRIKXnXbfrt//5g/U/du3TzteRJLbkQQUbVNhZP5ZR1o2KCBTZ5XxaUglbWPSyCkL8iVK1XCwXd0i3D8+HFWbIAUdFL8dtiisotBilnW6aHKXfppDBXWeuvWLSZ4ANBcVwhO4cKFnc4BJc/GyuVLl+xNnlfzsvDLL4eqYtnj2LZ1qy1L6jc/T707Rrzzjvlu715z9epVX98yxi0Q0kuLFXH8UqWqKSuea2mA1EHv+7vvvuvL9Uh9BIKCDlf8llsm60x+WfDEert2Tns6ZWVNGD9+vOfx8gp/CfPcVOlYL4WWq/mhnFk/Vj6LWyCkT+ZTp0550gkw1UzXUXPnzGFDAUgRcTBn9uykFnzILNn26NGjvh9HHVJ5HUsej/XPQpgJJA/lsfTq2dPJs8+fL1+WPsP1a9c8r8qjWH2Vig7z+qq1oW6dOr73/YoVLWpz0Pzk+zkRCOkciaggv8R9BdlUyjXs138AiIP/mNWrViW9oVM0fVrOnD7t23FUyWq/h7nqZiMtLY3+CAFCfZdcxfxndX1QFcZEHEpOnTrVhouHFYWPzZkzxxfJy5lZ27ZtfZPY7FQgCCXalS5VCkc/TitXtqythw4A4dywFi5cGJj1SCeZSrj1282mQghKly4dmHEcM3q082af4BY5yhs2bLBx+q7enayiniQlihdPyNxU92lFgoT5QObYsWOmaZMmvl8natWqZfMTQicQ9BDULKJ+vXo4+nFa61atAllJBACezP37983o998PZPijqoP4IXFZn2H37t2+v315nPXu3dvcvXuXF8FnouCHH34w69auNeXLlXOeEBwPyq1RVEEi5qbeJ5WvD7OI1S2e1rHixYr5ep1o5oPGds4FwoMP4Z3hw1PCkfcyrKpZ06as3gAh2pz80r09q/Z+RNwkm0kTJwZ6DEuWLBnqrrZB4cKFC6Zx48aeOuBqfBjvoWuTyGdM5PyUUNBpe1jRuydR+Nlnn/l6nVixfHk4BUL6Q1A3Y8XghVEY1KxRw6p7JfF5+XPq1K7NtTRAgFHt6wkTJoRmLawWcXp+unIloWOYXgyjYoUKoRhDhZ4oLpq8BG/njG7sJAQWLVpkWrVqZWrVrGkTQtVZOFFVrOJFTd0KFCiQ8BvDGhEfJ8w9EzQ/lMOk8sh+LIlatkyZpK4PngqE9Aeg69SRI0eaHNmzh2JhV0nXxYsX/+20S4mWLVvW05+p6zDV9aW6EUBw0BqxYP58U6lixdAdkKjyUr++fc2F8+c938QPHjxoOr7xRtK7THvScbVuXbNu3TrW9iwg5+lKRKjKif3qyy9tM6qBAwaYNzt3ts2pVNnq6QQJgSfZF1984eS7zps7N2mhhQ1ee82etisnIgyiQIJr7549Zvbs2baRXauWLX1Zql9J1cnsau+5QHjwoejKqk2bNoFd5IsWLWrGpaXZTf/hxXznzp0J+V4DIovf6RAnEgGEYQNSFbJZs2bZNSMVqq5JKOgAw2UlFB0sKVa4RfPmoRQGj+uBs3zZMuvwpqJYkLN/O+IMqdCJOs7u2LHDbNq0yaxZs8bGxc/46CMzdOhQ06F9e5vEKedfItXPc6NA/vzOnGq9Wx06dEjq91HvhGHDhplvv/3W5pr6XQQcOXLEzqHly5ebsWPHmpYtWtjbo6CsJ7rlSmaZ6YQJhAcf3OnTp20ikF9rfz+uzN+kSZMyfCEkGlxVPch00jzzjOnUsaMVCkFq2w0QRhRKqW65Z86cMQsXLEh6Pf4X8+RJ2nV5tueeM23btLElIlVhSE2not0XtIbq76hakhL0ktn0TA6ErveT2TxJJ8+aU5pbfhMMmvN6XnJ+9fmu/vSTuXjxok301WfWHq9wMJnEshx+OfrTp0+3J7aqJKNcHN2Mq0+AmlnppD9sQlDNzlw+u0uRMfa6N0Iszqtuv77dvNk+c5Xm9DKvJn2NkON/+fJlc/bs2b/n2f79++2BjG4CVPUpTGHtOyNCOWUEQvqD1kTSg5aj69cHo0m2bds2u8ll9pLr96dMmZKUz6gEpn379tmFWmOKYZh70/t148YNc+7cObNy5UrTvVs3eygghzxRVUaiMX22+fPn+8KBkOMnB7Bnjx52jZITIbt27ZrZtXOnGTtmjA2/yhsRNDkjf/aZJIeDpJtOGi9duuSbGxoVwlDOm0SDyh+mr/X6r7X/jqvmp+z69evmcuTzy2HXCaQEm/aytd98YxMfNT8++vBDe6o6dMgQ0y0ylxWfX/vVV22Zcs3rQgUL2hPwfJHnp5NjfQY1Q9Weo9AHJbIqbFifT4d9z4TQwY830VfPwbXv9P333/vmPXn4cEBzQ++7DknUjfrDyBxTdIVuxW5G5mX6fE2fqyrlrvm5dcsWG8I0ISKoenTvbhub6d8oWKCAPezQGqs5p/VE882PHdO9MlVrSymB8PCE10u0du1aG+eW7ImvBa5b165mz549MScG64YhUTWLMQzDHjZ1f1UogrqH0o8m62EUOp3U3hSkXhWYv0w+jRc3Pzpc/eSTTxBjKWASQqeT2KQy6QLhYbGgq6OlS5faa8jKlSsnLFO8f79+Nv5TpzLxvNRBL7+HYVgwTSd3D55Y6to9lU7aXNmyyD6QvgcoLr5KgvYhLDw2ePBgTzsTa37qMICxDrfp5uReEhPDfSUQHn4B9IJpw9PVqK5BdQXasGFDWzEo1jg8XfeVKlnSvFa/vmnZsqXp26eP2bB+vb3i0s9xpfR1cqefw+TGMCxRpqt3lVx+cB3TSWO/fv0YnxhM+8KDZQU1nici4+qXuG/M//b6668nJGdEfouKvjDm4TX1wUpmvxTfCoSMhEN6DoNyA9RpOD0WMz2uTf9Nj9HU7+vP6c/LEvHiLlm8mMmNYVjCYv0V6/u4tU2/VrVKFcYpClN46JPCSqdOnUpIB5apvdWlS0ITytXjQbkpjH34TDkXyiFKJoETCEFAyp5NGcOwRJ16Z3TKpFtSL7u9h6W3jcqqPsm505qupHTGCnuSqUKTl2FFT0IHoUQthM/mzpmT9OplCASPUM8HJjmGYV6a6qLrFDGzW9fNmzcHpqx0MnI3VBkmM3QrrV4FjBn2cFL79u3bk9rxViKBQ8nwJCarb0MyQ4sQCAmoE62EZa6lMQzzwho3amSd1mhDM9VsinF79BpfddyjPalTiVEq1WHpjtzbb78dd2ETl+FG6gPAswmuVata1Rw+fNg3fU8QCB6iE4VaNWsy8TEMc2o1atQwt2/fjjl/a8GCBdwk/Nc0Dtu3bYtpM9afVbffUpSQTXlHTvX9/XDK+yDKtyQULnimHjCDBg1yWjAHgRAAlDStZjO8BBiGubD69etneRPR31v02WcpX/5UNwfq/BtPSEfJEiWYjykYTrR50ybfCYOH33GS6oNjWkfUlNGPIBASwJZvv/Vl50MMwwKWkNy3b8wNHB/nQHzz9dcpe5NQvFgxc/78+bidPJXgJu47NUydpdWFWmE8fjrhzSh64cCBA7b8Mc/Pn6YeB1u2bLG3Pn4FgZAgRa824rwUGIZl1dQ99Y8//nC2Ju3atSvlbjdbtmjh9LROJbW7du3K/Ayh6QT+9Q4dzMaNG2259CAIg4ffcZXJVC19nqd/rEnjxuabiNj89ddffT+nEAgJTFqeOXMm134YhsVkhQoVMjt27HC+mejf+/HcOVO3bt2UGMcRI0ZkWvEpq3Hf49LSTPZs2ZivIckvGDN6tDkXeTeSWZnI5fycMWMGtwlJtJcja/iggQPNkcOHk1IKF4EQEJEw46OP2EgwDIvqBLNF8+bmN49PmrQu9ezZM7TrkjbnvXv3ejqG+rd10qxQFOZusCzbc8+Z8uXLm1atWtnbJT/nF8QzP9UPpW6dOoQ7J2rdiawFjRo2NLt37bJCM2g3UAiEJPH1mjWIBAzDnmhaH5YtW5bQdem7iBOtJMwwCax2bds6C8uKBoUNSNQxh/0vCkqUKGHmzJ5tw4dSCYnlggUKMA8cm4RX7hdeMO3atTOXL18OhdBEICRJzZ84cYIqGBiGPWI9unc3V65cSfiJk36eqvOkpaUFPyyrYEGbY5GM6/w/I4LkX//6l23Axnz2j1WsUMFMnTLF1plX7khQT3VdvOfqGr582TKTM0cO5kacpZJbt2pl1qxZY0PSgpBXgEAICGpyNPK997jywzDMNGrUyDqWyY571smXOsG/2blz4MZQTvm8uXPNL7/8ktSN2vZLuH3b5p2pWgnzO7GmE/LevXqZWZHxV78CNbjzW415PwgFzdH58+aZYkWLMm+inFdDhgyxRWcOHTpkhVYYQ9IQCD7KS1CMGg3VMCw1Td1PN23c6LuESK1NOoVv07q178dQ3Y3VuV4btt+csJs3bpjhw4bZ8APmu/tQodqvvmpv3d4ZPtxs2rTJlp+VGAiz4+ZFaJyaKDZ47TXm1QOHDZ06drTdshUWrnmlNTqV5hUCwUeb8edLlphXSpfm5cSwkFu+vHnt1bRiVf1+qqnPp8+p2Ho/5SjkyJ7dVKlc2eZqBKEyiHIhxo0bZ0qXKpXyjepiycXRDYzCcVVdqGmTJmbokCG2qpfC4cJQZchv77pCZZo3a2Yd5LBXXVSIkOaX/K46tWubHj162BsnHTRw24RA8KVQOHjwoG3mwwaBYeGycmXL2lPOIJW6exB97hUrVpgihQsnbQwVN/3+qFHmzp07gXXCVJ9egouy1/+bTC7TnFKNeJWj3bhhg7l27VpgGpOFEYkvJTTXC0kZZM0xdVBXWed3333X+llhDxFCIIRUxWsjPnPmjJk7Z44pW6YMzhWGBdAk9EePHm1Dda7+9FMoEiP1+XUarm7EX375pWnbtq2neVQKI+ncubNZuXKlOX36dGgSAeWY3Lp50+zZs8eMigieMCY1K2a7fr16pk/v3mbChAlm4cKFNpxu37595uTJk7asqG4C1B08PSwIQeC/eapcBSV3L1ywwDYb9Ot8U5lh3cyqj8XixYvtTdPRI0fMpYsX7YGC1i3mGAIhVGJBk1oLqTbISZMmmW7dutkqHThgGJZ806mUnCDFmU+ePNnG8soBUsyq3t2wb0YSPdp85UDMnTvXjB8/3nTp0sUULVIkpnHUNb/WNjUd+/jjj82GDRusCJEgCPspn+aIGlqliy5V2xk4cKCpXKlSwuezGmrJ0apUsaJp2KCBef31103//v3NqJEjTdrYsWbixIlm6tSp9hnNmzfPrF692mzbts0mbf744482IVgOpZz+vyJOf3rcNk5ZeASDbnY0V9etW2fngd7ZXj172vkqQe+iZKj6l9SsUcO0b9fODBk82IwZM8bmGX344Yc2sfrrr7+2Nxyaczcia60+k+ZaqlanQiCAnfgyvaRagLUx37hxw5ZFVOwghmHe2cWLF831a9dsBTJdT8upS38n2ZQeXackkFRRSOOlkBHlMmgM5URq3dLpserQp2/qjOGjY6iTdY1R+hhq/NLno5yjCxcu2HFVIyz9vkziVOMr09+TaazluGvP0NzVc9G/KwGmveTB09UnGUBGc/VB30RzTPNOc1FzU4JCc1b/1XzVPE2fn5qXmo+ai0+ah4BAAAAAAAAABAIAAAAAACAQAAAAAAAAgQAAAAAAAAgEAAAAAABAIAAAAAAAAAIBAAAAAAAQCAAAAAAAgEAAAAAAAAAEAgAAAAAAIBAAAAAAAACBAAAA4Df++usvs2vnTjN+3DjTpUsX075dO9O6dWtr7dq2NR06dDBvRX69f79+Ji0tzcybO9dsWL/enDhxwty/f58BBABAIAAAQJC5d++emTZtmmnSuLF5MU8e88zTT5un/ud/smz6+9mzZTO5cuY0+fLmNeXLlTONGjY0b7zxhunVs6cZNWqUWfvNN+b69etWUPz73//mIQAAAgEAACDZyDGfM2eOeblQobgEQTyWI3t2+/ObNGliBg8ebD744AOzcsUKc/ToUXPz5k3zn/+wpQIAAgEAAMBzfvvtNxsi9PRTTyVNHGQqHnLkMK+ULm369+9vVq1aZUXDL7/8wsMDAAQCAACAS3RzMGb0aN8Kg4zshRdesMLm9u3bhCYBAAIBAADABVu2bPH1zUE0lu2550yzpk25UQAABAIAAEA8/Pnnn6ZWzZqBFgcPWulSpcy+fft4sACAQAAAAMgKuj0IizhIt6JFipgLFy7wcAEAgQAAABAr/fr2DZ1AkNWtU4ecBABAIAAAAMRKmMKLHs5J+PHHH3nAAIBAAAAAiIWCBQqEUiDIBg8aRN8EAEAgAAAARIt6H+ikPawCoUjhwvY7AgAgEAAAAKLg1q1b5tlnngmtQJDduHGDBw0ACAQAAIBo+PXXX81zzz4baoGQytWMdHty6tQp89VXX5lOHTuaKpUr21sVhZWp0lON6tVNv379zPbt2+04kdQNgEAAAIAUR/H5Yc5BkJ0/fz4ln+vnn39ucufOHVsPidKlzaVLl3gxABAIAACQytSvXz/UAuH69espJQx2795tHf2sdsbWjZJuG+7evcvLAYBAAACAVGTM6NGhFQcvFypkw6hSgb/++suMS0szzzz9tJOxK1+unDl96hRVoAAQCAAAkGqcPXvW5MiePZQCoWvXrinh4EocDB061Pn4vfTiizaHAQAQCAAAkELIgX53xIjQiQNVZzpw4EBKPL/x48Z5No55X3rJ3Lx5kxcFAIEAAACpxJ07d0ypUqVCJRCKFytm/vjjj9A/u2PHjpns2bJ5OpZvdenCSwKAQAAAgFTjypUrplLFiqEQB2r+tnXr1tCHF6mMaekECDvlNaxbt46XBACBAAAAqcYvv/xiRo4caXLlzBlYcaDqPVOnTk2J5/Xt5s3OkpIzs8aNG/OCACAQAAAgFdGp+5kzZ0z37t1NmVdeCZQ4KJA/v1m8eHFKNPz6888/TdUqVRI2tjlz5DDnzp3jBQFAIAAAQKqLhRs3bthOvC1btDAlSpSwjdVyv/CCee6557Jca9+1PZ8rl2nUsKG5fft2yjwbOeuJHmeVUaXsKQACAQAA4G+xoHKav//+uw1FknA4ffq02bNnj1m+bJkZNXKkad2qlalSubIpUby4LZGpSkJeOasvRERK40aNzNdff22FQao5rps3b064QKhfrx4CAQCBAAAAkDURoQZl9+7dMz///LO5evWqOX/+vPnh7Flz8uRJc/ToUfP999+bvXv32s6/27dvt0nFcno3b9pkNm7c+Lfp1/R7+nP6O2repX/v/v37KRFK9CRmzpyZcIFQskQJBAIAAgEAAAD8yPDhwxMuEAoVLJjSogwAgQAAAAC+pW+fPgkXCPnz5UMgACAQAAAAwI8MHDgwKVWiEAgACAQAAADwIZMnTUq4QChapAgCAQCBAAAAAH5EpWcTLRDKlytHkjIAAgEAAAD8yJEjRxIuEDp17IhAAEAgAAAAgB/57bffTN6XXkqoQFi5YgUDD4BAAAAAAD+ik/wFCxYktMSpek8AAAIBAAAAfIo6SL+YJ09CBMKgQYMYcAAEAgAAAPgZ3SIoWfm5Z5/1/Pbg0qVLDDgAAgEAAACCIBIGDBjgqUBYs2YNAw2AQAAAAICg8Ndff5nu3bo5FwbPPvOM+WTWLAYYAIEAAAAAQUM3CRMnTnQmDrJny2bWrV1LWVMABAIAAAAElT///NMcOHDAlC1bNi5x0KJ5c3P27FnEAQACAQAAAIKOnHr1SPjmm2/Mm507m6efeirqG4O3+/c3u3buNH/88QcDCYBAAAAAgDCKhTt37pgtW7aYsWPGmF69epnOnTqZjm+8Yf/bp08fG5a0e9cuc+/ePW4MABAIAAAAAACAQAAAAAAAAAQCAAAAAAAgEAAAAAAAAIEAAAAAAAAIBAAAAAAAQCAAAAAAAAACAQAAAAAAEAgAAAAAAIBAAAAAAAAABAIAAAAAACAQAAAAAAAAgQAAAAAAAAgEAAAAAABAIAAAAAAAAAIBAAAAAAAQCAAAAAAAAAgEAAAAAABAIAAAAAAAAAIBAAAAAAAQCAAAAAAAgEAAAAAAAAAEAgAAAAAAIBAAAAAAAACBAAAAAAAACAQAAAAAAEAgAAAAAAAAAgEAAAAAABAIAAAAAACAQAAAAAAAAAQCAAAAAAAgEAAAAAAAAIEAAAAAAAAIBAAAAAAAQCAAAAAAAAACAQAAAAAAEAgAAAAAAIBAAAAAAAAABAIAAAAAACAQAAAAAAAAgQAAAAAAAAgEAAAAAABAIAAAAAAAAAIBAAAAAAAQCAAAAAAAgEBAIAAAAAAAAAIBAAAAAAAQCAAAAAAAgEAAAAAAAAAEAgAAAAAAIBAAAAAAAACBAAAAAAAAcfD/AcB4GA6w7lNqAAAAAElFTkSuQmCC" width="100%" class="img img-responsive center-block">
		</h2>

		<!-- Address -->
		<p>
		@if(!empty($receipt_details->address))
				<small class="text-center">
				{!! $receipt_details->address !!}
				</small>
		@endif
		@if(!empty($receipt_details->contact))
			<br/>{{ $receipt_details->contact }}
		@endif	
		@if(!empty($receipt_details->contact) && !empty($receipt_details->website))
			, 
		@endif
		@if(!empty($receipt_details->website))
			{{ $receipt_details->website }}
		@endif
		@if(!empty($receipt_details->location_custom_fields))
			<br>{{ $receipt_details->location_custom_fields }}
		@endif
		</p>
		<p>
		@if(!empty($receipt_details->sub_heading_line1))
			{{ $receipt_details->sub_heading_line1 }}
		@endif
		@if(!empty($receipt_details->sub_heading_line2))
			<br>{{ $receipt_details->sub_heading_line2 }}
		@endif
		@if(!empty($receipt_details->sub_heading_line3))
			<br>{{ $receipt_details->sub_heading_line3 }}
		@endif
		@if(!empty($receipt_details->sub_heading_line4))
			<br>{{ $receipt_details->sub_heading_line4 }}
		@endif		
		@if(!empty($receipt_details->sub_heading_line5))
			<br>{{ $receipt_details->sub_heading_line5 }}
		@endif
		</p>
		<p>
		@if(!empty($receipt_details->tax_info1))
			<b>{{ $receipt_details->tax_label1 }}</b> {{ $receipt_details->tax_info1 }}
		@endif

		@if(!empty($receipt_details->tax_info2))
			<b>{{ $receipt_details->tax_label2 }}</b> {{ $receipt_details->tax_info2 }}
		@endif
		</p>

		<!-- Title of receipt -->
		@if(!empty($receipt_details->invoice_heading))
			<h5 class="text-center">
				{!! $receipt_details->invoice_heading !!}
			</h5>
		@endif

		<!-- Invoice  number, Date  -->
		<p style="width: 100% !important" class="word-wrap">
			<span class="pull-left text-left word-wrap">
				@if(!empty($receipt_details->invoice_no_prefix))
					<b>{!! $receipt_details->invoice_no_prefix !!}</b>
				@endif
				{{$receipt_details->invoice_no}}

				<!-- Table information-->
		        @if(!empty($receipt_details->table_label) || !empty($receipt_details->table))
		        	<br/>
					<span class="pull-left text-left">
						@if(!empty($receipt_details->table_label))
							<b>{!! $receipt_details->table_label !!}</b>
						@endif
						{{$receipt_details->table}}

						<!-- Waiter info -->
					</span>
		        @endif

				<!-- customer info -->
				@if(!empty($receipt_details->customer_info))
					<br/>
					<b>{{ $receipt_details->customer_label }}</b> {!! $receipt_details->customer_info !!}
				@endif
				@if(!empty($receipt_details->client_id_label))
					<br/>
					<b>{{ $receipt_details->client_id_label }}</b> {{ $receipt_details->client_id }}
				@endif
				@if(!empty($receipt_details->customer_tax_label))
					<br/>
					<b>{{ $receipt_details->customer_tax_label }}</b> {{ $receipt_details->customer_tax_number }}
				@endif
				@if(!empty($receipt_details->customer_custom_fields))
					<br/>{!! $receipt_details->customer_custom_fields !!}
				@endif
				@if(!empty($receipt_details->sales_person_label))
					<br/>
					<b>{{ $receipt_details->sales_person_label }}</b> {{ $receipt_details->sales_person }}
				@endif
			</span>

			<span class="pull-right">
				<b>{{$receipt_details->date_label}}</b> {{$receipt_details->invoice_date}}
				
				<!-- Waiter info -->
				@if(!empty($receipt_details->service_staff_label) || !empty($receipt_details->service_staff))
		        	<br/>
					@if(!empty($receipt_details->service_staff_label))
						<b>{!! $receipt_details->service_staff_label !!}</b>
					@endif
					{{$receipt_details->service_staff}}
		        @endif
			</span>
		</p>
	</div>
	<!-- /.col -->
</div>


<div class="row">
	<div class="col-xs-12">
		<br/><br/>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>{{$receipt_details->table_product_label}}</th>
					<th>{{$receipt_details->table_qty_label}}</th>
					<th>{{$receipt_details->table_subtotal_label}}</th>
				</tr>
			</thead>
			<tbody>
				@forelse($receipt_details->lines as $line)
					<tr>
						<td>
                            {{$line['name']}} {{--$line['variation']--}} 
                            @if(!empty($line['sub_sku'])), {{$line['sub_sku']}} @endif @if(!empty($line['brand'])), {{$line['brand']}} @endif @if(!empty($line['cat_code'])), {{$line['cat_code']}}@endif
                            @if(!empty($line['sell_line_note']))({{$line['sell_line_note']}}) @endif 
                            @if(!empty($line['lot_number']))<br> {{$line['lot_number_label']}}:  {{$line['lot_number']}} @endif 
                            @if(!empty($line['product_expiry'])), {{$line['product_expiry_label']}}:  {{$line['product_expiry']}} @endif 
                        </td>
						<td>{{$line['quantity']}} {{--$line['units']--}} </td>
						<td>{{$line['line_total']}}</td>
					</tr>
					@if(!empty($line['modifiers']))
						@foreach($line['modifiers'] as $modifier)
							<tr>
								<td>
		                            {{$modifier['name']}} {{$modifier['variation']}} 
		                            @if(!empty($modifier['sub_sku'])), {{$modifier['sub_sku']}} @endif @if(!empty($modifier['cat_code'])), {{$modifier['cat_code']}}@endif
		                            @if(!empty($modifier['sell_line_note']))({{$modifier['sell_line_note']}}) @endif 
		                        </td>
								<!--<td>{{$modifier['quantity']}} {{$modifier['units']}} </td>
								<td>{{$modifier['unit_price_inc_tax']}}</td>
								<td>{{$modifier['line_total']}}</td>-->
							</tr>
						@endforeach
					@endif
				@empty
					<tr>
						<td colspan="4">&nbsp;</td>
					</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>

<div class="row">

	<div class="col-xs-12">

		<table class="table table-condensed">

			@if(!empty($receipt_details->payments))
				@foreach($receipt_details->payments as $payment)
					<tr>
						<td>{{$payment['method']}}</td>
						<td>{{$payment['amount']}}</td>
						<td>{{$payment['date']}}</td>
					</tr>
				@endforeach
			@endif

			<!-- Total Paid
			@if(!empty($receipt_details->total_paid))
				<tr>
					<th>
					&nbsp;
					</th>
					<th>
						{!! $receipt_details->total_paid_label !!}
					</th>
					<td>
						{{$receipt_details->total_paid}}
					</td>
				</tr>
			@endif-->

			<!-- Total Due-->
			@if(!empty($receipt_details->total_due))
			<tr>
				<th>
					{!! $receipt_details->total_due_label !!}
				</th>
				<td>
					{{$receipt_details->total_due}}
				</td>
			</tr>
			@endif
		</table>

		{{$receipt_details->additional_notes}}
	</div>

	<div class="col-xs-12">
        <div class="table-responsive">
			<table class="table">
				<tbody>
					<tr>
						<th style="width:70%">
							{!! $receipt_details->subtotal_label !!}
						</th>
						<td>
							{{$receipt_details->subtotal}}
						</td>
					</tr>

					<!-- Shipping Charges -->
					@if(!empty($receipt_details->shipping_charges))
						<tr>
							<th style="width:70%">
								{!! $receipt_details->shipping_charges_label !!}
							</th>
							<td>
								{{$receipt_details->shipping_charges}}
							</td>
						</tr>
					@endif

					<!-- Discount -->
					@if( !empty($receipt_details->discount) )
						<tr>
							<th>
								{!! $receipt_details->discount_label !!}
							</th>

							<td>
								(-) {{$receipt_details->discount}}
							</td>
						</tr>
					@endif

					<!-- Tax -->
					@if( !empty($receipt_details->tax) )
						<tr>
							<th>
								{!! $receipt_details->tax_label !!}
							</th>
							<td>
								(+) {{$receipt_details->tax}}
							</td>
						</tr>
					@endif

					<!-- Total -->
					<tr>
						<!--<td>
							<h3>To Pay: </h3>
						</td>-->
						<td>
							<h4>المطلوب</h4>
						</td>
						<td>
							<h4>{{$receipt_details->total}}</h4>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<div style="text-align:center">
								يشمل ضريبة القيمة المضافة (5%)
								{{ preg_replace("/[^0-9,.]/", "", $receipt_details->total)/100*5 }} ريال
							</div>
						<td>
					</tr>
				</tbody>
			</table>
        </div>
    </div>
</div>

@if($receipt_details->show_barcode)
	<div class="row">
		<div class="col-xs-12">
			{{-- Barcode --}}
			<img class="center-block" src="data:image/png;base64,{{DNS1D::getBarcodePNG($receipt_details->invoice_no, 'C128', 2,30,array(39, 48, 54), true)}}">
		</div>
	</div>
@endif
<div style="text-align:center">
	
	@if(!empty($receipt_details->footer_text))
		<div class="row">
			<div class="col-xs-12">
				<br>
				{!! $receipt_details->footer_text !!}
			</div>
		</div>
	@endif
	
</div>
<?php

namespace App\Restaurant;

use Illuminate\Database\Eloquent\Model;

class Modifier extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'modifiers';
}

<!-- Main Footer -->
  <footer class="no-print text-center text-info">
    <!-- To the right -->
    <!-- <div class="pull-right hidden-xs">
      Anything you want
    </div> -->
    <!-- Default to the left -->
    <small>
    	<b><?php echo e(config('app.name', 'ultimatePOS')); ?> - V<?php echo e(config('author.app_version')); ?> | Copyright &copy; <?php echo e(date('Y')); ?> All rights reserved.</b>
    </small>
</footer>
<script src="../js/WebPrint/base64.js"></script>
<script src="../js/WebPrint/utf8.js"></script>
<script src="../js/WebPrint/webprint.js"></script>


<script>

  function pos_print__(receipt){

    // Create Base64 Object
var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/++[++^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

// Define the string
var string = 'بسيب!';

// Encode the String
var encodedString = Base64.encode(string);
console.log(encodedString); // Outputs: "SGVsbG8gV29ybGQh"


    //console.log(receipt.html_content);
    $('#tcphost').val("192.168.1.236");
    $('#tcpport').val("9100");
    // $('#web_print').click();

    webprint = new WebPrint(true, {
      relayHost: "127.0.0.1",
      relayPort: "8080",
      listPrinterCallback: populatePrinters,
      listPortsCallback: populatePorts,
      readyCallback: function(){
        webprint.requestPorts();
        webprint.requestPrinters();
        /*getESCPImageString("https://wallaceit.com.au/webprint/wallaceit_receipt_logo.png", function (imgdata) {
          imgData = imgdata;
          console.log("image loaded");
        });*/
      }
    });

   console.log(receipt);

   // webprint.printTcp(getEscSample('receipt', $('#cutter').is(':checked'),$('#image').is(':checked')), "HP-LaserJet-400-color-M451nw--");

    $.each(receipt.sell_lines , function(index, val) {
      var bytes = utf8.encode(val.variation_id +". "+ val.name +"\t ("+ val.quantity +")");
      var encodedData = base64.encode(bytes);
      console.log(encodedData);
      var bytes = base64.decode(encodedData);
      var text = utf8.decode(bytes);

      console.log(text);

      console.log(index, val.ip_address[0].name +':'+ val.ip_address[0].port);
      webprint.printHtml(getEscSample(val.variation_id +". "+ val.name +"\t ("+ val.quantity +")", $('#cutter').is(':checked'),$('#image').is(':checked')), val.ip_address[0].name);

      //webprint.printTcp(getEscSample(text, $('#cutter').is(':checked'),$('#image').is(':checked')), $('#tcphost').val()+':'+$('#tcpport').val());
    });

  }

var populatePrinters = function(printers){
  var printerlist = $("#printerlist");
  printerlist.html('');
  for (var i in printers){
    printerlist.append('<option value="'+printers[i]+'">'+printers[i]+'</option>');
  }
};
var populatePorts = function(ports){
  var portlist = $("#portlist");
  portlist.html('');
  for (var i in ports){
    portlist.append('<option value="'+ports[i]+'">'+ports[i]+'</option>');
  }
  if ($("#portlist option").length)
    webprint.openPort($("#portlist option:first-child").val(), {baud:"9600", databits:"8", stopbits:"1", parity:"1", flow:"none"});
};

/*webprint = new WebPrint(true, {
  relayHost: "127.0.0.1",
  relayPort: "8080",
  listPrinterCallback: populatePrinters,
  listPortsCallback: populatePorts,
  readyCallback: function(){
    webprint.requestPorts();
    webprint.requestPrinters();
    getESCPImageString("https://wallaceit.com.au/webprint/wallaceit_receipt_logo.png", function (imgdata) {
      imgData = imgdata;
      console.log("image loaded");
    });
  }
});*/

// ESC/P receipt generation
var imgData = '';
getEscSample = function(transaction, cut, image){
    var data = '';

    /* if (image)
    data+= imgData; // add the logo
    */
    data+= "\n\t\t\t" +"\n" + 'Kitchen No.' + "\n"; // heading centered example
    data+=  "\t\t\t" + transaction + "\n"; // heading centered example
    // cut the ticket
    //if (cut)
    //  data+= "\n\n\n\n" + gs_cut + "\r";

  return data;
};

var esc_init = "\x1B" + "\x40"; // initialize printer
var esc_p = "\x1B" + "\x70" + "\x30"; // open drawer
var gs_cut = "\x1D" + "\x56" + "\x4E"; // cut paper
var esc_a_l = "\x1B" + "\x61" + "\x30"; // align left
var esc_a_c = "\x1B" + "\x61" + "\x31"; // align center
var esc_a_r = "\x1B" + "\x61" + "\x32"; // align right
var esc_double = "\x1B" + "\x21" + "\x31"; // heading
var font_reset = "\x1B" + "\x21" + "\x02"; // styles off
var esc_ul_on = "\x1B" + "\x2D" + "\x31"; // underline on
var esc_bold_on = "\x1B" + "\x45" + "\x31"; // emphasis on
var esc_bold_off = "\x1B" + "\x45" + "\x30"; // emphasis off

function getEscTableRow(leftstr, rightstr, bold, underline) {
  var pad = "";
  if (leftstr.length + rightstr.length > 48) {
    var clip = (leftstr.length + rightstr) - 48; // get amount to clip
    leftstr = leftstr.substring(0, (leftstr.length - (clip + 3)));
    pad = ".. ";
  } else {
    var num = 48 - (leftstr.length + rightstr.length);
    for (num; num > 0; num--) {
      pad += " ";
    }
  }
  var row = leftstr + pad + (underline ? esc_ul_on : '') + rightstr + (underline ? font_reset : '') + "\n";
  if (bold) { // format row
    row = esc_bold_on + row + esc_bold_off;
  }
  return row;
}

function getESCPImageString(url, callback) {
  img = new Image();
  img.onload = function () {
    // Create an empty canvas element
    //var canvas = document.createElement("canvas");
    var canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;
    // Copy the image contents to the canvas
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    // get image slices and append commands
    var bytedata = esc_init + esc_a_c + getESCPImageSlices(ctx, canvas) + font_reset;
    //alert(bytedata);
    callback(bytedata);
  };
  img.src = url;
}

function getESCPImageSlices(context, canvas) {
  var width = canvas.width;
  var height = canvas.height;
  var nL = Math.round(width % 256);
  var nH = Math.round(height / 256);
  var dotDensity = 33;
  // read each pixel and put into a boolean array
  var imageData = context.getImageData(0, 0, width, height);
  imageData = imageData.data;
  // create a boolean array of pixels
  var pixArr = [];
  for (var pix = 0; pix < imageData.length; pix += 4) {
    pixArr.push((imageData[pix] == 0));
  }
  // create the byte array
  var final = [];
  // this function adds bytes to the array
  function appendBytes() {
    for (var i = 0; i < arguments.length; i++) {
      final.push(arguments[i]);
    }
  }
  // Set the line spacing to 24 dots, the height of each "stripe" of the image that we're drawing.
  appendBytes(0x1B, 0x33, 24);
  // Starting from x = 0, read 24 bits down. The offset variable keeps track of our global 'y'position in the image.
  // keep making these 24-dot stripes until we've executed past the height of the bitmap.
  var offset = 0;
  while (offset < height) {
    // append the ESCP bit image command
    appendBytes(0x1B, 0x2A, dotDensity, nL, nH);
    for (var x = 0; x < width; ++x) {
      // Remember, 24 dots = 24 bits = 3 bytes. The 'k' variable keeps track of which of those three bytes that we're currently scribbling into.
      for (var k = 0; k < 3; ++k) {
        var slice = 0;
        // The 'b' variable keeps track of which bit in the byte we're recording.
        for (var b = 0; b < 8; ++b) {
          // Calculate the y position that we're currently trying to draw.
          var y = (((offset / 8) + k) * 8) + b;
          // Calculate the location of the pixel we want in the bit array. It'll be at (y * width) + x.
          var i = (y * width) + x;
          // If the image (or this stripe of the image)
          // is shorter than 24 dots, pad with zero.
          var bit;
          if (pixArr.hasOwnProperty(i)) bit = pixArr[i] ? 0x01 : 0x00; else bit = 0x00;
          // Finally, store our bit in the byte that we're currently scribbling to. Our current 'b' is actually the exact
          // opposite of where we want it to be in the byte, so subtract it from 7, shift our bit into place in a temp
          // byte, and OR it with the target byte to get it into the final byte.
          slice |= bit << (7 - b);    // shift bit and record byte
        }
        // Phew! Write the damn byte to the buffer
        appendBytes(slice);
      }
    }
    // We're done with this 24-dot high pass. Render a newline to bump the print head down to the next line and keep on trucking.
    offset += 24;
    appendBytes(10);
  }
  // Restore the line spacing to the default of 30 dots.
  appendBytes(0x1B, 0x33, 30);
  // convert the array into a bytestring and return
  final = ArrayToByteStr(final);

  return final;
}

/**
 * @return  {string}
 */
function ArrayToByteStr(array) {
  var s = '';
  for (var i = 0; i < array.length; i++) {
    s += String.fromCharCode(array[i]);
  }
  return s;
}

//webprint.printTcp(getEscSample(false,false, '192.168.1.236'+':'+'9100'));

</script>

<p class="none">
    Host: <input id="tcphost" type="text" value="192.168.1.87" /> Port <input id="tcpport" type="text" size="5" value="9100" />
    <button onclick="webprint.printTcp(esc_init+esc_p+esc_init, $('#tcphost').val()+':'+$('#tcpport').val());">Cash Draw</button><br/>
    <button id="web_print" onclick="webprint.printTcp(getEscSample($('#cutter').is(':checked'),$('#image').is(':checked')), $('#tcphost').val()+':'+$('#tcpport').val());">Print Network</button><br/>
</p>

<p class="none">
    Cutter: <input id="cutter" type="checkbox" checked="checked"/><br/>
    Image: <input id="image" type="checkbox" checked="checked"/>
</p>


<div class="modal-dialog" role="document" style="width:90%">
  <div class="modal-content">

    {!! Form::open(['url' => action('Restaurant\ModifierSetsController@store'), 'method' => 'post', 'id' => 'table_add_form' ]) !!}

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">@lang( 'restaurant.add_modifier' )</h4>
    </div>

    <div class="modal-body">
      <div class="row">
        
        <div class="col-sm-12">
          <div class="form-group">
            {!! Form::label('name', __( 'restaurant.modifier_set' ) . ':*') !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'required', 'placeholder' => __( 'messages.name' ) ]); !!}
          </div>
        </div>

        <div class="col-sm-12">
          <h4>@lang( 'Type' )</h4>

          <div class="col-sm-6">
            <div class="form-check">
              <input class="form-check-input" type="radio" name="type[]" id="basic" value="basic" checked>
              <label class="form-check-label" for="basic">
                  Basic
              </label>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-check">
              <input class="form-check-input" type="radio" name="type[]" id="variable" value="variable">
              <label class="form-check-label" for="variable">
                  Variable
              </label>
            </div>
          </div>

          @include('restaurant.modifier_sets._choose_product')

          <div class="form-group">
            {!! Form::label('Quantity', __( 'restaurant.quantity' ) . ':*') !!}
            {!! Form::number('quantity', null, ['class' => 'form-control', 'required', 'placeholder' => __( 'restaurant.quantity' ) ]); !!}
          </div>

          <div id="min-quantity" class="form-group none">
            {!! Form::label('Min-Quantity', __( 'restaurant.min_quantity' ) . ':') !!}
            {!! Form::number('min_quantity', null, ['class' => 'form-control', 'placeholder' => __( 'restaurant.min_quantity' ) ]); !!}
          </div>

        </div>


        <div id="modifiers" class="col-sm-12 none">
          <h4>@lang( 'restaurant.modifiers' )</h4>
          <table class="table table-condensed" id="add-modifier-table">
            <thead>
              <tr>
                <th>@lang( 'restaurant.modifier')</th>
                <th>
                  @lang( 'messages.price')
                  
                  @php
                    $html = '<tr><td>
                          <div class="form-group modifier-products col-sm-10">'.
                            Form::select('products', $products, null, ['placeholder' => __('messages.please_select'), 'class' => 'modifier_product_id col-sm-6 form-control select2', 'style' => 'width:100%', 'onClick' => 'modifier_product_id(this)' ])
                            .'</div><div class="form-group col-sm-2"><span class="input-group-btn">
                                <button type="button" class="btn btn-default bg-white btn-flat other-modifier" data-name="" onclick="otherFunction(this)"> <i class="fa fa-window-restore text-primary fa-lg"></i> Other</button>
                              </span></div><div class="modifier-box col-sm-10 form-group none">
                              <input type="text" name="modifier_name[]"
                              class="modifier_name form-control" 
                              placeholder="' . __( 'messages.please_enter' ) . '" required>
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              <input type="text" name="modifier_price[]" class="modifier_price form-control input_number" 
                              placeholder="' . __( 'messages.price' ) . '" required>
                            </div>
                          </td>
                          <td>
                            <div class="form-check text-center">
                              <input class="form-check-input" type="radio" name="modifier_default[]" id="default" value="default">
                            </div>
                          </td>';

                    $html_other_row = $html . '<td>
                            <button class="btn btn-danger btn-xs pull-right remove-modifier-row" type="button"><i class="fa fa-minus"></i></button>
                          </td>
                        </tr>';

                    $html_first_row = $html . "<td>
                            <button class='btn btn-primary btn-xs pull-right add-modifier-row' type='button'
                            data-html='{{ $html_other_row }}'>
                            <i class='fa fa-plus'></i></button>
                          </td>
                        </tr>";

                  @endphp
                  
                  
                </th>
                <th class="text-center">Default</th>
                <th>&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              {!! $html_first_row !!}
            </tbody>
          </table>
        </div>

      </div>
    </div>

    <div class="modal-footer">
      <button type="submit" class="btn btn-primary">@lang( 'messages.save' )</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">@lang( 'messages.close' )</button>
    </div>

    {!! Form::close() !!}

  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->

<script>
  $("select#product_id"). change(function(){
    $('.modifier_name').val( "product:" + $( this ).val() );
    $('.modifier_price').val( "product:" + $( this ).val() );
  });
  $("input#basic"). click(function(){
    $('#products-box').show();
    $('#modifiers').hide();
    $('#min-quantity').hide();
    $(this).prop("checked", true);
    $("input#variable").prop("checked", false);
    $("#product_id").val($( "#product_id option:first").val() );
  });
  $("input#variable"). click(function(){
    $('#products-box').hide();
    $('#products-box select').val('');
    $('#modifiers').show();
    $('#min-quantity').show();
    $(this).prop("checked", true);
    $("input#basic").prop("checked", false);
    $('.modifier_name').val( "" );
    $('.modifier_price').val( "" );
  });

 function otherFunction(a){
    $(a).closest("td").find(".modifier-box").toggle();
    $(a).closest("td").find(".modifier-products").toggle();
    $(a).closest("td").find(".modifier_product_id").val('');
    $(a).closest("td").find(".modifier_name").val('');
  };

  function modifier_product_id(b){
    $(b).closest("tr").find('.modifier_name').val( "product:" + $( b ).val() );
    // $(this).closest("tr").find('.modifier_price').val( "product:" + $( this ).val() );
  };

</script>
<?php $__env->startSection('title', 'POS'); ?>

<?php $__env->startSection('content'); ?>

<!-- Content Header (Page header) -->
<!-- <section class="content-header">
    <h1>Add Purchase</h1> -->
    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
    </ol> -->
<!-- </section> -->
<input type="hidden" id="__precision" value="<?php echo e(config('constants.currency_precision')); ?>">

<!-- Main content -->
<section class="content no-print pos-screen">
	<div class="row">
		<div class="col-md-6 col-sm-12">
			<div class="col-md-12 col-sm-12 actions-pos">
				<button type="button" class="col-lg-6 col-md-6 col-sm-6 btn btn-lg btn-danger" onclick="location.reload();"><i class="fa fa-shopping-bag" aria-hidden="true"></i>
				<?php echo app('translator')->getFromJson('sale.takeaway'); ?></button>
				<button type="button" class="col-lg-6 col-md-6 col-sm-6 btn btn-lg dine-in" onclick="location.href ='/pos/create_dine';"><i class="fa fa-cutlery" aria-hidden="true"></i>
				<?php echo app('translator')->getFromJson('sale.dine_in'); ?></button>
				<button type="button" class="col-lg-20 col-md-6 col-sm-6 btn btn-lg drive-thru hidden" onclick="location.reload();"><i class="fa fa-car" aria-hidden="true"></i>
				<?php echo app('translator')->getFromJson('sale.drive_thu'); ?></button>
				<button type="button" class="col-lg-3 col-md-3 col-sm-6 btn btn-lg delivery hidden" onclick="location.href ='/pos/create_delivery';"><i class="fa fa-motorcycle" aria-hidden="true"></i>
				<?php echo app('translator')->getFromJson('sale.home_delivery'); ?></button>
				<button type="button" class="col-lg-3 col-md-3 col-sm-4 btn btn-lg parties hidden" onclick="location.href ='/pos/create_party';"><i class="fa fa-video-camera" aria-hidden="true"></i>
				<?php echo app('translator')->getFromJson('sale.parties'); ?></button>
			</div>

			<div class="col-md-12 col-sm-12">
				<div class="box box-danger">

					<div class="box-header">
						<div class="">
							<?php if($pos_settings['hide_recent_trans'] == 0): ?>
									<?php echo $__env->make('sale_pos.partials.recent_transactions_box', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
							<?php endif; ?>
						</div>
						<p class="col-sm-12 hidden">
							<br>
							<button id="btn-ta" class="btn btn-primary pull-right" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
								<span class="ta-title"><?php echo app('translator')->getFromJson('sale.local'); ?></span> <i class="fa fa-arrow-down" aria-hidden="true"></i>
							</button>
						</p>
						<div class="col-sm-12 collapse" id="collapseExample">
							<div class="row card card-body">
								<div class="col-lg-3 col-md-2 col-sm-4">
									<h2 class="ta-icon text-primary" val="LOCAL">LOCAL</h2>
								</div>
								<div class="col-lg-3 col-md-2 col-sm-4">
									<img class="ta-icon" src="/takeaway/hungerstation.png" val="HungerStation" width="110">
								</div>
								<div class="col-lg-3 col-md-2 col-sm-4">
									<img class="ta-icon" src="/takeaway/ubereats.png" val="UberEats" width="110">
								</div>
								<div class="col-lg-3 col-md-2 col-sm-4">
									<img class="ta-icon" src="/takeaway/talabat.png" val="Talabat" width="110">
								</div>
								<div class="col-lg-3 col-md-2 col-sm-4">
									<img class="ta-icon" src="/takeaway/carriage.png" val="Carriage" width="110">
								</div>
								<div class="col-lg-3 col-md-2 col-sm-4">
									<h2 class="ta-icon text-success" val="OTHER">OTHER</h2>
								</div>
							</div>
						</div>
						<input type="hidden" id="item_addition_method" value="<?php echo e($business_details->item_addition_method); ?>">
						<?php if(is_null($default_location)): ?>
							<div class="col-sm-6">
								<div class="form-group" style="margin-bottom: 0px;">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-map-marker"></i>
										</span>
									<?php echo Form::select('select_location_id', $business_locations, null, ['class' => 'form-control input-sm mousetrap', 
									'placeholder' => __('lang_v1.select_location'),
									'id' => 'select_location_id', 
									'required', 'autofocus'], $bl_attributes);; ?>

									<span class="input-group-addon">
											<?php
                if(session('business.enable_tooltip')){
                    echo '<i class="fa fa-info-circle text-info hover-q " aria-hidden="true" 
                    data-container="body" data-toggle="popover" data-placement="auto" 
                    data-content="' . __('tooltip.sale_location') . '" data-html="true" data-trigger="hover"></i>';
                }
                ?>
										</span> 
									</div>
								</div>
							</div>
						<?php endif; ?>
					</div>

					<?php echo Form::open(['url' => action('SellPosController@store'), 'method' => 'post', 'id' => 'add_pos_sell_form' ]); ?>


					<?php echo Form::hidden('location_id', $default_location, ['id' => 'location_id', 'data-receipt_printer_type' => isset($bl_attributes[$default_location]['data-receipt_printer_type']) ? $bl_attributes[$default_location]['data-receipt_printer_type'] : 'browser']);; ?>


					<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<?php if(config('constants.enable_sell_in_diff_currency') == true): ?>
								<div class="col-md-4 col-sm-6">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">
												<i class="fa fa-exchange"></i>
											</span>
											<?php echo Form::text('exchange_rate', config('constants.currency_exchange_rate'), ['class' => 'form-control input-sm input_number', 'placeholder' => __('lang_v1.currency_exchange_rate'), 'id' => 'exchange_rate']);; ?>

										</div>
									</div>
								</div>
							<?php endif; ?>
							<?php if(!empty($price_groups)): ?>
								<?php if(count($price_groups) > 1): ?>
									<div class="col-md-4 col-sm-6">
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-money"></i>
												</span>
												<?php
													reset($price_groups);
												?>
												<?php echo Form::hidden('hidden_price_group', key($price_groups), ['id' => 'hidden_price_group']); ?>

												<?php echo Form::select('price_group', $price_groups, null, ['class' => 'form-control select2', 'id' => 'price_group']);; ?>

												<span class="input-group-addon">
													<?php
                if(session('business.enable_tooltip')){
                    echo '<i class="fa fa-info-circle text-info hover-q " aria-hidden="true" 
                    data-container="body" data-toggle="popover" data-placement="auto" 
                    data-content="' . __('lang_v1.price_group_help_text') . '" data-html="true" data-trigger="hover"></i>';
                }
                ?>
												</span> 
											</div>
										</div>
									</div>
								<?php else: ?>
									<?php
										reset($price_groups);
									?>
									<?php echo Form::hidden('price_group', key($price_groups), ['id' => 'price_group']); ?>

								<?php endif; ?>
							<?php endif; ?>
						</div>
						<div class="row">
							<div class="<?php if(!empty($commission_agent)): ?> col-sm-4 <?php else: ?> col-sm-6 <?php endif; ?>">
								<div class="form-group" style="width: 100% !important">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-user"></i>
										</span>
										<input type="hidden" id="default_customer_id" 
										value="<?php echo e($walk_in_customer['id']); ?>" >
										<input type="hidden" id="default_customer_name" 
										value="<?php echo e($walk_in_customer['name']); ?>" >
										<?php echo Form::select('contact_id', 
											[], null, ['class' => 'form-control mousetrap', 'id' => 'customer_id', 'placeholder' => 'Enter Customer name / phone', 'required']);; ?>

										<span class="input-group-btn">
											<button type="button" class="btn btn-default bg-white btn-flat add_new_customer" data-name=""  <?php if(!auth()->user()->can('customer.create')): ?> disabled <?php endif; ?>><i class="fa fa-plus-circle text-primary fa-lg"></i></button>
										</span>
									</div>
								</div>
							</div>

							<?php if(!empty($commission_agent)): ?>
								<div class="col-sm-4">
									<div class="form-group">
									<?php echo Form::select('commission_agent', 
												$commission_agent, null, ['class' => 'form-control select2', 'placeholder' => __('lang_v1.commission_agent')]);; ?>

									</div>
								</div>
							<?php endif; ?>

							<div class="<?php if(!empty($commission_agent)): ?> col-sm-4 <?php else: ?> col-sm-6 <?php endif; ?>">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-barcode"></i>
										</span>
										<?php echo Form::text('search_product', null, ['class' => 'form-control mousetrap', 'id' => 'search_product', 'placeholder' => __('lang_v1.search_product_placeholder'),
										'disabled' => is_null($default_location)? true : false,
										'autofocus' => is_null($default_location)? false : true,
										]);; ?>

									</div>
								</div>
							</div>
							<div class="clearfix"></div>

							<!-- Call restaurant module if defined -->
									<?php if(in_array('tables' ,$enabled_modules) || in_array('service_staff' ,$enabled_modules)): ?>
										<span id="restaurant_module_span">
												<div class="col-md-3"></div>
										</span>
									<?php endif; ?>
								</div>

						<div class="pos_product_div">

							<input type="hidden" name="sell_price_tax" id="sell_price_tax" value="<?php echo e($business_details->sell_price_tax); ?>">

							<!-- Keeps count of product rows -->
							<input type="hidden" id="product_row_count" 
								value="0">
							<?php
								$hide_tax = '';
								if( session()->get('business.enable_inline_tax') == 0){
									$hide_tax = 'hide';
								}
							?>
							<table class="table table-condensed table-bordered table-striped table-responsive" id="pos_table">
								<thead>
									<tr>
										<th class="tex-center col-md-4">	
											<?php echo app('translator')->getFromJson('sale.product'); ?> <?php
                if(session('business.enable_tooltip')){
                    echo '<i class="fa fa-info-circle text-info hover-q " aria-hidden="true" 
                    data-container="body" data-toggle="popover" data-placement="auto" 
                    data-content="' . __('lang_v1.tooltip_sell_product_column') . '" data-html="true" data-trigger="hover"></i>';
                }
                ?>
										</th>
										<th class="text-center col-md-3">
											<?php echo app('translator')->getFromJson('sale.qty'); ?>
										</th>
										<th class="text-center col-md-2 <?php echo e($hide_tax); ?>">
											<?php echo app('translator')->getFromJson('sale.price_inc_tax'); ?>
										</th>
										<th class="text-center col-md-3">
											<?php echo app('translator')->getFromJson('sale.subtotal'); ?>
										</th>
										<th class="text-center"><i class="fa fa-times" aria-hidden="true"></i></th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
						<?php echo $__env->make('sale_pos.partials.pos_payment_details', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

						<?php echo $__env->make('sale_pos.partials.payment_modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					</div>
					<!-- /.box-body -->
					<?php echo Form::close(); ?>

				</div>
				</div>	<!-- /.box -->
			</div>

		<div class="col-md-6 col-sm-12">
			<div class="new-menu box box-widget">
				<div class="box-body">
					<?php $__currentLoopData = $menu_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php if($menu_item->type == "category"): ?>
							<div class="category-btn col-md-3 col-xs-4 btn-default" 
							style="background: url(<?php echo e($menu_item->image_path); ?>) <?php echo e($menu_item->text_bg_color); ?>; background-size:cover; color: <?php echo e($menu_item->text_fg_color); ?>" 
							value="<?php echo e($menu_item->category_id); ?>"><?php echo e($menu_item->name); ?></div>
						<?php endif; ?>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
			</div>
			<div class="col-md-3 hidden col-sm-12">
				<?php echo $__env->make('sale_pos.partials.right_categories_div', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			</div>
			<div class="menu-items col-md-12 none col-sm-12">
				<?php echo $__env->make('sale_pos.partials.right_div', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			</div>
			<div class="col-md-12 col-sm-12">
				<?php echo $__env->make('sale_pos.partials.pos_details', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			</div>
		</div>

		<div id="change-back" class="col-md-12 ">
			<i class="fa fa-random" aria-hidden="true"></i>
		</div>
	</div>
</section>

<!-- This will be printed -->
<section class="invoice print_section" id="receipt_section">
</section>
<div class="modal fade contact_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
	<?php echo $__env->make('contact.create', ['quick_add' => true], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>
<!-- /.content -->
<div class="modal fade register_details_modal" tabindex="-1" role="dialog" 
	aria-labelledby="gridSystemModalLabel">
</div>
<div class="modal fade close_register_modal hidden" tabindex="-1" role="dialog" 
	aria-labelledby="gridSystemModalLabel">
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
	<script src="<?php echo e(asset('js/pos.js?v=' . $asset_v)); ?>"></script>
	<script src="<?php echo e(asset('js/printer.js?v=' . $asset_v)); ?>"></script>
	<?php echo $__env->make('sale_pos.partials.keyboard_shortcuts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<!-- Call restaurant module if defined -->
    <?php if(in_array('tables' ,$enabled_modules) || in_array('modifiers' ,$enabled_modules) || in_array('service_staff' ,$enabled_modules)): ?>
		<script src="<?php echo e(asset('js/restaurant.js?v=' . $asset_v)); ?>"></script>
    <?php endif; ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<style>
span#restaurant_module_span, span.select2.select2-container.select2-container--default.select2-container--focus {
    display: none;
}
</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
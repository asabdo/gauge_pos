<?php
  $id = 'modifier_' . $row_count . '_' . time();
?>
<div>
  <span class="selected_modifiers">
    <?php if(!empty($edit_modifiers) && !empty($product->modifiers) ): ?>
      <?php echo $__env->make('restaurant.product_modifier_set.add_selected_modifiers', array('index' => $row_count, 'modifiers' => $product->modifiers ) , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
  </span>&nbsp;
  <i class="fa fa-external-link-square cursor-pointer text-primary select-modifiers-btn" title="<?php echo e(app('translator')->getFromJson('restaurant.modifiers_for_product')); ?>" data-toggle="modal" data-target="#<?php echo e($id); ?>"></i>
</div>
<div class="modal fade modifier_modal" id="<?php echo e($id); ?>" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document" style="width: 98%">
  <div class="modal-content">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title"><?php echo e(app('translator')->getFromJson( 'restaurant.modifiers_for_product' )); ?>: <span class="text-success"></span>
      </h4>
    </div>

    <div class="modal-body">

      <div class="col-md-4">
          <?php if(!empty($product_ms)): ?>
        <div class="panel-group" id="accordion<?php echo e($id); ?>" role="tablist" aria-multiselectable="true">

          <?php $__currentLoopData = $product_ms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $modifier_set): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php
              $collapse_id = 'collapse'. $modifier_set->id . $id;
            ?>

            <div class="<?php echo e($modifier_set->name . $modifier_set->id); ?>-btn panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <span class="glyphicon glyphicon-tags"></span>
                <h3 class="text-center">
                  <a role="button" data-toggle="collapse" data-parent="#accordion<?php echo e($id); ?>" 
                    href="#<?php echo e($collapse_id); ?>" 
                    aria-expanded="true" aria-controls="collapseOne">
                     <?php echo e($modifier_set->name); ?>

                  </a>
                </h3>
              </div>
            </div>
            
          
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    
        </div>
          <?php endif; ?>

      </div>
      <div class="col-md-8">
        <?php if(!empty($product_ms)): ?>
          <div class="panel-group" id="accordion<?php echo e($id); ?>" role="tablist" aria-multiselectable="true">

        <?php $__currentLoopData = $product_ms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $modifier_set): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <?php
            $collapse_id = 'collapse'. $modifier_set->id . $id;
          ?>

          <div class="<?php echo e($modifier_set->name . $modifier_set->id); ?> panel panel-default modifier-panel <?php if($index != 0): ?> none <?php endif; ?>">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion<?php echo e($id); ?>" 
                  href="#<?php echo e($collapse_id); ?>" 
                  aria-expanded="true" aria-controls="collapseOne">
                  <?php echo e($modifier_set->name); ?>

                </a>
              </h4>
            </div>
            <input type="hidden" class="modifiers_exist" value="true">
            <input type="hidden" class="index" value="<?php echo e($row_count); ?>">

            <div id="{}" class="panel-collapse --collapse <?php if($loop->index==0): ?> in <?php endif; ?>" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <div class="btn-group" data-toggle="buttons">
                  <?php $__currentLoopData = $modifier_set->variations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $modifier): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <label class="btn bg-grey <?php if(!empty($edit_modifiers) && in_array($modifier->id, $product->modifiers_ids) ): ?> active <?php endif; ?>">
                      <input type="checkbox" autocomplete="off" 
                        value="<?php echo e($modifier->id); ?>" <?php if(!empty($edit_modifiers) && in_array($modifier->id, $product->modifiers_ids) ): ?> checked <?php endif; ?>> 
                        <div class="col-sm-6"> 
                          <div class="col-sm-6"> 
                            <input type="text" name="mod<?php echo e($modifier->id); ?>" class="prod-mod-<?php echo e($modifier_set->pivot->product_id); ?> form-control input-number text-center" value="0" min="0" max="10">
                          </div>
                          <div class="col-sm-6"> 
                          <?php echo e($modifier->name); ?>

                          <div class="mtb-price">
                            <b class="lb2">></b>
                            <span class="sp_amt"><?php echo e($modifier->default_sell_price); ?></span>
                            <span class="sp_currencysyb ng-binding">SAR </span>
                          </div>
                        </div>
                        </div>
                        <div class="button_group col-sm-6">
                            <div class="input-group">
                                <span class="input-group-btn pull-right">
                                    <button type="button" class="btn btn-lg btn-danger btn-number"  data-type="minus" data-field="mod<?php echo e($modifier->id); ?>">
                                      <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </span>
                                <span class="input-group-btn pull-left">
                                    <button type="button" class="btn btn-lg btn-success btn-number" data-type="plus" data-field="mod<?php echo e($modifier->id); ?>">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </label>
                    <hr class="row col-sm-12">

                    <script>
                        $(".<?php echo e($modifier_set->name . $modifier_set->id); ?>-btn").click(function() {
                          $(".modifier-panel").hide();
                          $(".<?php echo e($modifier_set->name . $modifier_set->id); ?>").show();
                        });
                    </script>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
              </div>
            </div>
          </div>
            
          
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

          </div>
        <?php endif; ?>
      </div>
    </div>

    <div class="modal-footer">
      <button type="button" class="btn btn-primary add_modifier" data-dismiss="modal">
        <?php echo e(app('translator')->getFromJson( 'messages.add' )); ?></button>
      <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo e(app('translator')->getFromJson( 'messages.close' )); ?></button>
    </div>

    <?php echo Form::close(); ?>


  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">
if( typeof $ !== 'undefined'){
  $(document).ready(function(){
    $('div#<?php echo e($id); ?>').modal('show');
  });
}

//plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

   
</script>
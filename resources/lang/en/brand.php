<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Brand Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Brand CRUD operations.
    |
    */

    'brands' => 'Type',
    'manage_your_brands' => 'Manage your types',
    'all_your_brands' => 'All your types',
    'note' => 'Note',
    'brand_name' => 'Type name',
    'short_description' => 'Short description',
    'added_success' => 'Type added successfully',
    'updated_success' => 'Type updated successfully',
    'deleted_success' => 'Type deleted successfully',
    'add_brand' => 'Add Type',
    'edit_brand' => 'Edit Type',

];

<div class="box box-widget">
	<div class="box-header with-border">
	@if(!empty($categories))
		<select class="" id="product_category" style="width:90% !important">

			<option value="all">@lang('lang_v1.all_category')</option>

			@foreach($categories as $category)
				<option value="{{$category['id']}}">{{$category['name']}}</option>
			@endforeach

			@foreach($categories as $category)
				@if(!empty($category['sub_categories']))
					<optgroup label="{{$category['name']}}">
						@foreach($category['sub_categories'] as $sc)
							<i class="fa fa-minus"></i> <option value="{{$sc['id']}}">{{$sc['name']}}</option>
						@endforeach
					</optgroup>
				@endif
			@endforeach
		</select>

		<div class="navbar-header col-sm-12">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<i class="fa fa-bars" title="Edit Discount"></i>
			</button>
		</div>
		<div id="navbar" class="navbar-collapse collapse row" aria-expanded="false">
			@foreach($categories as $category)
				@if(!empty($category['sub_categories']))
					<div class="row mar-top">
						<div id="select-category" class="col-sm-12" label="{{$category['name']}}">
							<button type="button" class="category-btn col-sm-12 btn btn-default" value="all">ALL</button>
							@foreach($category['sub_categories'] as $sc)
								<button type="button" class="category-btn col-md-12 col-sm-6 btn btn-default" value="{{$sc['id']}}">{{$sc['name']}}</button>
							@endforeach
						</div>
					</div>
				@endif
			@endforeach
			<button type="button" id="category-change" class="category-btn col-sm-12 btn btn-primary" value="change-category">Breakfast</button>
		</div>
	@endif
	@inject('request', 'Illuminate\Http\Request')

	<div class="hidden">
			{!! Form::select('size', $brands, null, ['id' => 'product_brand', 'class' => 'select2', 'name' => null, 'style' => 'width:45% !important']) !!}
	</div>
	@if(!empty($brands) && $request->segment(2) == 'create_party')
		&nbsp;

		<script>
			document.onreadystatechange = function () {
				/*$('#product_brand').val('1').trigger('change');*/
			};
		</script>
	@endif

	<!-- /.box-tools -->
	</div>
	<!-- /.box-header -->

</div>


<style>

.select2-container--default .select2-results>.select2-results__options {
	max-height: 595px;
}
</style>
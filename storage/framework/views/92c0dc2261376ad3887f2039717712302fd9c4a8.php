<!-- business information here -->

<div class="row">

	<!-- Logo -->
	<?php if(!empty($receipt_details->logo)): ?>
		<img src="<?php echo e($receipt_details->logo); ?>" class="img img-responsive center-block">
	<?php endif; ?>

	<!-- Header text -->
	<?php if(!empty($receipt_details->header_text)): ?>
		<div class="col-xs-12">
			<?php echo $receipt_details->header_text; ?>

		</div>
	<?php endif; ?>

	<!-- business information here -->
	<div class="col-xs-12 text-center">
		<h2 class="text-center">
			<!-- Shop & Location Name  -->
			<?php if(!empty($receipt_details->display_name)): ?>
				<?php echo e($receipt_details->display_name); ?>

			<?php endif; ?>
		</h2>

		<!-- Address -->
		<p>
		<?php if(!empty($receipt_details->address)): ?>
				<small class="text-center">
				<?php echo $receipt_details->address; ?>

				</small>
		<?php endif; ?>
		<?php if(!empty($receipt_details->contact)): ?>
			<br/><?php echo e($receipt_details->contact); ?>

		<?php endif; ?>	
		<?php if(!empty($receipt_details->contact) && !empty($receipt_details->website)): ?>
			, 
		<?php endif; ?>
		<?php if(!empty($receipt_details->website)): ?>
			<?php echo e($receipt_details->website); ?>

		<?php endif; ?>
		<?php if(!empty($receipt_details->location_custom_fields)): ?>
			<br><?php echo e($receipt_details->location_custom_fields); ?>

		<?php endif; ?>
		</p>
		<p>
		<?php if(!empty($receipt_details->sub_heading_line1)): ?>
			<?php echo e($receipt_details->sub_heading_line1); ?>

		<?php endif; ?>
		<?php if(!empty($receipt_details->sub_heading_line2)): ?>
			<br><?php echo e($receipt_details->sub_heading_line2); ?>

		<?php endif; ?>
		<?php if(!empty($receipt_details->sub_heading_line3)): ?>
			<br><?php echo e($receipt_details->sub_heading_line3); ?>

		<?php endif; ?>
		<?php if(!empty($receipt_details->sub_heading_line4)): ?>
			<br><?php echo e($receipt_details->sub_heading_line4); ?>

		<?php endif; ?>		
		<?php if(!empty($receipt_details->sub_heading_line5)): ?>
			<br><?php echo e($receipt_details->sub_heading_line5); ?>

		<?php endif; ?>
		</p>
		<p>
		<?php if(!empty($receipt_details->tax_info1)): ?>
			<b><?php echo e($receipt_details->tax_label1); ?></b> <?php echo e($receipt_details->tax_info1); ?>

		<?php endif; ?>

		<?php if(!empty($receipt_details->tax_info2)): ?>
			<b><?php echo e($receipt_details->tax_label2); ?></b> <?php echo e($receipt_details->tax_info2); ?>

		<?php endif; ?>
		</p>

		<!-- Title of receipt -->
		<?php if(!empty($receipt_details->invoice_heading)): ?>
			<h3 class="text-center">
				<?php echo $receipt_details->invoice_heading; ?>

			</h3>
		<?php endif; ?>

		<!-- Invoice  number, Date  -->
		<p style="width: 100% !important" class="word-wrap">
			<span class="pull-left text-left word-wrap">
				<?php if(!empty($receipt_details->invoice_no_prefix)): ?>
					<b><?php echo $receipt_details->invoice_no_prefix; ?></b>
				<?php endif; ?>
				<?php echo e($receipt_details->invoice_no); ?>


				<!-- Table information-->
		        <?php if(!empty($receipt_details->table_label) || !empty($receipt_details->table)): ?>
		        	<br/>
					<span class="pull-left text-left">
						<?php if(!empty($receipt_details->table_label)): ?>
							<b><?php echo $receipt_details->table_label; ?></b>
						<?php endif; ?>
						<?php echo e($receipt_details->table); ?>


						<!-- Waiter info -->
					</span>
		        <?php endif; ?>

				<!-- customer info -->
				<?php if(!empty($receipt_details->customer_info)): ?>
					<br/>
					<b><?php echo e($receipt_details->customer_label); ?></b> <?php echo $receipt_details->customer_info; ?>

				<?php endif; ?>
				<?php if(!empty($receipt_details->client_id_label)): ?>
					<br/>
					<b><?php echo e($receipt_details->client_id_label); ?></b> <?php echo e($receipt_details->client_id); ?>

				<?php endif; ?>
				<?php if(!empty($receipt_details->customer_tax_label)): ?>
					<br/>
					<b><?php echo e($receipt_details->customer_tax_label); ?></b> <?php echo e($receipt_details->customer_tax_number); ?>

				<?php endif; ?>
				<?php if(!empty($receipt_details->customer_custom_fields)): ?>
					<br/><?php echo $receipt_details->customer_custom_fields; ?>

				<?php endif; ?>
				<?php if(!empty($receipt_details->sales_person_label)): ?>
					<br/>
					<b><?php echo e($receipt_details->sales_person_label); ?></b> <?php echo e($receipt_details->sales_person); ?>

				<?php endif; ?>
			</span>

			<span class="pull-right">
				<b><?php echo e($receipt_details->date_label); ?></b> <?php echo e($receipt_details->invoice_date); ?>

				
				<!-- Waiter info -->
				<?php if(!empty($receipt_details->service_staff_label) || !empty($receipt_details->service_staff)): ?>
		        	<br/>
					<?php if(!empty($receipt_details->service_staff_label)): ?>
						<b><?php echo $receipt_details->service_staff_label; ?></b>
					<?php endif; ?>
					<?php echo e($receipt_details->service_staff); ?>

		        <?php endif; ?>
			</span>
		</p>
	</div>
	<!-- /.col -->
</div>


<div class="row">
	<div class="col-xs-12">
		<br/><br/>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th><?php echo e($receipt_details->table_product_label); ?></th>
					<th><?php echo e($receipt_details->table_qty_label); ?></th>
					<th><?php echo e($receipt_details->table_subtotal_label); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php $__empty_1 = true; $__currentLoopData = $receipt_details->lines; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $line): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
					<tr>
						<td>
                            <?php echo e($line['name']); ?>  
                            <?php if(!empty($line['sub_sku'])): ?>, <?php echo e($line['sub_sku']); ?> <?php endif; ?> <?php if(!empty($line['brand'])): ?>, <?php echo e($line['brand']); ?> <?php endif; ?> <?php if(!empty($line['cat_code'])): ?>, <?php echo e($line['cat_code']); ?><?php endif; ?>
                            <?php if(!empty($line['sell_line_note'])): ?>(<?php echo e($line['sell_line_note']); ?>) <?php endif; ?> 
                            <?php if(!empty($line['lot_number'])): ?><br> <?php echo e($line['lot_number_label']); ?>:  <?php echo e($line['lot_number']); ?> <?php endif; ?> 
                            <?php if(!empty($line['product_expiry'])): ?>, <?php echo e($line['product_expiry_label']); ?>:  <?php echo e($line['product_expiry']); ?> <?php endif; ?> 
                        </td>
						<td><?php echo e($line['quantity']); ?> <?php echo e($line['units']); ?> </td>
						<td><?php echo e($line['line_total']); ?></td>
					</tr>
					<?php if(!empty($line['modifiers'])): ?>
						<?php $__currentLoopData = $line['modifiers']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $modifier): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td>
		                            <?php echo e($modifier['name']); ?> <?php echo e($modifier['variation']); ?> 
		                            <?php if(!empty($modifier['sub_sku'])): ?>, <?php echo e($modifier['sub_sku']); ?> <?php endif; ?> <?php if(!empty($modifier['cat_code'])): ?>, <?php echo e($modifier['cat_code']); ?><?php endif; ?>
		                            <?php if(!empty($modifier['sell_line_note'])): ?>(<?php echo e($modifier['sell_line_note']); ?>) <?php endif; ?> 
		                        </td>
								<td><?php echo e($modifier['quantity']); ?> <?php echo e($modifier['units']); ?> </td>
								<td><?php echo e($modifier['unit_price_inc_tax']); ?></td>
								<td><?php echo e($modifier['line_total']); ?></td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
					<tr>
						<td colspan="4">&nbsp;</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<br/>
	<div class="col-md-12"><hr/></div>
	<br/>


	<div class="col-xs-12">

		<table class="table table-condensed">

			<?php if(!empty($receipt_details->payments)): ?>
				<?php $__currentLoopData = $receipt_details->payments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr>
						<td><?php echo e($payment['method']); ?></td>
						<td><?php echo e($payment['amount']); ?></td>
						<td><?php echo e($payment['date']); ?></td>
					</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<?php endif; ?>

			<!-- Total Paid-->
			<?php if(!empty($receipt_details->total_paid)): ?>
				<tr>
					<th>
					&nbsp;
					</th>
					<th>
						<?php echo $receipt_details->total_paid_label; ?>

					</th>
					<td>
						<?php echo e($receipt_details->total_paid); ?>

					</td>
				</tr>
			<?php endif; ?>

			<!-- Total Due-->
			<?php if(!empty($receipt_details->total_due)): ?>
			<tr>
				<th>
					<?php echo $receipt_details->total_due_label; ?>

				</th>
				<td>
					<?php echo e($receipt_details->total_due); ?>

				</td>
			</tr>
			<?php endif; ?>
		</table>

		<?php echo e($receipt_details->additional_notes); ?>

	</div>

	<div class="col-xs-12">
        <div class="table-responsive">
			<table class="table">
				<tbody>
					<tr>
						<th style="width:70%">
							<?php echo $receipt_details->subtotal_label; ?>

						</th>
						<td>
							<?php echo e($receipt_details->subtotal); ?>

						</td>
					</tr>

					<!-- Shipping Charges -->
					<?php if(!empty($receipt_details->shipping_charges)): ?>
						<tr>
							<th style="width:70%">
								<?php echo $receipt_details->shipping_charges_label; ?>

							</th>
							<td>
								<?php echo e($receipt_details->shipping_charges); ?>

							</td>
						</tr>
					<?php endif; ?>

					<!-- Discount -->
					<?php if( !empty($receipt_details->discount) ): ?>
						<tr>
							<th>
								<?php echo $receipt_details->discount_label; ?>

							</th>

							<td>
								(-) <?php echo e($receipt_details->discount); ?>

							</td>
						</tr>
					<?php endif; ?>

					<!-- Tax -->
					<?php if( !empty($receipt_details->tax) ): ?>
						<tr>
							<th>
								<?php echo $receipt_details->tax_label; ?>

							</th>
							<td>
								(+) <?php echo e($receipt_details->tax); ?>

							</td>
						</tr>
					<?php endif; ?>

					<!-- Total -->
					<tr>
						<td>
							<h3>To Pay: </h3>
						</td>
						<td>
							<h3><?php echo e($receipt_details->total); ?></h3>
						</td>
						<td>
							<h3>المطلوب</h3>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<div style="text-align:center">
								VAT 5%  يشمل ضريبة القيمة المضافة
							</div>
						<td>
					</tr>
				</tbody>
			</table>
        </div>
    </div>
</div>

<?php if($receipt_details->show_barcode): ?>
	<div class="row">
		<div class="col-xs-12">
			
			<img class="center-block" src="data:image/png;base64,<?php echo e(DNS1D::getBarcodePNG($receipt_details->invoice_no, 'C128', 2,30,array(39, 48, 54), true)); ?>">
		</div>
	</div>
<?php endif; ?>
<div style="text-align:center">
	<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJMAAAArCAYAAAB8fLoNAAATT0lEQVR42u1cZ1dU6ZrtfzE/4U7fmW5bBckUVIAihyKD5CxiAAUJoigKgoEsQVFEBAHJIElysATtbl039If+DTOO666+45q15+zHgcYGRQpo6DW8a511qs55z1sn7NrPfsJ7vvpqv+23/bbf9tt+22/7bW+3Z2MD6O/rwPizQSwapzOnJocxNtoPrvfvzn7bsBEo1VU3cfpUMgL8PaF3doCHuw6hIQYEBXrDz+CO8KMByDp3Grerb6G3pw2vXy/8y/6d228ftY72h8g4ewLOTio4qCzh7qaFr4+rLP5+HvD20sPVRQ2d1g5qR2to1DYIDvLB9ZICkMX27+Aut7mZCXkIPywZ9fOzkxgbGURvdwcePmhYeTjct5Pn0NpyX5jGRe8IH28XFF69gM4nj9D0oB7dXa344dVC6Y/fGzMXX0y/5feK8hJkZ6UJU9namEGrsUVEeKDCUq1/bEANDHSjsvIWrl0rQHJyHOLjoxAdfRRxcZE4efIYLl7MQVnZddTXV2N2dmJXLhZ4t64Z6O/tRFxMBAL8FPPh64noyDB4ebjAxVkDB3trhAb7I+JoMG5eL8JOASrvfIYwD83Y1St5GB7qWblHP79+ve55v3mzpH++MIm+3nbcuH4FR8P8YWN9WDGBrigvK/rjAcponMssLr4Cb283ODjYwM3NCe7uzrK4uurg5OQItdpOFp3OAS4uWvj4uKOrq21XL/af/3x3YGZqAvGx0fD19oTGUQU3F2ccMTsEa8sjsLOxgqe7qwIub1nbWFnA4OOlACsQo8NPt/XcqY1U9haig64V5ePHH42RJv2h+5/gUn62Mpa5AKrm9s0/BqCeP59praoqRUpKgoDGzs4SWq0KHh56hIcHIzExBunpJ3DuXDrS0lKFpTw9XQRU7M8+3N7QULvtF/z65csDj1uaUXS1ANnnMhAVcRRn0k5hdnpy5bca790VIGnVDtBpHGUhaAgsgsrLw00AZm9rrbCTrXzmfoItJipi2wDVeL9WxHVggJeYtNevXx5Y3kdRnXIsFmStl0tzU9xGM/fRta7qz/b9q/lWgsjXh7rKAXW1pXsfUDRner0G5ubfCePQlFVXl+HBg7vrnvzY2FPU1lYiPz8XsbERAiiCLzDQVwDX39+1LRc9PTP2Nu3UCQT6G+Cs08BJq4attSVSkhMxPzP9ln2Ki64KMCzMD8PHy0P6ECxkJDIQv7u76qF2sBeQ6Z208HBzgbenuyyHDx5AUkKcoquGtnTO9NiodyiiHzTWrYxFZnrc2ogTqQkwN/sGVpYHxWPb1PMpugCzw39W9JcKj1vv7S1A0SxwPTExIpqIDGQweAq7lJaWYGTky72IyclRFBRcEHNHMBJUBOJ2nGdZebGwDYGgsrMRIBEYTY0fbuiicaGUrEP2iY4Ml4X7CS6uuY/sxOP9DT6IDA+Tzy7OOgQH+uPSxTwcS0oQtkpOjF8BqCkt/2KWAIWmbfX2ew23xWOjqGY4gCaQWopm7Ivlx/MJpKcl4/ChrxVQxuFXJtsj4YPh4X5hEeogshJF9dzcpMkg6O5uR0JCtACKwGxtbdoyoOLiw4VFAvx8BQxkJ7LUYH+vjH2/4Y7ooaOhwaiprkRebjYKLl0UMIWFBKG6sly0Efvcra/Fi+fzsq2jrRXlpTfFbF4rvCJMRpDdu1tv0jmPDPeK4GYcabWpYmCSoQA7W3PxzhgC4GeC6+aNq5iZHt3w9968+aC55mZHcTwlRgGkBg+b6vYOO1Ej5eVlwdbWQrTPmTMnYTTOtG513KGhPpw/f06EeURECDo6WrZ00Z5ezsI6GWfS0NnRJswxPzu9Mubpk6lizirKbmF6clwYhkxDMJ3LOIO+ni6cz8mS7WSz8bER5F84j1dLL9Dc1AhHlZ2AlPtpDivLTdMkZCMCZWH+g2dLrcM1BTRBRNPHtcHXbYWlGGOqqryxqd/rfPJQGcsSMdHBWFycztwTYEpMjFN0jjOcnbUKI53fdpRHRQfBx1eP3PPpMva7d+9MomN/Pz9hpmej66cauI+iOvNsOggyAoXsRWFdeOUyJsfHUFFajqT4RPR29eDZyBhiIqOU7wkov1WK2OhIAR+PoemrLN+8x2Q0TpdaWx1CTnb6mmPr6yrgrNVD46CFo70ars4u8HJXdJ1GK2xbUbYxmFaHEiZGBxAW5AtHO3O0t+wB7TQw0KsIZT8cOnQAwcEBWP8GLZRu5TcuXMyAp5cOgUGeaLh71+SLJphc9U7relvUS9RTZC4CKjf7HGpvV2FkaBCJ8bHi8ZGtRp4Oo7W5RQFPGV6+WMKN4hLERimen6MaOVmZeLloFE+QY9y6cW3T53r3TpVEr+/UV2ItkzxSfkcHndoJep2z8lkDbw9PuDg5I/30SSy9mH+7GTCxXb2UC1cne9RW3dh9MKWnnxJW0unUuHfvzkcntLRk1GdlZYDM9dt9m2njEwM4nhoLVzdHRISHY3LCtMAmwUTva/jpWofg+fzsFNnEysJc9NGpE8dF99ypqxGzV3KtUAFHCToet+P7pVc4fUIB18QkHjU9RMvDZoSHhonOAv4HJ44fU8Y5jKqKW5s+z8SESDFdgwOda459OtgFla0D7KztBUgqW3sUFlxBbfVtMcGm3JP+7jYYFPN/MffM7oJpZmbqrYeHqwDp7Nm0NSeTn58npo+sZTB4b+lkq2/fgq/BDc5OTsqNHtgSmMg26+2niaK7f7XgkohqemsEE0F0Of8CUlOShZGM8ws4n52jmLpu3Cy5LusbJdfEFBKoNDkGHw/lAT/Z9HnSQ4uKDMbszNiaYxn5VqscYW1hJWtbKxuMDplWHfC3N0sSrTfOTQiYEmPDdhdMXV0dCAkJRHh4KIaGPn7AY2PDwlhcPD3d4Ohoj56eTtO9u57HCAj0EjA96egwaRw/g0HM3KdiQAxWUjwzXsSgJs0VzRvFNBeJLak1iI+Jhaebu2gldxdX2c6YFMdm+IDpFlNM3DKYcnPWZ4nFFzMIMPgLiD6YObWi20a3BIKl59PwctPA19MJ4yO9uwOo+fnpn8g8BFNOzjmsde2fQK1WQaWyFVZycLBbt98XX/TSnD46JhQOKhXKSktNBhNF9qeYidFxBjXplS2nUQgQBisZZ6IZtDQ/IhrF2sJSeajWysP1Ew+QjEbA2dtaKtrprEl5OsZ53Fw1kuH/VJ/szCxYmJkrAlwFPx/fFWZ68+OrTadZqJ9eGmfg6aqGXmuLrvam3QFTb28X3N0V78feBhUVax8umcrPz0f20wySoW7cKN7SySYlR8PG2hrpaWkmjWPw9RU3f6Cv55PHPx3oQ1CAnwQ1CSDGlSjM+Z3ActO7wOCtuP929rA8Yibg5MI+TloHXC++iqkJ09mC6RNWBHxqf193j3hw9ja2worLAVdT2w9L81N+3nr4+7hgcqx/d8A0PT0BLy930UQTE2vt+6NHTQI2Nzc9tFpH+Pv7YnLy2ZZOlrEmjVqN5KSkLYGpu/PzZpK5O0awab4YoGTsiMzDiDnZimuyEDUV0yfc5+3piowzp7b8MBiUvJCX+dlx8nJyBcDf/NvXkrrZNAO+Mpau/hyqyIdd1UzURATK0aMhULy2yLVmyRgZExMp5o3M9KmwwWba8eOJ0Go0iIuNNdnM0fX/kkQsBThBwv5cM9bE+BFNIMFEpiIbnUxNQdmtEjTeq9+Wh0EwhYX6rQQs12stzU0r50V2ZJnMev2Y05sY//Vaf/55/XKVAF9X5GWn7R6YGhrqodE4CON8qk99fQ18fb1gZ2ctLFVS8qGO5lO1QxsGL6PCoNNqkZiQsCUB/iVgupiXK2ECCvLlagGyEhemW65czhdPb2bq2bY+BBa+MTzAYrfV29/8n/e13CjyHVU2CtPqpaZqNaC+f2VsPXsmVYrhGDVfHVv622/GmRwbFL10KS9j98BEneTkpNnQ5a+pqYK3t4cI8aAgfxBg68aSxn/VGe/f/7KueE1KihcBfiI11aQLD/D3FxH9KQG+4lzMTL8NDwtZSeqygoCgYr0S0yZMo+zUfWVZCUU4qyU/B6b62irFBFso7GivMKYjggMN4kG2tT6UteWRQwqbfge9kxrF1y6BnuDysX9fle970FADS7N/x5X8rN0DU0fHYzDGRObZqG9GRjqsrS2g1+vEwysv/ziYV1d3G8eOJaK6ugKNjQ2Ym5ted8zo6AioHR1RVFho0oUHBgSIqRoa/LzQZPCRHpz54YOyJpsxicuCuZ2+r0yZfHfgXz8rwlcCxqdTBTQEjI2VuWJ+j8hnVoFyIdjUDrYKq+rQ0fZoDZgYFsjJPA07q4Ooq97FYjmGBshM1E19fd2fPZHm5gdISIgVIDHeRP1UWFgAxqkIHmoren0U8/y8XgB0eHhQgEsB3trSgvfv/5FpCpioc5arBNZrTAAzOEnRTV3EtAq9v9XFczvZOF2JSdtjyTEb/h6L4WJjwhQh/icBEc0eAXT44DcSNCWg3F2dZF145eNSllcvZhU2yoGHiwbx0SGYnx7ZPTAxVcKHT7a5c2fjikiFbd6mph4T7URRTlajiaT543cGNjkWxTq9xN8eX1lZJvvs7ezQ29Njspmj5vkcmNoft0jEm6aQwFrefvN6sSK0f5/8FUt1OXGgrrZ8w9970tEsEXPGt1iT7qrXKn8CKzF/y7qK7ESx/ujhg5XxutqbFa1kD43KCvfvVO5+Xo45N4pw5t+MxvkNS06oi+LjYyQxHBBgENBQnHMM6iqCk2EE9vko9vO0X4KjBF5sTIzi6cz/ZKoA501lLGkzx/Eh0DxSMy0aZ0t3/o86GxkS7CtlJUzubig52h9KgRxNnrWlmcS7mBtkuIIhBHp8NNeswVpmJZo3lY05MtNT8frl8wO7DiYGIS0szIRVqHc24wkSMDRbZCiCiKzDdVRUOJqa7n80VlHRFdjYWAoAaeJMPV+CiaBYL9H7uUYmY5qEsaXV2mMn2/17NVL/HRkRhN/Wcv+2cRoUGSosJEBMHMFkfviAmL6szLMCKNZhLXtz1eXXoba3hLe7E4b6O/dGYRzdYh8vN/EaThxPwqulL5858VpxV43PZ6emJ8ckRsNYTUtz45oLG+jrEm9Fp1EpItj0zDb/fdQRLNno6ezCl9ZE/fc/fil90tYueTBGvn+ve7toXMikwKa5On4sQfkDfBmbstyXpbwEItMyQ0+7V8DIuvKmB7VSXRkU6In2tsa9UxQn0djcc/KwA/19sDA39Xa7x7+Yl40D33wt4/d0tZsOJgW8HMPO2gYlRdewMDP3dtFoLJ18No7Bvn4QMK3Nj3D/bgNqqqpluX6tWIre0k6ekvRJVHjE7/ovHh8bQkpyPMwOfYujoYEbsuJyRSZnrnR1tmD5u8STJoZAoOm0NjD4uuD+veqVsebnxvYGO1VXlkrgjIDarkmITLhy1iw9EHojdG05yXGr49IMqFUOkvEnQ3Ht4+kl+TYmTrmwapGVAUzgLn/W65wksRsaFPy733Qyc2pKoghr3gtObaJJ+5JjOQWK8SpOwjyeEicaLDjIS2GsS3tzihNND0UfPQaare0Ys6nxLk6fTBERyXHrarbH22CkmElSAoQMRWARKCpbO9kWeTRcvpsdPCRAIri4j1UCTKzy2JnZ3fkX8x74G7xw6OCf5d0BzN/dulkodd+csdLy6B6onTjXjmtWbLIPAXTg2z8pXvARnDqZhNGRnr07Vy4kyE+CZUkJv3pgLxcXMhne36zZmxwfAan9TNoJRTQ6i7vLz9t1rtRMBAqB4eqsh4ermwCGoMrPu4C//+WvKC4skmw8wcSF4CKDEVTsPzLat2sPg+854PsGmLsjOI6Yfyv6yElnD0cHK7B2nGu+1IKTDLiwapMintPKp6dG9i6QSMGkX3oPudkZGB0ekJc6nMtIE0o+lhSH9scbUzLHYelGfGykzOdnnIQgLb1ZDN7A7TpfphwopAkkAsZZq4ONpZWAhtrov/7jP1FRWib7CTiNg6P04TEEIcE0/3yydbfvO8MFNHfJSdGIjwuXHBynj7OwbtkDjIkOlf2MWVEvfbXX2+X88+JtMOpKCk5OjJXvBBcXemB0T2leKNTv1t8GNRbzSjXV5ZJD4na++IHmjGaNaYHY6HDpt93nS5Azqr280NVnqIB5N1ZKcgoUZ5gwF8cJAcsFb4yac+Gslb10/1lMx/JeviqHXhsnYq5+qQXjVV/9ERpdVbIIxTGBQwFO08TvifHRoFsbGR4iYNI42q2Ai/0YTmCInwDidkZu2Y/MdLuqbMceGHUH82ysA2cC90NBm1oCeqyWZGUAwUUwLffjfpbxMsXCqU5f7bftb1cLLoopY8iebEJzl3n2tJirocFf0xV36qrlNTQEEkP7y0lJAogRWh7v6+2OC+ezxEzu9HmzMpHpEiZuyTScBFBXUw3O5mWEmNFuJnpZD870CWemfGqe3X7bpnY2/eQKIMLDglB09ZJy09evE2IArruzTcwbX5DFvvkXctBwp0Y01sL81E/7d/T/cau9XSFswrlhFNmvluYj9+/KfttvO9j4hpj373/ZkeQwBfbqstz99gcGyfKrhDbTPleyzBwlA7Zc0/xvNFbB5VypFmhva9pVQP0vswpeXb9KZp4AAAAASUVORK5CYII=" />
	<?php if(!empty($receipt_details->footer_text)): ?>
		<div class="row">
			<div class="col-xs-12">
				<br>
				<?php echo $receipt_details->footer_text; ?>

			</div>
		</div>
	<?php endif; ?>
	<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWIAAAAxCAYAAADgMaOXAAA3l0lEQVR42u19Z1RVW7Zm/6j+0aNHjx6vwqsbvAZyDudwyFFAERRQFMSIOYdrxIhiwixmDIiIKEEygkhQMOd0k7dyVVe9V69G9es7Rv/+en7zuBH1mq7eW9VVe40xx95n7805h7P2+tY3vznX3P/lv5jNbGYzm9nMZjazmc1sZjOb2cxmNrOZzWxmM9vfX+toacxqqj+DlrM1na3n6vObG6pA4/73/dlXWmsTzB4wm9n+Rg0dHT/6619+m/3vf/iVb4fsm7/ID99Kig5i1tRJGDtyBIanDEb60GS1lMSBGDwwDqlJCRiTMVzPj8tMx/hRGZgwJhNTssbq382bOQ0L505VWzx/OlYsmYvcVYuRt24Ftm3Kwfa8Ndi1dR12b9+Aw/t34PiRvSgsyNdta3N12UsnBrkfTIA2m9nM9g/fjh3eh1HpqYgKDYLNzxt+nm66DfT3gcXbA/5e7gjw9eo+ZliQxRehNov+XWxkGCKCLWqRIVb0jwjCwP7hSIyPUhsUF6nHosNsup+WnCAAH4OkAdGYO2MSjhzIh8mUzWa2v4P2lz/9Lov25z/+OoHs2PxFvv+2f/c2TBw3EoEWL4SHBiPIZkVggAVhIUGIDA/VY8Y+t8brkCCbXhscGIDQ4EBEhIUgMtiGiKAANe5HhQTqNkzAOsTqh/BAK2wEdDG+5tbX3QWhAf4YIqy76MB2mD1iNrP9HQP0f/7HH78xf4n32+j2jxyerIyW7JagarP6w+rvq9sAix/8fb3h6+0JPx+vbuDlluctfj5qxvVkzAZrDhFwDScoCxBzS+YcEx4irDhUtzwWJsBM42eTeQ+JC8Wpwr3dYNx1rirblKrMZrbv0LpamxOa62vQWHMGZ06fRNHhgzi0bzcO798j7ude7NyyCSuXLsKc6VMwbeJ4TJ0wrtuWLpyDhfNm4NM507D401lYl7MMBft2oqayFAwiGeDxl3/7bbb5S797qzxdrEBJICY4Kpi+wgjMZMuG8bVxjABN+cLq46lGQA4W1ktA5tYA20hhydFhwXre18NVt8Y5P9dPMGlU6jNgbDazme017XxDGcpPHMSJI/koPrxLAXbujKmYNG60BnMY7BnYP0oHHgd6xrAUDfxw4HOAkiXxXFxUOAbERCKBOmJkMEIDxW3190RwgA+iwwMxOKE/0lIG4cjB3ehobVC9kPKF2QPv1jhREiTZBwRIfx9fWP0EcP0tajaLMGBbIMKChcGGhHaf43Ea93mM+0EBBPQQAfYg6dcAeV9LtwX6C2D7+ug2LJAMOVDAV5i3Dxm0r9wLVv2bcKu72sqFM0wgNpvZXst62+t9K0oKsHb5PIxKS0BsuLihNg8B02gdUBxwHGwcmBxkNO7HRkboQOUg5DUcpDzHfYu3F0JsvmoE4rAgeU8Gf0Ks3cbjK5Z+CtNdfT/tXEPtNwZzJSslmBJ4uSXAGsBsAC/PhQQGqfEaHusJzuxzoz8JsDTuG695zsfdDV6uLnoPRIYEd98vfB0R4KFAvHHVwmfkCbOnzGa259rO9euwYNpUpA2KR5i/N6IEOCOFuYZbPNE/1KoWGxaA+IhADIoJxeC4CAyJj1RLTYjB8MHxyEhJwOi0wZielYklc6dh+YJZWDxnKnJXZ2P18kXIXjQXs6dPRNLAGAyMjVA27OPhhAH9wxEVZgN1TX6X//0ff+g0e+S7t1MnjqpWSxAkiEYEhcDT2RVRIWEIswXD29UDAT7+sHj5ItjCQFw4Av0EgH0D5Hyovg7yD4TNRwDYEgpvd0dEhgboBGrIDmTAZMPeru76Xh5Orvo3/cNjEOQXjEDfIIQFhMPiaZXJ2FVtStZIkxF3NJ3Ofn6/tbGi83x95ePmugq98VeJCzouc6SynAAfRj/FzfCWARkchrjIGLUwW1D3jBjk7yPXeSJGXKCowEAZuEEygEPEwmQAByPMJxgRfqEI9ZcO97VbiLCl0AD77BxolVnTZlNjtDZjxNCnM2Zrbfd+c/0JM9L+ntvF5rqy5qpyNJaXYU/eJqTExcLP2Qnejn1g83RFrDDVwQKQ40ckY9XiudiSuwKHdm9FyZF9KC08gBOH96L40B7dnq06hbbGKnS21ONy21ncutSGz25fwRd3r+HhzUv48tEdXLvUjnu3ruAXXz7A5w9u4ULrWezathFjM9MUhGOjQnSg87XZO+/WykuLVJIwvJPo0HCEBwbL2LPJmBUGbBGQ9g+Q8W1R8/Pw7gbkEGuwjHke81XzdfdRLybI6g2LjxsC/DxUK3bp1xsecr/0D4/U8Wz19tP3IhhbvIRNe9sUxAnKAb7uapPHZ5h929FQ2q29tcp+R0fOj9qbKr9pOFOasHXjamQKG2G6CWdRznT+nt7acTFhETJbilviZ1Fg5jmaXYz3ks72RGJcjABxAEL8/ASEBXytQQhw84a/sxeCPC0Is4hLa/UXQBbXlSDuZ+l2f7y9vOAnAO3q7AgPNyekDxuM5Ytna4edbywra2k8lWUC8ftv7Q1V37Q31KBgx3bEUwOUfo8SFzPYxwPJcdHIWTwPx/btxLnq07h9uR03Os/jakezbu9f71SgfXTrMh7c6MLNrlbcudKhRhDmlue+un8Djx/cxP3bV/H1F/dx61onyk4eQ3XFSdy+3oW7Ny+js70ZyYlxiIsORergAYiJCMJ5mSTMHvrujcE6e+7wExlBxltcZLSyV45rgjIJVWxElDJlDycXBVGyZVp0aAQGRMcKgw4XIJYxbPUUAHZDiM0bw5ITMGl8pi4IIQZQenJ3dBbQ9lJA9/f0ERwJUQvwsaoRJ2wC4CYQPw/KAsK0lvoyXd44Oj1ZZyznPr30h1Wtz8+inUV3hjMqZ1F/L0+NgtrTWAjGbvLaGf7uDrB6uiDI10NYrzdsHp6wunoiTFydgWHRyogjbVYFZKu3l3aaoVFZBJwDrFZER4bDKi4xXdXoMCs2rFqMtrOVjy+2VKO1ttQM4Lzn1lJT0dlcXdG5cPo0BEm/+jo5IlQm2OGJA7Bv6wY8ENB8cO0irrU3KRATXO9evaAgTOPr6xdblAETbA378t51fH7nqgI12TCvvXn1Ir54eBvb8nKROCAa8TFhmDNjEs431eIv//Y7bMxdCavcf0MGxWoA79D+Xf9QA5aByOaGSrzptft3b8GGtcuwffNaHD+6D/zb//t//v2Zvz+wNw/5O9Z863tWnDquQEzCxMAZtdvosFC4OTroMY7v5EEJQngWdeu/HN8WLx94ubg9kRv8lIjR/LwcMX3yaBwt2IWO8/V4dO+abBtV12dAMCG2P5IGxCvb5t+GimdMIzBTtgi1+SphmzQu3QTibibUg10SjPO3rEuICrUIyLpIR7irK8POI1hy9mTHcFZlBxKEfT2d4Stg6evhIK+d5LgLBsWEICNlIDKHJoo7G4mYQBuibQEysP3E3XWBj0s/WD3ErfF0lxnWTYV9Xy9vBWOrxQJHBwdNKA8OtCgQW31cMCHTLlOQDfN7mj33fltTlV2eSh3QXxmxy8cfITE6HLvz1uNK61ncudSOjsYq3BSw7TrfoKyX7JdgTACmEWx/+9UDBV4yYBrliK8f3lJQ5j6P/fKrh+jqOIehQwYq46UmTBd3++Z1+Oz+TTy4c01ZMfuebjDT3d7mf/nPv/yh869/frfUt56BQrL2xtqK9wYah/bvwN5deXjd5x87vAfTJo3GWLn3Z0wZi0Xzp8vvZdN9I6uE7UTRXgxJjETK4Ei8Spqwe672FDJqxtR3mdbG7expk3Hr6iWsXrZEALov3J36aZoa/45ZL1yFx0BfQmw0NuQuwcX2Ovziq9u4frkVly82Kxjfv31Z+7X0eCHy1q1RcKdc4evuqdjBrbLvQD8F4oljR3zr973cWvnKehdXzr8Y2LvRUZtwvb3e9330998ciNk2rl4Cb7e+CsRcKcNop7Jimd0ig0N1RmRHsnMY+fbzdFQWnDQgArOnj0HuivkoPLANp4r2obhgJ/I3rcGWNcuxYcVSTB83CrHBQYgOtiLE3wuB3p7CtL3t8seTVBqyYR9vYcJeHnBzcdD0ppAAuSECfbBjY47pon5Prf7MCd+ta1cgxFe8F3dn1YXXLFkAyhXtDVU4W3lSgfhLAdl7wozJiik7kOEajJevCdKVJUdxvGA3DuzMU6Nu3FxTptf+/utHqgtfvnhegZipa9SEaYcP5CtT/vXXnykrZt9Th5wyYfRbgeDZunJUni5CS+OZx9/19/jrn//Qfa8tmj8Tu3e8HDi/yyCuqyp97f80SVz3MSNTcabsOIzPmT97sngLruJBTOj++3GjU4TEOCG+fwD279nwwvtyIiHwGuyXiyuYQUFwJRBT32VO9zd//TftF3ong+KjhPm66ATJcc5+oJfCfjpRtBs3rp7DLx/fwsN7l3D7hnhINy7i7s1OPLwr98L965qCyLRGe86xt3rPJFzUqe3eswcmjBn+0t/getdTUG06cwx7Nq/CjvXZOH5gO04UbEfZsT04c+IAGsoL//Gya8g2161YqAyUQEyQZOd5ujjDx81DA3Wc1RiQC6ZY7+2qQJw8KBrr1yxC5cmDaKo5gdrywzh75jgaKo6hruwYztWUoq2uApXHD2OPuKNL503H8MEDEehDjcpNP4PA6+XuoSAcGRGhK3ycHPp2a8/u/T5SeaKp+lSWCZvfT1soA9Di5gR/V0cF4vXLl8iNXoK608W42nYWt4UFN1WVKNhSgiALJiBz/9Sxg5oVQS+IxswJZlREB/tjQGQQxgwfgrXLFqDoYL5qwb/95Rc4KfcDMyOoBTOTggE7smUG8rZsXKPBOgaFCApnyk68MGhZwGberElCALKUOU4UV3fJgpkYPzpN9/PWr/zOLLZn/vLojKHyXqvxtiDMiYAyQmNt2TMMVsfaG0gTozNSMHXiqBeuW7F0Hrbl2WWIuqqTCA/xxqiMJCQnRQgzDse3SRMEX44zTSUUYOUk5+XmoPuujp9gxNAk1ez5+//p91+jtPgQxo0aJhOll3gmDvB27ydg7IJQIUTXr5zF/TsCvjfbBIRbBYw78eBul7zuEGZ8FTeutGPNykUqOWmKYqBV5UvWsvBxd9HxbBVgzxr9YiD2+d+TTLdofx5mTRiB9MHRGDc8EQmRFgyJDcKgKCtSB4Ri5YKpaK4q/seROagRE+yCrZ7Cih0UJKkXGRox2TC3dCtoATIzx0cHIWfFPJxvOIXO85XChPPRWFmI+vIjAsKHUXOqALWnDosdRb2AckN5Ec7XV2LlojmIDQtU+YOzJpdX+nh6qTTh6eGhQMxjXBDA2dOl98+Rt2YZGEx8lc7dU7boeey7yhn8u+flED0mDFKttjSB1lJ3Kqunna87nU1rld+UZrzmOWrcNL6vYT2/Z9uTa172nTuba3HhLLXyik5+h+fPtzWezn5VQNPIkjG2xntMGT1C2LAjIgP8lBnv37YR969eRGNFiTLiy611aGsoV1mCkgSzIg7u2owJmcMQYfMRL8cDceE2BPu5iwfjq0DMFDebN+WoPgizeiFlYDRY96DtXL0yY7I12uPP7wmruqSBvN/96ktdcUc2xuwJgsXBvTteGGi1Z05i1bJP5f5bqAxxeGqCAjJ1VAJJU/3bywkEgudLQWakDdGJwQDnPTs3Uaf9pqayRLXb6ZPHKHNdK2PH+JuV2fORNDAKKUmxSE7sj/RhidiXv1nP79y6DssWv15uWfzpDAXBLGGOBLaCfS/WaNi5NRdJCeE4VXIQC+ZNQEJ8EL4tWEcwJDNV71N+V05wTEPjZEe2y8Ab5ZLPH9xQRvvZ/Wu4ebUdtWdKsH3zGsyfPQmTxqcja+xQXGg7g0f3L+Krz6/olkB891Y7bl5rVTC+2tWi/UHgNjIr9DMtvjrWiR0kcS8DYkoTPRkx24XGsseVxftxZHcewi0uWCDe98EduSjYuQ7H9m1GW/2pdwLii2crOiuL9+Jw/rq/D0DfvSU3iwBL8LM9cSt0tQwBWIyiPWczmpdrH2E5n4JJ/2eri9FcW4yq0v2oLStQq6OdPvTEjnRbrbhatWXFGJYYq+/DmZLA6+zYD/5+fmp+3naWrEsiPVwRavHGwlmT0dFU0/1D7d60KisnJ+dHeTlPE8P5mtv8LSt10BDUjG1XS9UzP/KVtqcpcRfOVryQq2ocWzp3Iq511HdfSxfd7jKdyjJArKWmLP9Si/2arnN1uuXrc0+014tPvjcBdGvuEuQum9OteRvA2RN4DSBdL5NcZmoc0hIjMTIlFuPTE7FMPIqK44dgfA+2iZkpiAn2we7tq3wvNlU+vnS+uvtcZ0vlYztAn8oyPuPbtlPHpMOz38dqE0cOUyYs/yNaxZtprj4JmUjQ3liBS62NCsQrFs5Wxhse4K1gS/DlPgGYFhNi0a3V0wmufT5QIOY1XLSxfMl8Zb4EY2rCTGmjPszsiYd3r4O6puEWEyyKCw++WlNtqctKHRynAa1vO194aDe2bsrB3JkTBTCXah1e4xy/yxj5fwm4zGtmPjMZek9GTKmE+zu2rMfkrFEC9gcEINOVSZYUHcKMKeOVZW5Yu0Kv43HmRvP6oiP7haWP0Nd7dm5WNjvxDQJVbefqstevyRaPQJjuoBj0jwxUwF++ZG73344fPQxTJqYLIF5GU0Mp4mKs2LQ++5n3bqgpV0Zq5PpSdzdkH+rw3CfY8zfq6mgSVnxPJsRLCsafP7xqZ7ry/pQh2mQMffn5JTx6cEFeX8BnDzoVjAnEZMf3bl/EvVtd4ukkwVe8ZTJoQ9Yg+NuXQnurPDEyLenZ8djx+uprBGqy4GXzJuGZybPOLvVcbK58K/myZ8W35fMnIzkuULy/6qyOxheB/UbHD1gdjkAcZPGwp5g8AWIFYwKwt69GUv3cHNUsHo7YvmEVasuPqSxRXXrwCRAfEBA+YAfiHoDccIp2BNWnjuGcuGuTRg/X2VGTwq3+8PJwg6+Pj8oTDN6REWtQUICaQDx/+iT5Ozs4blq9AKOGxiswTRs3DJNHpyB1YBiGCjvgduHMcd1Ad/1CAwxQJSC3N1Y+ox2SqT7PLFueAHjusnlIiLJhV94qBdRL52sVAAlcBGKCrgGIPMbXBF+ea6uv/MYAYm6N/V15K7LXLJ31QkcbTNl4zWsIvivE9Tq8eyPWZs/GVGEkAfK7zxg/EpXFh9HVUoMlcyYhPtwCX5deGDs8Ac9PLATgN+n7ScJs3ft8qEA8QcCnvuyEeDAnlBHXiycjzB6d585IH5RrLvHQQf01Q8bTsZcu8CAzZn6xoQsbOcZ7tq7HotlT9PpeP/0fCq5cxMGsCWZQMJWNcgVdY+4TlAkeXDRA0OaWixJe9/3JOndtW//Cdcw6IEjzPHVXAiKBsbT4iF5LEKZezXoXZ+sqMWFshmZrcDIwGDHBmDoxAbq9pUGLqHNBCgFaQF1lFoIxr+PfDE9NBLVlTipk+3xffg5rbHBCyHgOhF4HyJQ5yPJHDh8sQOqrYFxaXIDocPl9wnyxUib25UtnqDyROiTmmfdmoNG+qu5JKqqMJ0p+jPNQOqBOz+974tgBfPPXPyoIP7hzGY+/uIkvP7uuTPf6lWZlvl99fg3377bhi8+68MWjSypR3L1llyce3e+S/QsKxGNGpsDf21mZNkFY841lS2JFbKFlSH+8TBd+qXbcXu87e+JILJ414YXf72LzmbId65dj4YzxWL1oJrLnTlYPHE/kDnrl1ScP4+LZyk565+drT/VY2VeZnyvjKzkuGO0NpW+kXX+vjVkToTa7LhughT3sQEwQZuST2RNMO7N4ME3NCXvlxm9tLEdjZZFKEs01Rag9vb+HHUT9qSdWWqB2+vh+BeKFsyYhPMgeCAgJstoDdK6u8PK0yxRkxRT3rdJ5IX4+mDMlC41VZfnPu91kmPHhfkh/7gb8VhekufIbA1C72ee3uPfXOhphB8M5yjS1s1qesnEDVF+Qd2rKXhnxba23TwJb1i4WdllWRnv+GoO5j0kbKJ/the3CcFpqT8pEd0h+zyOYNWEU5k4eK27UFpw8shsDIqwyGQ0HWXGwryP43gagvykIK6seORSun/wcXg69FIipDxOIa08XokZu4raG07jQVKHBODJfShCcjClLUP/lgg5mUlA7ZgCPwTkyZwbx2sUT2JW3FlFBfjogXeQzqA0T7L767K4y4euXO5QhP7p3Q4HOkCVYutEAzVc1ssZ1OUufua6++pSC18yp41BVXowvHt7UCYC1LWZNsw/mlKR4rdPLSeA3v/hcP5v1MNasXNINqgQsbpctntc9cVA+IbiSQROsCfDcavaJ/G8E6j/9/pf4X7/9Wv9m2qSxWDB3ukoYzH54XTCPMsvzWvLJ4wUY0D9UJih/lQpCbF4YOWKQSgZpqf0RFe4DL/dPwHS2p3p01TdkohzHDLwTjBmEp1zBYJ1WZBMwpmfwiy/v4fe//kImw5sKwGS4v/nlfQ3M3bvdIcda8OBuh7JgsmHu8zgZMXViMuf2llr9zSlNkAUb9US48IOky5A2R6Qm4G2DnmSwsyZkYGbWsx7FZfGI1i+fj5T4MIwZlqBAnDYoClnpg4UEFoLnt65dqsdGJsdh0cwsfY99W596UHlC7qg7G+y6+KCMr8N/o5KddO3DAu1RTTJRo+MMINYEcGGvPs6OcPjoZ8hZOl9mGWrChaoJV5zYI4P2gNg+tbrSA09NWDOt4Uwxqk8fxYwJmXDp9yF6ffBjzZAgI+ZiDgOIyYrti0k8NMNi5oTxAgzfrhHHCSsYnhSF54OPGoQSdkzGTAZNwF48e6Je19ZQ0bk1N/ub4UkxGCYdlJk6QBh1pFaGMgB6nXRumMWt+33nTR2jQKga3qzJGBIbjoTIIOng/pg+7mmC+rjhQxArrvmwhBgkRocgRgBo3pRxOF9TVrZo1niVG17XF0vmTNDvy+89f9poZcR7t+agSoDwzIkjuNLaiF2bVskk5YJt65ahcN9W+a4u6iXY5ZDKx8/rzK/SygnE7n0+gLfjJxg3PFlBuLnqlLJhmjEZrFuxSDyifgjydVMNmJIRF3cQdJkrzH3mFV8U7+BCc60u+mDq2rUL57Bt/SoFYnfnPgpumzfkKAhzdR1X2hkLO8gyGewhk2KK5IljBa9N96ILT2225/EjB3cJo7aovvqrxw8UaBiQIlASQHkNJYbpk8fhz3/8jcojBE4yZB6nXsxFRQRmgjX/rqL0BK5fuoiJY0fp0yu2523AptwcTe2aP2u6vuewIYmYMXkCvnp0H48/e4CqslIMHTwIi+fPUdZO/fiV8Zqz1Z0EawYinz/HAF6geK2hgd4Cnk+15tbmis5CAY5gmzsy0xOfAWIyYoIv09e4HoD1H54W6LHCw9lBUwnJ8BtqTisj/uqzWwqsXzy6pmDLTInPHlwSpnxVXrcpE/7i0RU9RtC+dV2Y8sMb4jlskEkpTLMuAi1e3ezbpsF/+2IOrlNISx7w1iBHQE0f3B8E42cmLgFbHifwnjycjxsXzmJ33mpEB3op4F5rrf05gTjAoy9mZWWipGA3Jmak6vlDuzbqe/F8hpA56s2FMpFNykzC+uWzZQwU/W3AmIyYYrqPm7OmnbDzDFmCEkV8eDgGRUcjOsiGLWtWKig0VBxXMC49suMJEB94FoR7ADGlDFbz2pK7HOMyhyrTiI+N0pqoDNRp+poAsbeHPf/Q29VF846njx+LuvIXGR4Bl5JERnJ/GC7+U+AchSAfB4weNgDrls9FzpKZGCEdlpFsB8IVC6YjwLMfCLA7NqzQQiQsSsKaqXa2nQ23Pj/T8zOkQ2kHxUXdsGKxgHAElsyeipJDe7Fq4RwEC7OfPMqeksO6DASz/E1rUbBzM5iRwGXCS+dMw4SRgxEV6IFXAeTVdrvOTNAmEPP7Z2UkyW/uiXgBP+rEbfWVWCBuWKC3o0xSGQLSa3Uysnn1BWWNy601z3xGa8OrF8JMGTNM2DA14o/kpo7XCZYr6c6eOaFZMHVlR3UioAwR6OOqbJgZEsyYMNLXWhvOdC9rJiCTHROICdJky3UC7pQayHTJlnLkd6QkQVZMJkzdmAE7LhDgYg9jKe2bSBOUH3oyYuYSMwDFVWCHD+zEH3/3GL/++qF+DleDEVgNaYL7TJ0jIy6U/uSKMUoNPE+pgjID5Qey59HpaQq+fFoFy3w21VVr6U+miLEinYL76JFabW7z+rU4sHun/k3yoAHYvX2LTD6rlDG+7v9hQI/fnTIGg1+Ts0aqVpw4IFK1V6e+P8eh/S8ytlnTx8Bf7vmSogOwewVlCoAsxsTgO1fNsd4ExzPHdf+I8O78Yk5OXGpOrZis+Na1Dk1Pu3+H7PeigPB1Bd7PH17WLY8RrKklM33t0oVmTBg7QgGY/UZtmp/NbA0jfc6my6LdZGKK+05AnCxj05Amus7VqGdaWXwQg/sHK8N9cK1d7bJ4vZOFVA0TctVWX4aNKxfImA1B5fFDeCzEoKzwAAYJiSJ483035yyW9wjEzg3LMHtiOlZ8OkWwbZ8QErs39jZ5yiQG75xWR0bsJyzEU2ZJP2GjPYGYy5qTYuIwY9x4zJ8yVb7oMRmsZaojMoWEX7y6dL9a7ckeViJAfMJuZcX7xA4oK965Za3qbmNHZ2jN0359+9p14idAzHQ5T2cn+Lm6YurY0ejIeQpYDHIZ+xEBbgKw0c+suqMWmxhjU531qMxwdOtpm1YtQYTVC7s25mQvmT0FNk8nmVCWofZUkQbBcrM/hdXdQTpuMXZvyskiMCXHRaB/sD+y507TANaJgj0Ynhgns2aa3BRTdJ8u/aoniw+ihe3NEVeU110QdnfqqHS6uNoE5+XSwYOiA16YNJ7/vzhxvOCxyESSEi/sLHWQuF9zMDAyQKUJ6tijhg4EPQM/149VK38e4F8nU3CS8Xftq6yYn1FeVKDpamTDBOKqkwVYv2IO+n7wLypNMBOC2i+ZLlPayH6vtDd1AzGzKyhP8DwDfGTJvIbpaNR+yXTXr1mubJhaKgGYxuAdsyrIQg1pgpkVr7zxWxsSyIiZRdHz+NGCfHXjp4j3xfzix5/f0YJD42TCNBaKZI5I0YAgg3hbZeLk92NeraERU/elDMF9Pm6IixyYI0utleA6asQwLQNK0GHtZf0thQ3zNQF56YJ5ut2zY+tbZU1ojGL1Eg2kMYOCf8OUNk4uDKwtmj/tW9+jUVzr9LSBKC7cB3t2ySmVBQwWzJoREUFcExCoK93IkI1SlmT1nHCudp3XDArqxQTZX319V5mxoRf/5pd38etf3BOWfF7li9/96jPNlmBGCUucsm8pedgfw+SlzFtJna8diEn0UpNi3zoHmNLE4JhQLJph92qvPqlfXSOkISbQF0vEO7sj9911uQ/vy/22VO7POPFMy4UscH+AkJg6Gec35Z781cNb2CrjPiEiULXj7eJVBgmpiad3nRipucq//+o2jMDd23zXO131vnfeVU+OlBuXecQeTv26gViDdB5eyoiDPP0wbEAixqUNx8lDTE87Ll/6IDgr8ctXlexHTcm+Z6y2eH+31VUIcz62G0UFOzAmIxlx0XKDWHzg7emusgTrTRCEafxMLxdn+Lq4YGJm5ktv3uT4EGWOz2vBPD5lTKqyuVZxOSrk8xdKJ4aIW01AXj5/hgLszg2rwUAbsxr2b9ugQE3Q3bp2uYLy5FFpyhLHDEtSlrt93SrEBPkjVVgbAXf6uJGa+jXpSZGaUK6ll8FOAL7YVKusOEFu0LFpQ4TZpylYvixdTqPzG5aXDYkLFsY7Ft8mIQR6OWvQjvr1tnXLngk+MngZ6u+s7J8pau1vWJtj1sSR+p5kxQRiTkpkw1Un7XnhjANsXrMAXk6fqC5MIF69ZJ6yXWazUIog8BrFfgxgNvKOeR2LARn5pdSJWRSebJhyBNPXCMiUKBhgoqtMbZH2OkbMYFZ8TMgL0gQbwZlgPExcYWrF1HKpD7PGhXoCE0brZ5EFE/xZaIhg9JSZznsmi4IyxJoV2Vg4d5YyYxbBp7E4PmULZdkZwxWgG6or0dHSlGUM4j//4XdZRgDu/CsepskUvNeyrudyk59ZxFF6CF1PGFxd1WnNVlAQ9vTQeg/BliCt/8BiPyzQw/FNtsql0NkL54MTzr1bl8RDeKjBt2uXzykrZvCOEgRX1TGYR7ZMGYPXcMUgs0Xoxfhp6qv/kxW4Xpr6Su+WQEx9mIyYQNzVg2W+CdCRlWbIOJwz8dlFPl3NNWUjEmMRLffl0d1bcUHuszPFhzFz/EikCIlqKCvG+mULECTjhqD9QO7F2+Kp8VikjPXigzuwd8saRNk8lTWPFixZL0TodleTYMLpt3pqDP+P2121Cd8ZiA1NNSY8QGesnkBMQOSiDnaYj4MrbO7eCPHxxa4N61FTWqzyBKOSNXIDvA6Iy0/sx+njezVoN2xInEZX3V0dxZzh4uwMD3d3++IO5hULE/dxc1Ugzsp4eZEQMl9mSzx/nFkEBD3qxGTFZJnRMnMmxdilh5ULZsHXubcC7N4t67AtdwUyUxJU29Uskrxc3yibPVhHBk09eFRqIjKGDIRb7w8UbDcJk1owfaKwyX7d0gSBuL+wrPXCvihjkA0HeDgJuE8H5RKrR2+8SrNlpgcB1WC3MyeMANPoKGvwO/A7h/l7qFRin3jsAT4jXY9ATDnjYnPZG6fzzJ6Uqd4BWTE1b0oTZMQVMsE2ibfT3liG/dtzNAWNgToPAexPZVIjABusl/sEX0oS3DcKA5ENc8tMCqMWsVPfjzSgZYAvwZgBO7JiggclDF5Htnq6pPC1wMQ84m/LmtDg6MbVYCoYjSllDMgZecHUg5mSZgBz27nGd66Ny0cF8UnIxuv/+NMfyt4GbMtLC9+bLklpgjIKGSnBNtQaplXUWBWNZS4Jxkqw/O01hrnogr89c5cpNVD3JRv+6vMbmsJGAGZ2BAGZ8gW1ZOZ0M7joKWOAHgy9Bcog/Ex7xcZQBWF+hrGgIyWx/zNA/CYZCQS5T6eOB+1pDrA9FXHn+lXKbgnGq2RcpyfFKQjPGJfB2Iyy30C5vyekp6Cj4Qx4/XC5zwngzK5gxkWonwuO7tmMRYIXwT4OKlO8rUb8zkBstMxhA2WQfWSXJ1z66sIO1oSwcXaTWS3Qq7eASi9MGT0Exw9sVjni9LGdwpz2devDZ0p2o/z4zie2Xa3yxE5UlwoTLlyHyvJ8HC/Mw4J5o2WgucGh94/h3PcD/Uxq0wRfPw93+Li6w0rwl+2QgS8X96eOHYKM5BfX2u/elJ01e1KagFkwBsdaMWxQCGLDfbE51558z62fey89lhgbiLTBURiWFImNOQv0PLfJA0OxdrndlcxdMRepg8IxMNqK+Eh/JMUF6XlueXz08IF6Hc97OX+AwfHBSEkI03PLF07Vc0tmZ2FCRhJ2rFvaaQCnkVT+/PdfI6x22tihmCgAPGV0itra7OnYsHI2sueNR/6W7G5WdKml4jvX7WVgcnzaMAR7eSPC34Ks4SNQdeIYTiujP6OTa3P1Mc2ESRHXnZMQpRiy/KN7tuPLO9dUgrnd1YYbF1pwq7MV19qbddvRWK21Kh5e79KJyebtBm/nvhgYFYqt61YLcJ/Dl/duCpu+hJtd7Xj8QNzBpjpd8BNh89Nt0cE9bzQYLn6HAuMjZMKalDX8vQZk+H4zp43+u1gc0FR/RiczY1FFz0caUb+1M1cvDdoxoEhjIJIsv7OtBZ/fv4M716+o/eKLR3hw7xYe3r+NX379BR5/+QgVZScxeeI4hAYHqGf7OguVSZx1rel1XW9vfmuwqirdi/P1x3Gt42lfE/QuNJeiofIQcpZOweI5ozF/ejry1sxFc00hLrdWdG7KmYMQ/z6IDnLG8KQQpCUGY3x6nBC0XFw6X/Z416bFiAp0EozaK+Rjl3jSiYgLcwePv46tG5owjd/lvSy7zkiNF7dTGFegvaoay2FqcR4BRzeHfjIA/1Xcck/kLJ5hzw8uP4Lyot0oO5aP0qPbFZDPlOxVqzq5R43ATON+RdkunD65UywfhYc3Y8mCyeIa+sNVwN/dqTe8XBw1QMfP5NYisyq3gwfE4+Vpd9kJW3MXvtSF2LVxST5B+QWmtG4JQizOWDrfniC+e3vuCzfGjrzlZS9+Xk4C7aWMKMxHQf2l+dp5q3x7JpIzQZ3pM6sWTsPapbOQk/P2KwFfJkG8yarClqqy/HHDhiKI2SqOThgaHyfu3EnUlhbhXPVJ7VumIjITJjNlkAJxkHhNlGMSo0NVC7/aJky44xzuXbmg+ywWRPB9dOOSHt+5IQcewoK5tJ02aXQ6qk8V44u7N3D78gVcbmvGgxuX1ZqqyxETEqBAzNokxYf2fW+gNnZUMqZOer8lGffsWv9MCtnfspH9U5NnGpmuqHOz67fMZCAoU8flawIwZQk+h7CjpQm3r13Go7u38OD2Dd2n3b1xFV98dh9t55tw4vhRbNuyEdOmTEBkeDB8vd21UNebADG9OWYcXX6i8b5N6xLQvNFlX1zxfFCM4Ewg5PmL506B4GwA9fYNC2VSdxcSMxPHD25EiWBP0YEN4unZ5SQC8IoFWWgVkL/RWZ21NXc+kuOteqyr5TR+cCBOSYiAS+9/1ci4UYWNMgVLVrJ+MBOlGWFkmkhN6RGcOpovQLxXV81VnzygoFxxfA+4ZNAA5MoTAsDFZMn5qK48iBNF28R2oLx0P44WbJcbYCZGpg1BckL8s4xYtvxcAnFSfP/3fmOvXjodbv1+jCXzsmAAWVvT6Xd2TYP8+iEjNeaNvu/WtQuxa2N2/lOQX5nwXUH3XdrwQfHw7tdXgTjKakF1SZGA8Ql122pKD6Kh4jAahXFQ96Y+zjQ3P/GYaATnvNXZGsg8X1sh7txpnK0sxeH8rcjNXoBPp01AclwkbOLxeDn1waCYcGErK3Dtwnl8/fCO3PjMsmhRUOb+6aLDWhiKFmb1wYnD+783UNu2eRXyd+T+w5ZkZPoapQZKBgRi5vIazxK0B9PsK1gZqJs1dRLW56xEWYmA0eVOBeKHd24qG25prMPxIwXYkrces2dORWpyIgbGxyAiLEhB2MfL7Y2AmPEZGuXBrnN1+e/yvxEwac8E9Nrtq+seXqv9+dV2eyU3AjOBOC7cQ8jiVhh/c6OHhHBDQJwg3HnOHpzjNS21xwjC3xhga0wALwNh45o772PhR1ZmMnxd+ygQcwUdo5xkxRlDkzFz8iScOJivgjcF8NLDe3Fs73YB3gMKygTnqpJDqhVXnywA14cTkOna0rh/7PDWJ7YdJQLg1eXHUVtZgiMH8rFu1XIFXsMIxlzQQZacGB/13gfLrrxl2akDQ7Bu+ez3+t4TM5Mwb+r/X4+CSU8aCB/xeALlN/dzcsSR/O0CxkdV+2+pKVZp4uyZw1qNjcFHLoPmAhCnj34KauLMFMlKT9VUvVkTRmsAk0/1GBAeqMDt/PHPlEUThFcsnIu68pMqRdy92ol717rU7ly5qNvDe3aohEHmTGnizMkis3btd2xczs0FLARh+zJj+/MFGUSjdZe69fTQXGNvIT+sKTw6fQTGZKSrjR+VifShqYiPjhLgDdHAOh/ewJTT0GB5b5sVVn9fXSHL7assWHAlyNtFYx0XGqsemz30kjZ9/AgB4L4I8HISQO6rBd5ZzDk9dYgA8QTMyhqDPTIrVpcUCiAfU/e1SdgPtwRmpoowYklwJjCXHNqF04V7xb3dh9IjuzVtraasEFWlhbpKq7z4KI7s3Yll8+diZEoKAr29YfPyUvN3c9Ei856OvZEQHWIOxu+xTRk1HAHuzrC4OsLboTd2bchB/enjOsFSfrJLEwc0MHu+thzHD+zSICfZDQN8Lr1+Bh+nTzQPmSlw3Hf88Md6ju4os0/4qKX9Ozaj8cxplSNuXWJB+VYFYBoZMlnx9g1rEOznqWCcOXQw2hprzAH7HRufcMJVisw+MYCY4KsZFL4+WiSeBeF96YG6uz1ZdWfT6+xLou2lBviaD4XlQxsCAyxagsDi59MNvgRlvn4TIGbWz4BwG4tIfWP20MsCDaOS4enwgbJiAjELuhAIKVOQHZMxTckciXVLl2DDsmxsWpmNHetyULhnp4Ixy13Wny5GY3mJMucqGbhkVhVFBSiWwctaBBtXL9VVebnLF2PhzOlIjo/VXGGXTz6B1UNcJXd3MVcN6nAVl1vfDxD/mmWh/8ztfRTL54oj117/CvfeHyFI+nrrmuUaXT5XfUKlJkpMBOKmM6Uyoe7TlXesd8EUwGlj0zXFjyyHgMvsCxoHHHOweX798oXCrMs0v5jpbDTuM5uCGRfMsDDyjudMGaceGe+/edOyzCc6v0NjqhxTBo1FNMbTtwnG9vrA9mwJsmICMJ/gwdK3BGBex/xjZlvweu5zAUjPJzP3XDrN18bj015mXAlq83JAXJi/VvMze+gljau5+EOpeTtrzigHhLdzb3g4COPp0xdR1gBEB9gQ6sMoux9ipcNGJQ/BmsWfIn9jLor25QsgHxVmfEQYcgFKCvZg7+Z1yF36KeLCg5Rl8wkdfGRSRECAgrCPs7M+UJSpar4uTiqL8DP93fvB0+lDDOpvAnHHS1bHvQ/NeOWnM4UJfyxA/AHCZMJlDiaXjl44W969wogFnJjWxgU8LHbERTBF+3eiXQCbxhV/qxbOBpd/c8n3hhWLdEk2K7ixfCer0HGxB3OMmVNspLoRiI2nfJQLuPPJ0FxCzZzlDauWmIP1Xe6Zlros1qfgajw+b47ASSA1qrEZsoQBvHxYsLESzjjPcwRZ/p3BpAnOhhkLQozCQq+yUH9XxZbYUD8tJGX20Esaq31Z3D+Ba++fqtklij5azpBmcWEesQd8HBx1P9Jihb+zE9w/6WUH5eAAjEpJwtxJ47FoxhTMnzIekzPTkBQdCn8XAXQnB12yTPNydBT27aDgSzA29gnELCxEicTm46gpZqmDQr/3Tvsu2Qr/CM1Y4ce8S8++H8IiE2+YDJj8Tat07T2zY6gTa0aMeDisT3y60C5TcKk1a1JwOTQXxfAcS4BSvrguLJc1MXiMz7tjgXku+CDrJRPuyYyNh43SYyIIs0AQl1EX5G8xB+u7AHFrQwJrP7BqGwu9U4Iw2LBRAMiQIYzjBqhyv/sp7U9eE8ApURi1KwxQ5t8Yr18HxFyWbwLxa1re6vlayczH+UOxj/VHo87H3GIvp14Cui4KwFZXNwFkuwUwu4GpZq6OsLo59TCHbzFXNYteT3NTs7q5q/HBlX4uDuLWuiDIx0nA+V8FtHuJuzrC7LTvubGClY/TR3CTCdji1lufgLB60XQNtLbWlWgaG1fa8ckrjZXFuuCDq++4NeoVG1suMuk8V929ZfU6Vr273NYoQCwA3X4WXefrcbWDDyRt09esyjd2xBD1xNz7fYgsPiro5FGz39+xcXWhp5ApMmJmShipa1rv29NNl0DzOAvzcPtK41NzZBts8dUty+XSXnWcW2ZfsbY4l+X3/tl/w7BBEfoUH7N3XjkgZ2uFMBaQcfr4X9QIygRGr759NM2JEXZfRwf4OfcTpuvwxPoiwN2xhzm8YD5Owood+6h5yfvQvOW9aD58sqyHi6ZGOX30E/nc/ynfoTcyh8aACdlmz3z/jY+hibK568oim2cfhPg6YsywOGHHrMdRoCBM8CXgktFwy8LxBGMa91tqT6n+13G2stsIyARjAi7tRmcLHt7swme3L6Ot8YwWgSIIB/u5KQgzJrArb43Z5++h8ZFKLCIfHOCpVdFYC4JF4bn193btfmoHdWRWxzNqCDP32Eh7M5ab8+GfDN5zjYFhLPZO0zrm/LtXmFufnyDEz6n74Qhme03bsWFp2cTMIeJC+Gh5xQAZlPwRvfp9rObt0EuA8xMB4j7CaPsJm+1nB1rHXsqqnrUPus2o8MUC5Ezwp3n2+0SNq7V8nHprZDUq0AtJ/QMxd6r52O0funHVpHuff8EnP/mv0i8/QXSgG4bE2jC4fwBYk4KlRFkiND9vNQ7lb0LRge0oOcwFPQzqHdL6xSwWxKp8NLJngjdBuqGyBIX7dyBvTTbWZM/H8gUzMWXsCAVeb+dP1POKCw9AztJ5rGPcafbG+2l87NH6NUvBmhyUKWz+7naGbBFW7CUEy62vPlXD/ogju/FBpYaxEhwtUP6Oz7Xk8yr95e/48GAaX/PBwzxn8+NTnz30WvsTf9z0IcARwX5K7lhDpekf6TlzP0TbnPMpVi6comUYWag8PtSqFhfqpxYf5o+BEVYMirIhKcamVe7tFviixdu0slGauCUs0swSdcMGRiMtIUaNa7+njRmhhZ13bVyW33MFGtu+HYsTzB75gSbidYs7503JQGSAC/r9/L/DWbwTX+cPNX4Q6N0P4VZXvR9oLOvJyZpMZ2CkRavLDRZmbRgnVG5ZyCg1IRoDo4IQE+KvFiZgYPFwgFUGMvczUgYqO259gwdsmu3t29ZNq7Fs8WzMmzURC+ZOwdyZE5A1Jg0ZaYkYlT5EH3PE/fRhgzBiaALSUgZg6JA4pA6OVUtJjEFifDgGxASrDYoL634dE24V0HUVduwhwMvaFgRtZwFjF2HRXogOs2iJVhbhMnvinVjyEpOh/BN6RlzOPkm8I0oW7n1/qrq9t3g3LEpEYKZxn3EFX5eP4O/W64Xjxjkv54/g7fIxrF79EOzvosHYAG8HxIb7Y+TQATi8dzPazlaa99nfQdPVYq21CW3nqrJbz1Xmn2+uLDvfUPZNc20p6iuP60MemmpOqvF11akjKNi9CUf2bcHR/Vt1f+/2XOzZthYH8zei8MA2dPSoZtbVUplv/spmM9tbNnopi2dn6RNDZmQN12pwLD/KwAtZL9lwQpQVrIbH1zS+5vFui7YhLsKCAVEBGD6kP2ZNzsTm3KX6ENouGfRmzvA/T7vyQz6Q02zvp/2zppb947OunB+1C9Ni5bT2J0/hJSB3tf9AD2o029+MbZu/gtnMZjazmc1sZjOb2cxmNrOZzWxmM5vZzGY2s5nNbGYzm9nMZjazmc1sZjOb2cxmNrOZzWzvvf0/CF69DleN9U0AAAAASUVORK5CYII="/>
</div>
<?php

namespace App\Http\Controllers\Restaurant;

use App\Product;
use App\Restaurant\Modifier;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Controller;
use App\Utils\BusinessUtil;
use App\BusinessLocation;

use App\Utils\ProductUtil;

class ModifierSetsController extends Controller
{
    /**
     * All Utils instance.
     *
     */
    protected $businessUtil;
    protected $productUtil;

    /**
     * Constructor
     *
     * @param ProductUtils $product
     * @return void
     */
    public function __construct(ProductUtil $productUtil, BusinessUtil $businessUtil)
    {
        $this->productUtil = $productUtil;
        $this->businessUtil = $businessUtil;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if (request()->ajax()) {
    
            $business_id = request()->session()->get('user.business_id');

            $modifer_set = Product::where('business_id', $business_id)
                            ->where('type', 'modifier')
                            ->with(['variations', 'modifier_products']);

            return \Datatables::of($modifer_set)
                ->addColumn(
                    'action',
                    '
                    @can("product.update")
                        <button type="button" data-href="{{action(\'Restaurant\ModifierSetsController@edit\', [$id])}}" class="btn btn-xs btn-primary hidden edit_modifier_button" data-container=".modifier_modal"><i class="glyphicon glyphicon-edit"></i> @lang("messages.edit")</button>
                        &nbsp;
                        <button type="button" data-href="{{action(\'Restaurant\ProductModifierSetController@edit\', [$id])}}" class="btn btn-xs btn-info edit_modifier_button" data-container=".modifier_modal"><i class="fa fa-cubes"></i> @lang("restaurant.manage_products")</button>
                    &nbsp;
                    @endcan

                    @can("product.delete")
                        <button data-href="{{action(\'Restaurant\ModifierSetsController@destroy\', [$id])}}" class="btn btn-xs btn-danger delete_modifier_button"><i class="glyphicon glyphicon-trash"></i> @lang("messages.delete")</button>
                    @endcan
                    '
                )
                ->editColumn('modifier_products', function ($row) {
                    $products = [];
                    foreach ($row->modifier_products as $product) {
                         $products[] = $product->name;
                    }
                     return implode(',  ', $products);
                })
                ->editColumn('quantity', function ($row) {
                    $modifer_set_id = $row->variations->first()->variation_product_id;
                    $quantity = DB::table('modifiers')->where('id', $modifer_set_id)->select('quantity')->pluck('quantity')->first();
                    $min_quantity = DB::table('modifiers')->where('id', $modifer_set_id)->select('min_quantity')->pluck('min_quantity')->first();

                     //return $quantity;
                     return     $quantity." (Min.: ".$min_quantity.")";
                })
                ->editColumn('type', function ($row) {
                    $modifer_set_id = $row->variations->first()->variation_product_id;
                    $type = DB::table('modifiers')->where('id', $modifer_set_id)->select('type')->pluck('type')->first();

                     return $type;
                })
                ->editColumn('variations', function ($row) {
                    $modifiers = [];
                    foreach ($row->variations as $modifier) {

                        if (strpos( ($modifier->name), 'product:') !== false) {
                            $modi = explode(':', ($modifier->name) );
                            $product = Product::where('id', $modi[1])->first();
                            $modifiers[] = '<span class="btn-xs btn-primary" title="Product">'.$product->name .' ('.$modifier->default_purchase_price.')</span> ';
                        } else {
                            $modifiers[] = $modifier->name.' ('.$modifier->default_purchase_price.')';
                        }

                    }
                    return implode(', ', $modifiers);
                })
                ->removeColumn('id')
                ->escapeColumns(['action'])
                ->make(true);
        }
        $business_id = request()->session()->get('user.business_id');
        $business_details = $this->businessUtil->getDetails($business_id);

        $business_locations = BusinessLocation::forDropdown($business_id, false, true);
        $bl_attributes = $business_locations['attributes'];
        $business_locations = $business_locations['locations'];

        $products = Product::where('business_id', $business_id)
                            ->where('type', 'single');

        $default_location = null;
        if (count($business_locations) == 1) {
            foreach ($business_locations as $id => $name) {
                $default_location = $id;
            }
        }

        $products = Product::pluck('name', 'id');

        return view('restaurant.modifier_sets.index', compact('products', 'default_location', 'business_locations', 'bl_attributes'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if (!auth()->user()->can('product.create')) {
            abort(403, 'Unauthorized action.');
        }
        $products = Product::where('type', 'single')
        ->pluck('name', 'id');

        return view('restaurant.modifier_sets.create', compact('products') );
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            if (!auth()->user()->can('product.create')) {
                abort(403, 'Unauthorized action.');
            }
            
            $input = $request->all();
            
            $business_id = $request->session()->get('user.business_id');
            $user_id = $request->session()->get('user.id');

            $modifer_set_data = [
                'name' => $input['name'],
                'type' => 'modifier',
                'sku' => ' ',
                'tax_type' => 'inclusive',
                'alert_quantity' => 0,
                'business_id' => $business_id,
                'created_by' => $user_id
            ];

            $modifer_set = Product::create($modifer_set_data);

            $sku = $this->productUtil->generateProductSku($modifer_set->id);
            $modifer_set->sku = $sku;
            $modifer_set->save();

            $modifers = [];
            foreach ($input['modifier_name'] as $key => $value) {
                $modifers[] = [
                    'value' => $value,
                    'default_purchase_price' => $input['modifier_price'][$key],
                    'dpp_inc_tax' => $input['modifier_price'][$key],
                    'profit_percent' => 0,
                    'default_sell_price' => $input['modifier_price'][$key],
                    'sell_price_inc_tax' => $input['modifier_price'][$key],
                ];
            }

            $modifiers_data = [];
            $modifiers_data[] = [
                'name' => 'DUMMY',
                'variations' => $modifers
            ];
            $this->productUtil->createVariableProductVariations($modifer_set->id, $modifiers_data);
            $product_id = NULL;
            if ($input['product_id']) {
                $product_id = $input['product_id'];
            }
            $modifer_data = [
                'product_id' => $product_id,
                'type' =>  $input['type'][0].'',
                'quantity' => $input['quantity'],
                'min_quantity' => $input['min_quantity']+0
            ];
            
            $id = DB::table('modifiers')->insertGetId( $modifer_data);
            
            DB::table('variations')
                ->where('product_id', $modifer_set->id)
                ->update(['variation_product_id' => $id]);

            // DB::beginTransaction();
            // $modifer = Modifier::create($modifer_data);
            // $modifer->save();
            
            // DB::commit();

            $output = ['success' => 1, 'msg' => __("lang_v1.added_success")];
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = ['success' => 0, 'msg' => __("messages.something_went_wrong")];
        }
        

        return $output;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('restaurant.modifier_sets.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id, Request $request)
    {
        if (!auth()->user()->can('product.update')) {
            abort(403, 'Unauthorized action.');
        }

        try {
            $business_id = $request->session()->get('user.business_id');

            $modifer_set = Product::where('business_id', $business_id)
                            ->where('id', $id)
                            ->with(['variations'])
                            ->first();

            return view('restaurant.modifier_sets.edit')
                ->with(compact('modifer_set'));
        } catch (\Exception $e) {
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = ['success' => 0, 'msg' => __("messages.something_went_wrong")];
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        if (!auth()->user()->can('product.update')) {
            abort(403, 'Unauthorized action.');
        }

        try {
            DB::beginTransaction();

            $input = $request->all();
            $business_id = $request->session()->get('user.business_id');
            $user_id = $request->session()->get('user.id');

            $modifer_set = Product::where('business_id', $business_id)
                    ->where('id', $id)
                    ->where('type', 'modifier')
                    ->first();
            $modifer_set->update(['name' => $input['name']]);

            //Get the dummy product variation
            $product_variation = $modifer_set->product_variations()->first();

            $modifiers_data[$product_variation->id]['name'] = $product_variation->name;

            $variations_edit = [];
            $variations = [];

            //Set existing variations
            if (!empty($input['modifier_name_edit'])) {
                $modifier_name_edit = $input['modifier_name_edit'];
                $modifier_price_edit = $input['modifier_price_edit'];

                foreach ($modifier_name_edit as $key => $name) {
                    if (isset($modifier_price_edit[$key])) {
                        $variations_edit[$key]['value'] = $name;
                        $variations_edit[$key]['default_purchase_price'] = $modifier_price_edit[$key];
                        $variations_edit[$key]['dpp_inc_tax'] = $modifier_price_edit[$key];
                        $variations_edit[$key]['default_sell_price'] = $modifier_price_edit[$key];
                        $variations_edit[$key]['sell_price_inc_tax'] = $modifier_price_edit[$key];
                        $variations_edit[$key]['profit_percent'] = 0;
                    }
                }
            }
            //Set new variations
            if (!empty($input['modifier_name'])) {
                foreach ($input['modifier_name'] as $key => $value) {
                    $variations[] = [
                        'value' => $value,
                        'default_purchase_price' => $input['modifier_price'][$key],
                        'dpp_inc_tax' => $input['modifier_price'][$key],
                        'profit_percent' => 0,
                        'default_sell_price' => $input['modifier_price'][$key],
                        'sell_price_inc_tax' => $input['modifier_price'][$key],
                    ];
                }
            }

            //Update variations
            $modifiers_data[$product_variation->id]['variations_edit'] = $variations_edit;
            $modifiers_data[$product_variation->id]['variations'] = $variations;
            $this->productUtil->updateVariableProductVariations($modifer_set->id, $modifiers_data);

            DB::commit();

            $output = ['success' => 1, 'msg' => __("lang_v1.updated_success")];
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = ['success' => 0, 'msg' => __("messages.something_went_wrong")];
        }

        return $output;
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        if (!auth()->user()->can('product.delete')) {
            abort(403, 'Unauthorized action.');
        }

        try {
            DB::beginTransaction();
            $business_id = $request->session()->get('user.business_id');

            Product::where('business_id', $business_id)
                ->where('type', 'modifier')
                ->where('id', $id)
                ->delete();

            DB::commit();

            $output = ['success' => 1, 'msg' => __("lang_v1.deleted_success")];
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = ['success' => 0, 'msg' => __("messages.something_went_wrong")];
        }

        return $output;
    }
}

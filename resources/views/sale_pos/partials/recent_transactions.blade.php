@if(!empty($transactions))

	<table id="recent-transaction-{{$transactions->first()->id}}" class="recent-transaction table table-slim no-border">
		<thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
		@foreach ($transactions as $transaction)
			<tr class="cursor-pointer"
	    		data-toggle="tooltip"
	    		data-html="true"
	    		title="Customer: {{optional($transaction->contact)->name}}
		    		@if(!empty($transaction->contact->mobile) && $transaction->contact->is_default == 0)
		    			<br/>Mobile: {{$transaction->contact->mobile}}
		    		@endif
	    		" >
				<td>
					{{ $loop->iteration}}.
					<a class="edit-sale" href="{{action('SellPosController@edit', [$transaction->id])}}">
	    				<i class="fa fa-pencil text-muted" aria-hidden="true" title="{{__('lang_v1.click_to_edit')}}"></i>
	    			</a>
				</td>
				<td>
					{{ $transaction->invoice_no }}
				</td>
				<td>
					<i class="fa fa-puzzle-piece" aria-hidden="true"></i>  
					{{ $transaction->res_table_id }}*
				</td>
				<td class="display_currency">
					{{ $transaction->final_total }}
				</td>
				<td>
	    			<a href="{{action('SellPosController@destroy', [$transaction->id])}}" class="delete-sale" style="padding-left: 20px"><i class="fa fa-times text-danger" title="{{__('lang_v1.click_to_delete')}}"></i></a>
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
	<script>
	$(document).ready(function() {
		$('#recent-transaction-{{$transactions->first()->id}}').DataTable( {
			scrollY:        '50vh',
			scrollCollapse: true,
			paging:         false,
			buttons: []
		} );
	} );
	
	</script>
@else
	<p>@lang('sale.no_recent_transactions')</p>
@endif
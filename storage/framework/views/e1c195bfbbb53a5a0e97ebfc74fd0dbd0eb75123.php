<?php $request = app('Illuminate\Http\Request'); ?>
<div class="col-md-12 no-print pos-header">
  <div class="row">

    <div class="col-md-7 col-sm-2">
      <div class="text-center pull-left mt-15 hidden-xs"><strong> <?php echo e(date('Y-m-d')); ?> </strong></div>
      <div class="text-center pull-left mt-15 hidden-xs ltr-dir time-header"><strong> <?php echo e(date('h:i A')); ?> </strong></div>
      <a href="#" title="<?php echo e(__('lang_v1.go_back')); ?>" data-toggle="tooltip" data-placement="bottom" class="back-new-menu btn-default m-6 btn-xs m-5 pull-right none">
          <span class="icon-Artboard-25">
                <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span>
          </span>
      </a>
    </div>

    <div class="col-md-5 col-sm-10">

      <a href="<?php echo e(action('SellPosController@index')); ?>" title="<?php echo e(__('lang_v1.go_back')); ?>" data-toggle="tooltip" data-placement="bottom" class="btn btn-default m-6 hidden-xs btn-xs m-5 pull-right">
          <span class="icon-Artboard-31">
              <span class="path1"></span><span class="path2"></span>
          </span>
      </a>

      <button type="button" id="close_register" title="<?php echo e(__('cash_register.close_register')); ?>" data-toggle="tooltip" data-placement="bottom" class="btn btn-default m-6 hidden-xs btn-xs m-5 btn-modal pull-right" data-container=".close_register_modal" 
          data-href="<?php echo e(action('CashRegisterController@getCloseRegister')); ?>">
          <span class="icon-Artboard-30">
              <span class="path1"></span><span class="path2"></span><span class="path3"></span>
          </span>
      </button>

      <button type="button" id="register_details" title="<?php echo e(__('cash_register.register_details')); ?>" data-toggle="tooltip" data-placement="bottom" class="btn btn-default m-6 hidden-xs btn-xs m-5 btn-modal pull-right" data-container=".register_details_modal" 
          data-href="<?php echo e(action('CashRegisterController@getRegisterDetails')); ?>">
          <span class="icon-Artboard-29">
                <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span>
          </span>
      </button>

      <button id="btnCalculator" type="button" class="btn btn-default pull-right m-5 hidden-xs btn-xs mt-10 popover-default" data-toggle="popover" data-trigger="click" data-content='<?php echo $__env->make("layouts.partials.calculator", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>' data-html="true" data-placement="bottom">
          <span class="icon-Artboard-28">
                <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span>
          </span>
      </button>

      <button type="button" title="<?php echo e(__('lang_v1.full_screen')); ?>" data-toggle="tooltip" data-placement="bottom" class="btn btn-default m-6 hidden-xs btn-xs m-5 pull-right" id="full_screen">
          <span class="icon-Artboard-26">
              <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span>
          </span>
      </button>

    </div>

  </div>
</div>

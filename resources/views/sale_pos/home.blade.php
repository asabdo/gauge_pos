@extends('layouts.app')
@section('title', __( 'sale.list_pos'))

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header no-print">
    <h1>POS
    </h1>
</section>

<!-- Main content -->

<section class="no-print grid-px">
        
        <div class="">
          <a class="module-title" href="/products"><i class="fa fa-lemon-o" aria-hidden="true"></i>
Items</a>
        </div>

        <div class="">
          <a class="module-title" href="/reports/stock-report"><i class="fa fa-battery-three-quarters" aria-hidden="true"></i>
Stock</a>
        </div>

        <div class="">
          <a class="module-title" href="/users"><i class="fa fa-users" aria-hidden="true"></i>
Users</a>
        </div>

        <div class="">
          <a class="module-title" href="/modules/kitchen"><i class="fa fa-cutlery" aria-hidden="true"></i>
Kitchen</a>
        </div>
        
        <div class="">
          <a class="module-title" href="/pos/create"><i class="fa fa-print" aria-hidden="true"></i>
POS</a>
        </div>
        


</section>

<!-- /.content -->
@endsection

<style>
.grid-px {
    display: grid;
    grid-template-columns: 50% 1fr 1fr;
    grid-template-rows: 156px 153px 150px;
    grid-gap: 17px;
}
.grid-px div:nth-of-type(1) .module-title {
  padding: 135px;
}
.grid-px div:nth-of-type(1) {
  background: transport;
}
.grid-px div:nth-of-type(2) {
  background: #fb747b;
}
.grid-px div:nth-of-type(3) {
  background: #088888;
}
.grid-px div:nth-of-type(4) {
  background: #749b94;
}
.grid-px div:nth-of-type(5) {
  background: #aaaea1;
}
.grid-px div:first-of-type {
  grid-column: 1 / 2;
  grid-row: 1 / 3;
}
.grid-px div:nth-of-type(2) {
  grid-column: 2 / 4;
  grid-row: 1;
}
.grid-px div:nth-of-type(5) {
  grid-column: 1 / 4;
  grid-row: 3;
}

.module-title {
  display: grid;
    font-size: 25px;
    padding: 50px;
    text-align: center;
    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#e2e2e2+0,dbdbdb+50,d1d1d1+51,fefefe+100;Grey+Gloss+%231 */
    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#353535+1,ffffff+50,333333+100&0.5+0,0.46+100 */
    background: -moz-linear-gradient(top, rgba(53,53,53,0.5) 0%, rgba(53,53,53,0.5) 1%, rgba(255,255,255,0.48) 50%, rgba(51,51,51,0.46) 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(top, rgba(53,53,53,0.5) 0%,rgba(53,53,53,0.5) 1%,rgba(255,255,255,0.48) 50%,rgba(51,51,51,0.46) 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to bottom, rgba(53,53,53,0.5) 0%,rgba(53,53,53,0.5) 1%,rgba(255,255,255,0.48) 50%,rgba(51,51,51,0.46) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#80353535', endColorstr='#75333333',GradientType=0 ); /* IE6-9 */
}
</style>
